//
//  HBImageCache.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 25/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class HBFile;

@interface HBImageCache : NSCache

+ (instancetype)sharedCache;

- (void)clearCache;

//- (UIImage *)cachedImageForFile:(HBFile *)file;

//- (void)setCachedImage:(UIImage *)image forFile:(HBFile *)file;

@end

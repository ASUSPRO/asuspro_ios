//
//  UIButton+HBRemoteImage.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 25/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)

#import <UIKit/UIKit.h>

@class HBFile;

@interface UIButton (HBRemoteImage)

- (void)hb_setImageWithFile:(HBFile *)file forState:(UIControlState)state placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion;
- (void)hb_setBackgroundImageWithFile:(HBFile *)file forState:(UIControlState)state placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion;

- (void)hb_cancelCurrentImageOperation;
- (void)hb_cancelCurrentBackgroundImageOperation;

@end

#endif
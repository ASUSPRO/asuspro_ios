//
//  UIButton+HBRemoteImage.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 25/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)

#import <objc/runtime.h>
#import "UIButton+HBRemoteImage.h"
#import "HBImageDownloader.h"
#import "HBMacros.h"

static const char kCurrentImageOperationKey;
static const char kCurrentBackgroundImageOperationKey;

@interface UIButton ()

@property (nonatomic, strong) AFHTTPRequestOperation *hb_currentImageOperation;
@property (nonatomic, strong) AFHTTPRequestOperation *hb_currentBackgroundImageOperation;

@end

@implementation UIButton (HBRemoteImage)

- (AFHTTPRequestOperation *)hb_currentImageOperation {
    return objc_getAssociatedObject(self, &kCurrentImageOperationKey);
}

- (void)setHb_currentImageOperation:(AFHTTPRequestOperation *)hb_currentImageOperation {
    objc_setAssociatedObject(self, &kCurrentImageOperationKey, hb_currentImageOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (AFHTTPRequestOperation *)hb_currentBackgroundImageOperation {
    return objc_getAssociatedObject(self, &kCurrentBackgroundImageOperationKey);
}

- (void)setHb_currentBackgroundImageOperation:(AFHTTPRequestOperation *)hb_currentBackgroundImageOperation {
    objc_setAssociatedObject(self, &kCurrentBackgroundImageOperationKey, hb_currentBackgroundImageOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Public

- (void)hb_setImageWithFile:(HBFile *)file forState:(UIControlState)state placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion {
    [self hb_cancelCurrentImageOperation];
    
//    HB_WEAK_SELF weakSelf = self;
//    [HBImageDownloader setImageWithFile:file placeholderImage:placeholderImage imageSetterBlock:^(UIImage *image) {
//        [weakSelf setImage:image forState:state];
//    } completion:^(UIImage *image, NSError *error) {
//        weakSelf.hb_currentImageOperation = nil;
//        
//        if (completion) completion(image, error);
//    }];
}

- (void)hb_setBackgroundImageWithFile:(HBFile *)file forState:(UIControlState)state placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion {
    [self hb_cancelCurrentBackgroundImageOperation];
    
//    HB_WEAK_SELF weakSelf = self;
//    [HBImageDownloader setImageWithFile:file placeholderImage:placeholderImage imageSetterBlock:^(UIImage *image) {
//        [weakSelf setBackgroundImage:image forState:state];
//    } completion:^(UIImage *image, NSError *error) {
//        weakSelf.hb_currentBackgroundImageOperation = nil;
//        
//        if (completion) completion(image, error);
//    }];
}

- (void)hb_cancelCurrentImageOperation {
    [self.hb_currentImageOperation cancel];
    self.hb_currentImageOperation = nil;
}

- (void)hb_cancelCurrentBackgroundImageOperation {
    [self.hb_currentBackgroundImageOperation cancel];
    self.hb_currentBackgroundImageOperation = nil;
}


@end

#endif
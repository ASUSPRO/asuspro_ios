//
//  UIImageView+HBRemoteImage.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 25/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//


#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)

#import <objc/runtime.h>
#import "UIImageView+HBRemoteImage.h"
#import "HBImageDownloader.h"
#import "HBMacros.h"

static const char kCurrentImageOperationKey;

@interface UIImageView ()

@property (nonatomic, strong) AFHTTPRequestOperation *hb_currentImageOperation;

@end

@implementation UIImageView (HBRemoteImage)

- (AFHTTPRequestOperation *)hb_currentImageOperation {
  return objc_getAssociatedObject(self, &kCurrentImageOperationKey);
}

- (void)setHb_currentImageOperation:(AFHTTPRequestOperation *)hb_currentImageOperation {
  objc_setAssociatedObject(self, &kCurrentImageOperationKey, hb_currentImageOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Public

- (void)hb_setImageWithFile:(HBFile *)file placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion {
  [self hb_cancelCurrentImageOperation];
  
//  HB_WEAK_SELF weakSelf = self;
//  [HBImageDownloader setImageWithFile:file placeholderImage:placeholderImage imageSetterBlock:^(UIImage *image) {
//    weakSelf.image = image;
//  } completion:^(UIImage *image, NSError *error) {
//    weakSelf.hb_currentImageOperation = nil;
//    
//    if (completion) completion(image, error);
//  }];
}

- (void)hb_cancelCurrentImageOperation {
  [self.hb_currentImageOperation cancel];
  self.hb_currentImageOperation = nil;
}

@end

#endif

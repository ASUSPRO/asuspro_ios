//
//  UIImageView+HBRemoteImage.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 25/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)

#import <UIKit/UIKit.h>

@class HBFile;

@interface UIImageView (HBRemoteImage)

- (void)hb_setImageWithFile:(HBFile *)file placeholderImage:(UIImage *)placeholderImage completion:(void (^)(UIImage *image, NSError *error))completion;

- (void)hb_cancelCurrentImageOperation;

@end

#endif

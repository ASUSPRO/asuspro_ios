//
//  ASUSOrderAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSOrderAPI.h"

@implementation ASUSOrderAPI

+(HBRequest *)requestForUploadOrderByData:(NSString *)data
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/uploadOrder?data=%@",data) parameters:nil];
}

+(HBRequest *)requestForGetOrderListByUserID:(NSInteger)userID  page:(NSInteger )page
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getOrderList?user_id=%ld&page=%ld",(long)userID,(long)page) parameters:nil];
}
@end

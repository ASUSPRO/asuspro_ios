//
//  ASUSUserAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSUserAPI : HBBaseAPI

+(HBRequest *)requestForUpdateAvatarByUserID:(NSInteger)userID;

+(HBRequest *)requestForUpdateUserInfoByUserID:(NSInteger)userID name:(NSString *)name pay_password:(NSString *)payPassword birthdday:(NSString *)birthday mobile:(NSString *)mobile phone:(NSString *)phone;

+(HBRequest *)requestForUserFeedBackByUserID:(NSInteger)userID userName:(NSString *)username feedBack:(NSString *)feedBack;

+(HBRequest *)requestForCheckPayPassByUserID:(NSInteger)userID pay_password:(NSString *)pay_password;

+(HBRequest *)requestForUserSendEmialByEmial:(NSString *)emial;

@end

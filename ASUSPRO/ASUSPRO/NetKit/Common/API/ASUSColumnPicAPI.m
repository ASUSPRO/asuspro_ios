//
//  ASUSColumnPicAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/20.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSColumnPicAPI.h"

@implementation ASUSColumnPicAPI

+(HBRequest *)requestForColumnPic
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getColumnPic") parameters:nil];
}
@end

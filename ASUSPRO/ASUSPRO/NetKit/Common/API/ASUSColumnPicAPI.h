//
//  ASUSColumnPicAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/20.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSColumnPicAPI : HBBaseAPI

+(HBRequest *)requestForColumnPic;

@end

//
//  ASUSReceiveAddressAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSReceiveAddressAPI : HBBaseAPI
+(HBRequest *)requestForAddReceiveAddressByUserID:(NSInteger)userID userName:(NSString *)username province:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default;
+(HBRequest *)requestForEditReceiveAddressByAddressID:(NSInteger)addressID userID:(NSInteger)userID userName:(NSString *)username province:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default;

+(HBRequest *)requestForGetReceiveAddressByUserID:(NSInteger)userID;

+(HBRequest *)requestForDeleteReceiveAddressByUserID:(NSInteger)userID addressID:(NSInteger)addressId;
@end

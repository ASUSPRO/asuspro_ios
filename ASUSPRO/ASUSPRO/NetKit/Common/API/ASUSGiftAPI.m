//
//  ASUSGiftAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSGiftAPI.h"

@implementation ASUSGiftAPI

+(HBRequest *)requestForGiftCategory
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/giftCategory") parameters:nil];
}

+(HBRequest *)requestForNewGiftListPerMonth
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getNewGiftList") parameters:nil];
}

+(HBRequest *)requestForAsusGiftList
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getAsusGiftList") parameters:nil];
}

+(HBRequest *)requestForGetGiftListByCategoryID:(NSInteger)categoryID  page:(NSInteger)page
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/giftList?category_id=%ld&page=%ld",(long)categoryID,(long)page) parameters:nil];
}

+(HBRequest *)requestForGetGiftDetailByGiftID:(NSInteger)giftID
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/giftDetail?gift_id=%ld",(long)giftID) parameters:nil];
}
@end

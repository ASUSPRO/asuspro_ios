//
//  ASUSProductAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSProductAPI : HBBaseAPI

+(HBRequest *)requestForProductCategory;

+(HBRequest *)requestForGetProductListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page;

@end

//
//  ASUSGiftAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSGiftAPI : HBBaseAPI

+(HBRequest *)requestForGiftCategory;

+(HBRequest *)requestForNewGiftListPerMonth;

+(HBRequest *)requestForAsusGiftList;

+(HBRequest *)requestForGetGiftListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page;

+(HBRequest *)requestForGetGiftDetailByGiftID:(NSInteger)giftID;

@end

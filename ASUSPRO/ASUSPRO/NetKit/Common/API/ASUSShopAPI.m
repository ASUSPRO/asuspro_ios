//
//  ASUSShopAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSShopAPI.h"

@implementation ASUSShopAPI

+(HBRequest *)requestForGetShopIndexList
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getShopIndexList") parameters:nil];    
}
@end

//
//  ASUSOrderAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSOrderAPI : HBBaseAPI

+(HBRequest *)requestForUploadOrderByData:(NSString *)data;

+(HBRequest *)requestForGetOrderListByUserID:(NSInteger)userID  page:(NSInteger )page;

@end

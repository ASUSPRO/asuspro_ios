//
//  ASUSProductAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductAPI.h"

@implementation ASUSProductAPI

+(HBRequest *)requestForProductCategory
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/productCategory") parameters:nil];    
}
+(HBRequest *)requestForGetProductListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/productList?category_id=%ld&page=%ld",(long)categoryID,(long)page) parameters:nil];
}

@end

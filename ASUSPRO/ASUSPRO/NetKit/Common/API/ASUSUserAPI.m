//
//  ASUSUserAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSUserAPI.h"

@implementation ASUSUserAPI

+(HBRequest *)requestForUpdateAvatarByUserID:(NSInteger)userID
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/uploadUserPic?user_id=%ld",(long)userID) parameters:nil];
}

+(HBRequest *)requestForUpdateUserInfoByUserID:(NSInteger)userID name:(NSString *)name pay_password:(NSString *)payPassword birthdday:(NSString *)birthday mobile:(NSString *)mobile phone:(NSString *)phone
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/updateUserInfo?user_id=%ld&name=%@&pay_password=%@&birthday=%@&mobile=%@&phone=%@",(long)userID,name,payPassword,birthday,mobile,phone) parameters:nil];
}

+(HBRequest *)requestForCheckPayPassByUserID:(NSInteger)userID pay_password:(NSString *)pay_password
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/checkPayPass?user_id=%ld&pay_password=%@",(long)userID,pay_password) parameters:nil];
}
+(HBRequest *)requestForUserFeedBackByUserID:(NSInteger)userID userName:(NSString *)username feedBack:(NSString *)feedBack
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/userFeedback?user_id=%ld&username=%@&feedback=%@",(long)userID,username,feedBack) parameters:nil];
}

+(HBRequest *)requestForUserSendEmialByEmial:(NSString *)emial
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/reBackPass?emial=%@",emial) parameters:nil];
}

@end

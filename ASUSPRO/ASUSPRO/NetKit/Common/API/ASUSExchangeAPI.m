//
//  ASUSExchangeAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeAPI.h"

@implementation ASUSExchangeAPI

+(HBRequest *)requestForGetExchangeListByUserID:(NSInteger)userID page:(NSInteger)page
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getExchangeList?user_id=%ld&page=%ld",(long)userID,(long)page) parameters:nil];
}
@end

//
//  ASUSSearchAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/23.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSSearchAPI : HBBaseAPI

+(HBRequest *)requestForHomeSearchByKeyword:(NSString *)keyword;

+(HBRequest *)requestForGiftSearchByKeyword:(NSString *)keyword;
@end

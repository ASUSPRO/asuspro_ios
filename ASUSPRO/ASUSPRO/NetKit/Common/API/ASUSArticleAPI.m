//
//  ASUSArticleAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/21.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSArticleAPI.h"

@implementation ASUSArticleAPI

+(HBRequest *)requestForGetArticleCategoryByCategoryID:(NSInteger)categoryID
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getColumnList?cid=%ld",(long)categoryID) parameters:nil];
}
+(HBRequest *)requestForGetArticleListByArticleID:(NSInteger)articleID  page:(NSInteger )page
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/articleList?cid=%ld&page=%ld",(long)articleID,(long)page) parameters:nil];
}

+(HBRequest *)requestForGetArticleContentByArticleID:(NSInteger)articleID  contentId:(NSInteger)contentid
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getArticleContent?cid=%ld&id=%ld",(long)articleID,(long)contentid) parameters:nil];
}
@end

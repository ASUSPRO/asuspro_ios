//
//  ASUSArticleAPI.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/21.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBBaseAPI.h"

@interface ASUSArticleAPI : HBBaseAPI

+(HBRequest *)requestForGetArticleCategoryByCategoryID:(NSInteger)categoryID;

+(HBRequest *)requestForGetArticleListByArticleID:(NSInteger)articleID  page:(NSInteger )page;

+(HBRequest *)requestForGetArticleContentByArticleID:(NSInteger)articleID  contentId:(NSInteger )contentid;

@end

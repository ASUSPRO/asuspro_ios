//
//  HBBaseAPI.h
//  PodioKit
//
//  Created by Romain Briche on 28/01/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBRequest.h"
#import "BAClient.h"

@interface HBBaseAPI : NSObject

@end

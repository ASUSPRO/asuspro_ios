//
//  ASUSReceiveAddressAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSReceiveAddressAPI.h"

@implementation ASUSReceiveAddressAPI

+(HBRequest *)requestForAddReceiveAddressByUserID:(NSInteger)userID userName:(NSString *)username province:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default
{
     return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/addReceive?user_id=%ld&username=%@&province=%@&city=%@&area=%@&address=%@&zip=%@&mobile=%@&is_default=%ld",(long)userID,username,province,city,area,address,zip,mobile,(long)is_default) parameters:nil];
}
+(HBRequest *)requestForEditReceiveAddressByAddressID:(NSInteger)addressID userID:(NSInteger)userID userName:(NSString *)username province:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/editReceive?addr_id=%ld&user_id=%ld&username=%@&province=%@&city=%@&area=%@&address=%@&zip=%@&mobile=%@&is_default=%ld",(long)addressID,(long)userID,username,province,city,area,address,zip,mobile,(long)is_default) parameters:nil];
}

+(HBRequest *)requestForGetReceiveAddressByUserID:(NSInteger)userID
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/getReceive?user_id=%ld",(long)userID) parameters:nil];
}

+(HBRequest *)requestForDeleteReceiveAddressByUserID:(NSInteger)userID addressID:(NSInteger)addressId
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/deleteReceive?user_id=%ld&address_id=%ld",(long)userID,(long)addressId) parameters:nil];
}
@end

//
//  ASUSSearchAPI.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/23.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSSearchAPI.h"

@implementation ASUSSearchAPI

+(HBRequest *)requestForHomeSearchByKeyword:(NSString *)keyword
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/searchArticle?keyword=%@",keyword) parameters:nil];
}

+(HBRequest *)requestForGiftSearchByKeyword:(NSString *)keyword
{
    return [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/searchGift?keyword=%@",keyword) parameters:nil];
}
@end

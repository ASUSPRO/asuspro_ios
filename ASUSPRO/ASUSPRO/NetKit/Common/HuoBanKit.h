//
//  PodioKit.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 21/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAClient.h"

@interface HuoBanKit : NSObject

+ (void)authenticateTokenWithcompletion:(void (^)(NSError *error, NSString *message))completion;
+ (AFHTTPRequestOperation *)authenticateAsUserWithUserName:(NSString *)username password:(NSString *)password completion:(HBRequestCompletionBlock)completion;
+ (void)automaticallyStoreTokenInKeychainForCurrentApp;

+ (BOOL)isAuthenticated;

+ (void)logout;

@end

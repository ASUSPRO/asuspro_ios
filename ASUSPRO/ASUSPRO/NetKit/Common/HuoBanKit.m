//
//  PodioKit.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 21/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HuoBanKit.h"
#import "BAClient.h"

@implementation HuoBanKit

+ (void)authenticateTokenWithcompletion:(void (^)(NSError *error, NSString *message))completion{
    [[BAClient sharedClient] authenticateTokenWithCompletion:completion];
}

+ (BOOL)isAuthenticated {
    return [[BAClient sharedClient] isAuthenticated];
}

+ (AFHTTPRequestOperation *)authenticateAsUserWithUserName:(NSString *)username password:(NSString *)password completion:(HBRequestCompletionBlock)completion {
    return [[BAClient sharedClient] authenticateAsUserWithUserName:username password:password completion:completion];
}

+ (void)automaticallyStoreTokenInKeychainForCurrentApp {
  
}

+(void)logout {
    [[BAClient sharedClient] logout];
}

@end

//
//  BAClient.m
//  qudiaoyu
//
//  Created by abel on 14/11/25.
//  Copyright (c) 2014年 qudiaoyu.com.cn. All rights reserved.
//

#import "BAClient.h"
#import "HBRequestSerializer.h"
#import "BAAuthenticate.h"
#import "NSURLRequest+HBDescription.h"

#define kUserDefaultsCookie @"kUserDefaultsCookie"
static NSString * const kDefaultBaseURLString = @"http://www.ahaokang.com";

NSString * const authenLogoutNotification = @"GFAuthenLogoutNotification";
NSString * const authenticatedSuccessNotification = @"GFAuthenticatedSuccessNotification";

@interface BAClient ()

@property (nonatomic, strong) HBRequestSerializer * requestSerializer;
@property (nonatomic, strong) NSArray *cookies;
@property (nonatomic, strong) BAAuthenticate * authenticate;

@end


@implementation BAClient

+ (instancetype)sharedClient {
    static BAClient *sharedClient;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        sharedClient = [self new];
    });
    
    return sharedClient;
}

- (id)init {
    @synchronized(self) {
        self = [super init];
        if (!self) return nil;
        NSURL *baseURL = [[NSURL alloc] initWithString:kDefaultBaseURLString];
        self = [super initWithBaseURL:baseURL];
       
        self.requestSerializer = [HBRequestSerializer serializer];
        
        NSArray *responseSerializers = @[[AFJSONResponseSerializer serializer],
                                         [AFHTTPResponseSerializer serializer]];
        self.responseSerializer = [AFCompoundResponseSerializer compoundSerializerWithResponseSerializers:responseSerializers];
        self.authenticate = [BAAuthenticate sharedClient];
        [self setCookie];
        return self;
    }
}


- (void)cookie {
    self.cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:kDefaultBaseURLString]];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.cookies];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserDefaultsCookie];
}

- (void)setCookie {
    NSData *cookiesdata = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsCookie];
    if([cookiesdata length]) {
        self.cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookiesdata];
        for (NSHTTPCookie * cookie in self.cookies) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
    }
}

- (void)dealloc {
}

#pragma mark - Public

- (BOOL)isAuthenticated {
    return self.authenticate.userId != 0;
}

- (void)logout {
    self.authenticate.userId = 0;
    self.authenticate.token = nil;
    self.authenticate.score = nil;
    self.authenticate.userName = nil;
    self.authenticate.address = nil;
    self.authenticate.avatarLink = nil;
    self.authenticate.name = nil;
    self.authenticate.email = nil;
    self.authenticate.userType = nil;
    self.authenticate.gender = nil;
    self.authenticate.password = nil;
    self.authenticate.mobile = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultsCookie];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSNotificationCenter defaultCenter] postNotificationName:authenLogoutNotification object:nil];
}

- (void)authenticateTokenWithCompletion:(void (^)(NSError *error, NSString *message))completion {
    HBRequest * request = [HBRequest POSTRequestWithPath:HBRequestPath(@"/Interface/checkUserToken?token=%@",self.authenticate.token) parameters:nil];
    [self performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"token:%@",response.body);
        if (error == nil && [response.body[@"ResultCode"] integerValue] == 0) {
            self.authenticate.userId = response.body[@"ResultContent"][@"user_id"];
            self.authenticate.userName = response.body[@"ResultContent"][@"username"];
            self.authenticate.token = response.body[@"ResultContent"][@"token"];
        }
        if (completion) {
            completion(error,response.body[@"ResultMsg"]);
        }
    }];
}

- (AFHTTPRequestOperation *)authenticateAsUserWithUserName:(NSString *)username password:(NSString *)password completion:(HBRequestCompletionBlock)completion {
    NSParameterAssert(username);
    NSParameterAssert(password);

    [self loginWithUserName:username password:password completion:completion];

//    if (self.authenticate.formhash) {
//        [self loginWithEmail:username password:password completion:completion];
//    } else {
//        [self authenticateCookieWithCompletion:^(NSError *error) {
//            if (error == nil) {
//                
//            } else {
//                completion(nil,error);
//            }
//        }];
//    }
//    
   return nil;
}

- (AFHTTPRequestOperation *)loginWithUserName:(NSString *)username password:(NSString *)password completion:(HBRequestCompletionBlock)completion
{
    HBRequest *request = [HBRequest POSTRequestWithPath:[NSString stringWithFormat:@"http://www.ahaokang.com/Interface/userLogin?account=%@&password=%@",username,password] parameters:nil];
    request.contentType =  HBRequestContentTypeJSON;
    return [self performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"login:%@",response.body);
        if(error == nil && [response.body[@"ResultContent"] isKindOfClass:[NSDictionary class]]) {
            if ([response.body[@"ResultCode"] integerValue] == 0) {
                self.authenticate.userType = response.body[@"ResultContent"][@"user_type"];
                self.authenticate.token = response.body[@"ResultContent"][@"token"];
                self.authenticate.userId = response.body[@"ResultContent"][@"id"];
                self.authenticate.userName = response.body[@"ResultContent"][@"username"];
                self.authenticate.gender = response.body[@"ResultContent"][@"gender"];
                self.authenticate.mobile = response.body[@"ResultContent"][@"mobile"];
                self.authenticate.avatarLink = response.body[@"ResultContent"][@"pic_url"];
                self.authenticate.password = response.body[@"ResultContent"][@"password"];
                self.authenticate.score = response.body[@"ResultContent"][@"score"];
                self.authenticate.email = response.body[@"ResultContent"][@"email"];
                self.authenticate.name = response.body[@"ResultContent"][@"name"];
                self.authenticate.address = response.body[@"ResultContent"][@"address"];
                self.authenticate.brithday = response.body[@"ResultContent"][@"birthday"];
                self.authenticate.phone = response.body[@"ResultContent"][@"phone"];
                self.authenticate.company = response.body[@"ResultContent"][@"company"];
                self.authenticate.dealerId = response.body[@"ResultContent"][@"dealer_id"];
                self.authenticate.set_pay_password = response.body[@"ResultContent"][@"set_pay_password"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:authenticatedSuccessNotification object:nil];
                completion(response,nil);
            } else {
                error = [NSError errorWithDomain:self.baseURL.absoluteString code:303 userInfo:@{@"message" : response.body[@"ResultMsg"]}];
                completion(nil,error);
                
            }
        } else {
            if (completion) {
                if (error == nil) {
                    error = [NSError errorWithDomain:self.baseURL.absoluteString code:303 userInfo:@{@"message" : @"登录失败"}];
                }
                completion(nil,error);
            }
        }
    }];
}

- (AFHTTPRequestOperation *)performRequest:(HBRequest *)request completion:(HBRequestCompletionBlock)completion {
    NSURLRequest *urlRequest = [(HBRequestSerializer *)self.requestSerializer URLRequestForRequest:request relativeToURL:self.baseURL];
    
//    debug(@"%@",[urlRequest hb_description]);
    
    
    
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:urlRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSUInteger statusCode = operation.response.statusCode;
        HBResponse *response = [[HBResponse alloc] initWithStatusCode:statusCode body:responseObject];
        
        if (completion) completion(response, nil);
        [self cookie];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSUInteger statusCode = operation.response.statusCode;
        HBResponse *response = [[HBResponse alloc] initWithStatusCode:statusCode body:operation.responseObject];
        
        if (completion) completion(response, error);
    }];
    
    // If this is a download request with a provided local file path, configure an output stream instead
    // of buffering the data in memory.
    if (request.method == HBRequestMethodGET && request.fileData.filePath) {
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:request.fileData.filePath append:NO];
    }
    
    [operation start];
    
    return operation;
}


@end

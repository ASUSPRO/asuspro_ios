//
//  BACookie.h
//  qudiaoyu
//
//  Created by abel on 14/11/26.
//  Copyright (c) 2014年 qudiaoyu.com.cn. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BAAuthenticate : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *avatarLink;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *brithday;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *dealerId;
@property (nonatomic, strong) NSString *set_pay_password;

@property (nonatomic, strong) NSString *payPassword;

+ (instancetype)sharedClient;

@end

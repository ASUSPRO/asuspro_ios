//
//  HBRequest.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 16/01/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBRequestFileData.h"

#define HBRequestPath(fmt, ...) [NSString stringWithFormat:fmt, ##__VA_ARGS__]
#define TestHBRequestPath(fmt) [NSString stringWithFormat:@"/gwl/changping%@",fmt]

typedef NSURLRequest * (^HBURLRequestConfigurationBlock) (NSURLRequest *request);

typedef NS_ENUM(NSUInteger, HBRequestMethod) {
  HBRequestMethodGET = 0,
  HBRequestMethodPOST,
};

typedef NS_ENUM(NSUInteger, HBRequestContentType) {
  HBRequestContentTypeJSON = 0,
  HBRequestContentTypeFormURLEncoded,
  HBRequestContentTypeMultipart
};

@interface HBRequest : NSObject

@property (nonatomic, assign, readonly) HBRequestMethod method;
@property (nonatomic, copy, readonly) NSString *path;
@property (nonatomic, copy, readonly) NSURL *URL;
@property (nonatomic, copy) NSDictionary *parameters;
@property (nonatomic, strong) HBRequestFileData *fileData;
@property (nonatomic, assign, readwrite) HBRequestContentType contentType;
@property (nonatomic, copy, readwrite) HBURLRequestConfigurationBlock URLRequestConfigurationBlock;

+ (instancetype)GETRequestWithPath:(NSString *)path parameters:(NSDictionary *)parameters;
+ (instancetype)POSTRequestWithPath:(NSString *)path parameters:(NSDictionary *)parameters;

+ (instancetype)GETRequestWithURL:(NSURL *)url parameters:(NSDictionary *)parameters;
+ (instancetype)POSTRequestWithURL:(NSURL *)url parameters:(NSDictionary *)parameters;

@end

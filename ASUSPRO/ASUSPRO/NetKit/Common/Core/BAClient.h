//
//  BAClient.h
//  qudiaoyu
//
//  Created by abel on 14/11/25.
//  Copyright (c) 2014年 qudiaoyu.com.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "HBRequest.h"
#import "HBResponse.h"

typedef void(^HBRequestCompletionBlock)(HBResponse *response, NSError *error);

extern NSString * const authenLogoutNotification;
extern NSString * const authenticatedSuccessNotification;

@interface BAClient : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;


- (BOOL)isAuthenticated;
- (void)logout;

- (void)authenticateTokenWithCompletion:(void (^)(NSError *error, NSString *message))completion;

- (AFHTTPRequestOperation *)authenticateAsUserWithUserName:(NSString *)username password:(NSString *)password completion:(HBRequestCompletionBlock)completion;
- (AFHTTPRequestOperation *)performRequest:(HBRequest *)request completion:(HBRequestCompletionBlock)completion;

@end

//
//  HBRequestSerializer.m
//  PodioKit
//
//  Created by Romain Briche on 22/01/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBRequestSerializer.h"
#import "NSString+HBRandom.h"
#import "HBRequest.h"

static NSString * const kHTTPMethodGET = @"GET";
static NSString * const kHTTPMethodPOST = @"POST";

@interface HBRequestSerializer ()

@property (nonatomic, assign) HBRequestContentType requestContentType;

@end

@implementation HBRequestSerializer

- (NSMutableURLRequest *)URLRequestForRequest:(HBRequest *)request relativeToURL:(NSURL *)baseURL {
    @synchronized(self) {
        NSParameterAssert(request);
        NSParameterAssert(baseURL);
        
        NSURL *url = nil;
        if (request.URL) {
            url = request.URL;
        } else {
            NSParameterAssert(request.path);
            url = [NSURL URLWithString:request.path relativeToURL:baseURL];
            
        }
        
        NSMutableURLRequest *urlRequest = nil;
        if (request.method == HBRequestMethodGET) {
            urlRequest = [NSMutableURLRequest requestWithURL:url];
        } else if (request.method == HBRequestMethodPOST) {
            
            if (request.contentType == HBRequestContentTypeMultipart) {
                urlRequest = [self multipartFormRequestWithMethod:@"POST" URLString:url.absoluteString parameters:request.parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                    if (request.fileData.data) {
//                        [formData appendPartWithFormData:request.fileData.data name:@"Filedata" ];
                        [formData appendPartWithFileData:request.fileData.data name:@"pic_url" fileName:@"avatar.jpg" mimeType:@"image/png"];
                    }
                } error:nil];
            }else {
                HBRequestContentType contentType = self.requestContentType;
                self.requestContentType = request.contentType;
                NSString *path = [request.path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                urlRequest = [self requestWithMethod:@"POST" URLString:[[NSURL URLWithString:path relativeToURL:baseURL] absoluteString] parameters:request.parameters error:nil];
                if (request.fileData.data) {
                    urlRequest.HTTPBody = request.fileData.data;
                }
                self.requestContentType = contentType;
            }
        }
        
        if (request.URLRequestConfigurationBlock) {
            urlRequest = [request.URLRequestConfigurationBlock(urlRequest) mutableCopy];
        }
        
        return urlRequest;
    }
}

@end

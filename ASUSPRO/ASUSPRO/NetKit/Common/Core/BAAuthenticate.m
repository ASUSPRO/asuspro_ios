//
//  BACookie.m
//  qudiaoyu
//
//  Created by abel on 14/11/26.
//  Copyright (c) 2014年 qudiaoyu.com.cn. All rights reserved.
//

#import "BAAuthenticate.h"



@implementation BAAuthenticate

+ (instancetype)sharedClient {
    static BAAuthenticate *sharedAuthenticate;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        sharedAuthenticate = [self new];
    });
    
    return sharedAuthenticate;
}


-(instancetype)init {
    if (self = [super init]) {
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        _userId = [userDefaults objectForKey:@"userId"];
        _userType = [userDefaults objectForKey:@"userType"];
        _token = [userDefaults objectForKey:@"token"];
        _userName = [userDefaults objectForKey:@"userName"];
        _address = [userDefaults objectForKey:@"address"];
        _email = [userDefaults objectForKey:@"email"];
        _name = [userDefaults objectForKey:@"name"];
        _score = [userDefaults objectForKey:@"score"];
        _password = [userDefaults objectForKey:@"password"];
        _avatarLink = [userDefaults objectForKey:@"avatarLink"];
        _mobile = [userDefaults objectForKey:@"mobile"];
        _gender = [userDefaults objectForKey:@"gender"];
        _brithday = [userDefaults objectForKey:@"birthday"];
        _phone = [userDefaults objectForKey:@"phone"];
        _company = [userDefaults objectForKey:@"company"];
        _dealerId = [userDefaults objectForKey:@"dealerId"];
        _payPassword = [userDefaults objectForKey:@"paypassword"];
        _set_pay_password = [userDefaults objectForKey:@"setpaypassword"];
    }
    return self;
}

- (void)setUserId:(NSString *)userId{
    _userId = userId;
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
}

- (void)setToken:(NSString *)token {
    _token = token;
    if (token.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
    }
    
}

- (void)setUserName:(NSString *)userName {
    _userName = userName;
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"userName"];
}

- (void)setUserType:(NSString *)userType
{
    _userType = userType;
     [[NSUserDefaults standardUserDefaults] setObject:_userType forKey:@"userType"];
}

- (void)setGender:(NSString *)gender
{
    _gender = gender;
    [[NSUserDefaults standardUserDefaults] setObject:gender forKey:@"gender"];
}

- (void)setMobile:(NSString *)mobile
{
    _mobile = mobile;
    [[NSUserDefaults standardUserDefaults] setObject:mobile forKey:@"mobile"];
}

- (void)setAvatarLink:(NSString *)avatarLink
{
    _avatarLink = avatarLink;
    [[NSUserDefaults standardUserDefaults] setObject:avatarLink forKey:@"avatarLink"];
}

- (void)setPassword:(NSString *)password
{
    _password = password;
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
}
- (void)setScore:(NSString *)score
{
    _score = score;
    [[NSUserDefaults standardUserDefaults] setObject:score forKey:@"score"];
}
- (void)setAddress:(NSString *)address
{
    _address = address;
    [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"address"];
}

- (void)setEmail:(NSString *)email
{
    _email = email;
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
}

- (void)setName:(NSString *)name
{
    _name = name;
     [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"name"];
}

- (void)setBrithday:(NSString *)brithday
{
    _brithday = brithday;
    [[NSUserDefaults standardUserDefaults] setObject:brithday forKey:@"birthday"];
}

- (void)setCompany:(NSString *)company
{
    _company = company;
    [[NSUserDefaults standardUserDefaults] setObject:company forKey:@"company"];
}

- (void)setPhone:(NSString *)phone
{
    _phone = phone;
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"phone"];
}

- (void)setDealerId:(NSString *)dealerId
{
    _dealerId = dealerId;
    [[NSUserDefaults standardUserDefaults] setObject:dealerId forKey:@"dealerId"];
}

- (void)setPayPassword:(NSString *)payPassword
{
    _payPassword = payPassword;
    [[NSUserDefaults standardUserDefaults] setObject:payPassword forKey:@"paypassword"];
}

- (void)setSet_pay_password:(NSString *)set_pay_password
{
    _set_pay_password = set_pay_password;
    [[NSUserDefaults standardUserDefaults] setObject:set_pay_password forKey:@"setpaypassword"];
}

@end

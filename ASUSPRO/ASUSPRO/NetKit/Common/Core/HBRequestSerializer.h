//
//  HBRequestSerializer.h
//  PodioKit
//
//  Created by Romain Briche on 22/01/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <AFNetworking/AFURLRequestSerialization.h>

@class HBRequest;

@interface HBRequestSerializer : AFJSONRequestSerializer

- (NSMutableURLRequest *)URLRequestForRequest:(HBRequest *)request relativeToURL:(NSURL *)baseURL;

@end

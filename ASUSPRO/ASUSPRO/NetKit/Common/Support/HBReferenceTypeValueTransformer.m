//
//  HBReferenceTypeValueTransformer.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 22/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBReferenceTypeValueTransformer.h"
#import "HBConstants.h"

@implementation HBReferenceTypeValueTransformer

- (instancetype)init {
  return [super initWithDictionary:@{
    @"app" : @(HBReferenceTypeApp),
    @"app_revision" : @(HBReferenceTypeAppRevision),
    @"app_field" : @(HBReferenceTypeAppField),
    @"item" : @(HBReferenceTypeItem),
    @"bulletin" : @(HBReferenceTypeBulletin),
    @"comment" : @(HBReferenceTypeComment),
    @"status" : @(HBReferenceTypeStatus),
    @"space_member" : @(HBReferenceTypeSpaceMember),
    @"alert" : @(HBReferenceTypeAlert),
    @"item_revision" : @(HBReferenceTypeItemRevision),
    @"rating" : @(HBReferenceTypeRating),
    @"task" : @(HBReferenceTypeTask),
    @"task_action" : @(HBReferenceTypeTaskAction),
    @"space" : @(HBReferenceTypeSpace),
    @"org" : @(HBReferenceTypeOrg),
    @"conversation" : @(HBReferenceTypeConversation),
    @"message" : @(HBReferenceTypeMessage),
    @"notification" : @(HBReferenceTypeNotification),
    @"file" : @(HBReferenceTypeFile),
    @"file_service" : @(HBReferenceTypeFileService),
    @"profile" : @(HBReferenceTypeProfile),
    @"user" : @(HBReferenceTypeUser),
    @"widget" : @(HBReferenceTypeWidget),
    @"share" : @(HBReferenceTypeShare),
    @"form" : @(HBReferenceTypeForm),
    @"auth_client" : @(HBReferenceTypeAuthClient),
    @"connection" : @(HBReferenceTypeConnection),
    @"integration" : @(HBReferenceTypeIntegration),
    @"share_install" : @(HBReferenceTypeShareInstall),
    @"icon" : @(HBReferenceTypeIcon),
    @"org_member" : @(HBReferenceTypeOrgMember),
    @"news" : @(HBReferenceTypeNews),
    @"hook" : @(HBReferenceTypeHook),
    @"tag" : @(HBReferenceTypeTag),
    @"embed" : @(HBReferenceTypeEmbed),
    @"question" : @(HBReferenceTypeQuestion),
    @"question_answer" : @(HBReferenceTypeQuestionAnswer),
    @"action" : @(HBReferenceTypeAction),
    @"contract" : @(HBReferenceTypeContract),
    @"meeting" : @(HBReferenceTypeMeeting),
    @"batch" : @(HBReferenceTypeBatch),
    @"system" : @(HBReferenceTypeSystem),
    @"space_member_request" : @(HBReferenceTypeSpaceMemberRequest),
    @"live" : @(HBReferenceTypeLive),
    @"item_participation" : @(HBReferenceTypeItemParticipation),
  }];
}

@end

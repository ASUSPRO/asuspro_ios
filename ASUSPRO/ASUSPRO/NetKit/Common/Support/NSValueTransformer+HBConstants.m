//
//  NSValueTransformer+HBConstants.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 11/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "NSValueTransformer+HBConstants.h"
#import "NSValueTransformer+HBTransformers.h"

@implementation NSValueTransformer (HBConstants)

+ (HBReferenceType)hb_referenceTypeFromString:(NSString *)string {
  id referenceTypeValue = [[NSValueTransformer hb_referenceTypeTransformer] transformedValue:string];

  HBReferenceType referenceType = HBReferenceTypeNone;
  if ([referenceTypeValue isKindOfClass:[NSNumber class]]) {
    referenceType = [referenceTypeValue unsignedIntegerValue];
  }

  return referenceType;
}

+ (NSString *)hb_stringFromReferenceType:(HBReferenceType)referenceType {
  return [[NSValueTransformer hb_referenceTypeTransformer] reverseTransformedValue:@(referenceType)];
}

@end

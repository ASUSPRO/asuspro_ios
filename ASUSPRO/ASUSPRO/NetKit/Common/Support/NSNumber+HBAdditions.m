//
//  NSNumber(HBAdditions)
//  PodioKit
//
//  Created by Sebastian Rehnby on 18/05/14
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//


#import "NSNumber+HBAdditions.h"
#import "NSNumberFormatter+HBAdditions.h"

static NSNumberFormatter *sNumberFormatter = nil;

@implementation NSNumber (HBAdditions)

+ (NSNumber *)hb_numberFromUSNumberString:(NSString *)numberString {
  return [[self hb_USNumberFormatter] numberFromString:numberString];
}

- (NSString *)hb_USNumberString {
  return [[[self class] hb_USNumberFormatter] stringFromNumber:self];
}

+ (NSNumberFormatter *)hb_USNumberFormatter {
  if (!sNumberFormatter) {
    sNumberFormatter = [NSNumberFormatter hb_USNumberFormatter];
  }

  return sNumberFormatter;
}

@end
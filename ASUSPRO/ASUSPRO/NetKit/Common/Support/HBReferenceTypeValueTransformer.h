//
//  HBReferenceTypeValueTransformer.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 22/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBDictionaryMappingValueTransformer.h"

@interface HBReferenceTypeValueTransformer : HBDictionaryMappingValueTransformer

@end

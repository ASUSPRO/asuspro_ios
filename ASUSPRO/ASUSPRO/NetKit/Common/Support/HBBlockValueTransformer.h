//
//  HBBlockValueTransformer.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 14/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id (^HBValueTransformationBlock) (id value);

@interface HBBlockValueTransformer : NSValueTransformer

- (instancetype)initWithBlock:(HBValueTransformationBlock)block;

+ (instancetype)transformerWithBlock:(HBValueTransformationBlock)block;

@end

//
//  HBConstants.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 22/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#ifndef PodioKit_HBConstants_h
#define PodioKit_HBConstants_h

typedef NS_ENUM(NSUInteger, HBReferenceType) {
  HBReferenceTypeNone = 0,
  HBReferenceTypeApp,
  HBReferenceTypeAppRevision,
  HBReferenceTypeAppField,
  HBReferenceTypeItem,
  HBReferenceTypeBulletin,
  HBReferenceTypeComment,
  HBReferenceTypeStatus,
  HBReferenceTypeSpaceMember,
  HBReferenceTypeAlert,
  HBReferenceTypeItemRevision,
  HBReferenceTypeRating,
  HBReferenceTypeTask,
  HBReferenceTypeTaskAction,
  HBReferenceTypeSpace,
  HBReferenceTypeOrg,
  HBReferenceTypeConversation,
  HBReferenceTypeMessage,
  HBReferenceTypeNotification,
  HBReferenceTypeFile,
  HBReferenceTypeFileService,
  HBReferenceTypeProfile,
  HBReferenceTypeUser,
  HBReferenceTypeWidget,
  HBReferenceTypeShare,
  HBReferenceTypeForm,
  HBReferenceTypeAuthClient,
  HBReferenceTypeConnection,
  HBReferenceTypeIntegration,
  HBReferenceTypeShareInstall,
  HBReferenceTypeIcon,
  HBReferenceTypeOrgMember,
  HBReferenceTypeNews,
  HBReferenceTypeHook,
  HBReferenceTypeTag,
  HBReferenceTypeEmbed,
  HBReferenceTypeQuestion,
  HBReferenceTypeQuestionAnswer,
  HBReferenceTypeAction,
  HBReferenceTypeContract,
  HBReferenceTypeMeeting,
  HBReferenceTypeBatch,
  HBReferenceTypeSystem,
  HBReferenceTypeSpaceMemberRequest,
  HBReferenceTypeLive,
  HBReferenceTypeItemParticipation
};

#endif

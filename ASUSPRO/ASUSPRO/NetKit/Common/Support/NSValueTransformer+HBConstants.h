//
//  NSValueTransformer+HBConstants.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 11/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBConstants.h"

@interface NSValueTransformer (HBConstants)

+ (HBReferenceType)hb_referenceTypeFromString:(NSString *)string;
+ (NSString *)hb_stringFromReferenceType:(HBReferenceType)referenceType;

@end

//
//  HBReversibleBlockValueTransformer.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 14/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBBlockValueTransformer.h"

@interface HBReversibleBlockValueTransformer : HBBlockValueTransformer

- (instancetype)initWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock;

+ (instancetype)transformerWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock;

@end

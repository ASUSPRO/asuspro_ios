//
//  HBDateValueTransformer.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 02/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBDateValueTransformer.h"
#import "NSDate+HBAdditions.h"

@implementation HBDateValueTransformer

- (instancetype)init {
  return [super initWithBlock:^id(NSString *dateString) {
    return [NSDate hb_dateFromUTCDateTimeString:dateString];
  } reverseBlock:^id(NSDate *date) {
    return [date hb_UTCDateTimeString];
  }];
}

@end

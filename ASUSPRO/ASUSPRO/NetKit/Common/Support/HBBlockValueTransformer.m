//
//  HBBlockValueTransformer.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 14/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBBlockValueTransformer.h"

@interface HBBlockValueTransformer ()

@property (nonatomic, copy) HBValueTransformationBlock transformBlock;

@end

@implementation HBBlockValueTransformer

- (instancetype)init {
  return [self initWithBlock:nil];
}

- (instancetype)initWithBlock:(HBValueTransformationBlock)block {
  self = [super init];
  if (!self) return nil;
  
  _transformBlock = [block copy];
  
  return self;
}

+ (instancetype)transformerWithBlock:(HBValueTransformationBlock)block {
  return [[self alloc] initWithBlock:block];
}

#pragma mark - NSValueTransformer

+ (BOOL)allowsReverseTransformation {
  return NO;
}

- (id)transformedValue:(id)value {
  return self.transformBlock ? self.transformBlock(value) : nil;
}

@end

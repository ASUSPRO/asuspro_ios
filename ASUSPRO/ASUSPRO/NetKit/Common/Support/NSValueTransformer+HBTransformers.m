//
//  NSValueTransformer+HBTransformers.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 15/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "NSValueTransformer+HBTransformers.h"
//#import "HBAppField.h"
#import "HBConstants.h"
#import "HBNumberValueTransformer.h"

@implementation NSValueTransformer (HBTransformers)

+ (NSValueTransformer *)hb_transformerWithBlock:(HBValueTransformationBlock)block {
  return [HBBlockValueTransformer transformerWithBlock:block];
}

+ (NSValueTransformer *)hb_transformerWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock {
  return [HBReversibleBlockValueTransformer transformerWithBlock:block reverseBlock:reverseBlock];
}

+ (NSValueTransformer *)hb_transformerWithModelClass:(Class)modelClass {
  return [HBModelValueTransformer transformerWithModelClass:modelClass];
}

+ (NSValueTransformer *)hb_transformerWithDictionary:(NSDictionary *)dictionary {
  return [HBDictionaryMappingValueTransformer transformerWithDictionary:dictionary];
}

+ (NSValueTransformer *)hb_URLTransformer {
  return [HBURLValueTransformer new];
}

+ (NSValueTransformer *)hb_dateValueTransformer {
  return [HBDateValueTransformer new];
}

+ (NSValueTransformer *)hb_referenceTypeTransformer {
  return [HBReferenceTypeValueTransformer new];
}

+ (NSValueTransformer *)hb_appFieldTypeTransformer {
  return [HBAppFieldTypeValueTransformer new];
}

+ (NSValueTransformer *)hb_numberValueTransformer {
  return [HBNumberValueTransformer new];
}
@end

//
//  HBReversibleBlockValueTransformer.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 14/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBReversibleBlockValueTransformer.h"

@interface HBReversibleBlockValueTransformer ()

@property (nonatomic, copy) HBValueTransformationBlock reverseBlock;

@end

@implementation HBReversibleBlockValueTransformer

- (instancetype)init {
  return [self initWithBlock:nil reverseBlock:nil];
}

- (instancetype)initWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock {
  self = [super initWithBlock:block];
  if (!self) return nil;
  
  _reverseBlock = [reverseBlock copy];
  
  return self;
}

+ (instancetype)transformerWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock {
  return [[self alloc] initWithBlock:block reverseBlock:reverseBlock];
}

#pragma mark - NSValueTransformer

+ (BOOL)allowsReverseTransformation {
  return YES;
}

- (id)reverseTransformedValue:(id)value {
  return self.reverseBlock ? self.reverseBlock(value) : nil;
}

@end

//
//  NSDate+HBAdditions.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 08/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "NSDate+HBAdditions.h"
#import "NSDateFormatter+HBAdditions.h"

static NSDateFormatter *sUTCDateFormatter = nil;
static NSDateFormatter *sUTCDateTimeFormatter = nil;

@implementation NSDate (HBAdditions)

#pragma mark - Public

+ (NSDate *)hb_dateFromUTCDateString:(NSString *)dateString {
  return [[self UTCDateFormatter] dateFromString:dateString];
}

+ (NSDate *)hb_dateFromUTCDateTimeString:(NSString *)dateTimeString {
  return [[self UTCDateTimeFormatter] dateFromString:dateTimeString];
}

- (NSString *)hb_UTCDateString {
  return [[[self class] UTCDateFormatter] stringFromDate:self];
}

- (NSString *)hb_UTCDateTimeString {
  return [[[self class] UTCDateTimeFormatter] stringFromDate:self];
}

#pragma mark - Private

+ (NSDateFormatter *)UTCDateFormatter {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sUTCDateFormatter = [NSDateFormatter hb_UTCDateFormatter];
  });
  
  return sUTCDateFormatter;
}

+ (NSDateFormatter *)UTCDateTimeFormatter {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sUTCDateTimeFormatter = [NSDateFormatter hb_UTCDateTimeFormatter];
  });
  
  return sUTCDateTimeFormatter;
}

@end

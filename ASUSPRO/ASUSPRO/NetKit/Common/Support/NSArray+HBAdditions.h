//
//  NSArray+HBAdditions.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 01/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (HBAdditions)

- (NSArray *)hb_mappedArrayWithBlock:(id (^)(id obj))block;
- (NSArray *)hb_filteredArrayWithBlock:(BOOL (^)(id obj))block;
- (id)hb_firstObjectPassingTest:(BOOL (^)(id obj))block;

@end

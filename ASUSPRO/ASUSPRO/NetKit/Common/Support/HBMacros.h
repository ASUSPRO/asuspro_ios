//
//  HBMacros.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 15/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

// References
#define HB_STRONG(obj) __typeof__(obj)
#define HB_WEAK(obj) __typeof__(obj) __weak
#define HB_WEAK_SELF HB_WEAK(self)


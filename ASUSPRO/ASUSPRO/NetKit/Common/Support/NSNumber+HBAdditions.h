//
//  NSNumber(HBAdditions) 
//  PodioKit
//
//  Created by Sebastian Rehnby on 18/05/14
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSNumber (HBAdditions)

+ (NSNumber *)hb_numberFromUSNumberString:(NSString *)numberString;
- (NSString *)hb_USNumberString;

@end
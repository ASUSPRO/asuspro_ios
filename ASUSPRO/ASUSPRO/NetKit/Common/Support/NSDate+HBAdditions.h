//
//  NSDate+HBAdditions.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 08/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (HBAdditions)

+ (NSDate *)hb_dateFromUTCDateString:(NSString *)dateString;
+ (NSDate *)hb_dateFromUTCDateTimeString:(NSString *)dateTimeString;

- (NSString *)hb_UTCDateString;
- (NSString *)hb_UTCDateTimeString;

@end

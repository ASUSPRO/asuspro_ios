//
//  NSValueTransformer+HBTransformers.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 15/04/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBBlockValueTransformer.h"
#import "HBReversibleBlockValueTransformer.h"
#import "HBModelValueTransformer.h"
#import "HBURLValueTransformer.h"
#import "HBReferenceTypeValueTransformer.h"
#import "HBAppFieldTypeValueTransformer.h"
#import "HBDateValueTransformer.h"

@interface NSValueTransformer (HBTransformers)

+ (NSValueTransformer *)hb_transformerWithBlock:(HBValueTransformationBlock)block;
+ (NSValueTransformer *)hb_transformerWithBlock:(HBValueTransformationBlock)block reverseBlock:(HBValueTransformationBlock)reverseBlock;
+ (NSValueTransformer *)hb_transformerWithModelClass:(Class)modelClass;

+ (NSValueTransformer *)hb_transformerWithDictionary:(NSDictionary *)dictionary;
+ (NSValueTransformer *)hb_URLTransformer;
+ (NSValueTransformer *)hb_dateValueTransformer;
+ (NSValueTransformer *)hb_referenceTypeTransformer;
+ (NSValueTransformer *)hb_appFieldTypeTransformer;
+ (NSValueTransformer *)hb_numberValueTransformer;

@end

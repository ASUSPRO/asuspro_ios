//
//  ASUSAddressModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSAddressModel : HBObject

+(void)fetchProvinceWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchCityByCode:(NSInteger)cityCode  completion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchAreaByCode:(NSInteger)areaCode  completion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchAddressListByUserId:(NSInteger )userId Completion:(void (^)(NSError *error, NSArray *list))completion;

@end

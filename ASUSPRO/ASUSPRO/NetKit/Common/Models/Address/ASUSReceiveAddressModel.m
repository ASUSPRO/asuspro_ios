//
//  ASUSReceiveAddressModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSReceiveAddressModel.h"
#import "ASUSReceiveAddressAPI.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSReceiveAddressModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames
{
    return @{@"addressId":@"id",
             @"userId":@"user_id",
             @"userName":@"username",
             @"mobile":@"mobile",
             @"province":@"province",
             @"city":@"city",
             @"area":@"area",
             @"address":@"address",
             @"postCode":@"zip",
             @"is_default":@"is_default"
             };
}

+(void)addReceiveAddressByUserID:(NSInteger)userID userName:(NSString *)username provinceCode:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default completion:(void (^)(NSError *error,  NSString *message))completion
{
    HBRequest * request = [ASUSReceiveAddressAPI requestForAddReceiveAddressByUserID:userID userName:username province:province city:city area:area address:address zip:zip mobile:mobile isDefault:is_default];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"AddReceiveAddress:%@",response.body);
        NSString * messageVal = nil;
        if(error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            messageVal = response.body[@"ResultMsg"];
        }
        if (completion) {
            completion(error, messageVal);
        }
    }];
}

+(void)editReceiveAddressByAddressID:(NSInteger)addressID userID:(NSInteger)userID userName:(NSString *)username provinceCode:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default completion:(void (^)(NSError *error, NSString *message))completion
{
    HBRequest * request = [ASUSReceiveAddressAPI requestForEditReceiveAddressByAddressID:addressID userID:userID userName:username province:province city:city area:area address:address zip:zip mobile:mobile isDefault:is_default];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"editReceiveAddress:%@",response.body);
        NSString * messageVal = nil;
        if(error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            messageVal = response.body[@"ResultMsg"];
        }
        if (completion) {
            completion(error, messageVal);
        }
    }];
 
}
+(void)fetchReceiveAddressByUserID:(NSInteger)userID completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSReceiveAddressAPI requestForGetReceiveAddressByUserID:userID];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"ReceiveAddressList:%@",response.body);
        NSArray * receiveAddressArr = nil;
        if (error == nil&&[response.body[@"ResultCode"] integerValue] == 0&&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            receiveAddressArr = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *receiveAddressDictionary) {
                return [[self alloc] initWithDictionary:receiveAddressDictionary];
            }];
        }
        if (completion) {
            completion(error,receiveAddressArr);
        }
        
    }];
}

+(void)deleteReceiveAddressByUserID:(NSInteger)userID addressID:(NSInteger)addressId completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSReceiveAddressAPI requestForDeleteReceiveAddressByUserID:userID addressID:addressId];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"deleteAddress:%@",response.body);
        NSArray * receiveAddressArr = nil;
        if (completion) {
            completion(error,receiveAddressArr);
        }
        
    }];
}


@end

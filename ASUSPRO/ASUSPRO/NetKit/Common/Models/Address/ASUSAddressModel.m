//
//  ASUSAddressModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSAddressModel.h"
#import "ASUSAddressAPI.h"
#import "ASUSReceiveAddressAPI.h"

@implementation ASUSAddressModel

+(void)fetchProvinceWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSAddressAPI requestProvince];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"province:%@",response.body);
//        NSArray * blogAricles = nil;
//        if (error == nil) {
//            blogAricles = [response.body[@"Variables"][@"postlist"] hb_mappedArrayWithBlock:^id(NSDictionary *blogAricleDictionary) {
//                return [[self alloc] initWithDictionary:blogAricleDictionary];
//            }];
//        }
//        if (completion) {
//            completion(error,blogAricles);
//        }
        
    }];

}

+(void)fetchCityByCode:(NSInteger)cityCode  completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSAddressAPI requestCityWithCode:cityCode];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
         NSLog(@"city:%@",response.body);
//        NSArray * blogAricles = nil;
//        if (error == nil) {
//            blogAricles = [response.body[@"Variables"][@"postlist"] hb_mappedArrayWithBlock:^id(NSDictionary *blogAricleDictionary) {
//                return [[self alloc] initWithDictionary:blogAricleDictionary];
//            }];
//        }
//        if (completion) {
//            completion(error,blogAricles);
//        }
        
    }];

}

+(void)fetchAreaByCode:(NSInteger)areaCode completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSAddressAPI requestAreaWithCode:areaCode];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
         NSLog(@"area:%@",response.body);
//        NSArray * blogAricles = nil;
//        if (error == nil) {
//            blogAricles = [response.body[@"Variables"][@"postlist"] hb_mappedArrayWithBlock:^id(NSDictionary *blogAricleDictionary) {
//                return [[self alloc] initWithDictionary:blogAricleDictionary];
//            }];
//        }
//        if (completion) {
//            completion(error,blogAricles);
//        }
        
    }];

}

+(void)fetchAddressListByUserId:(NSInteger )userId Completion:(void (^)(NSError *error, NSArray *list))completion {
    HBRequest * request = [ASUSReceiveAddressAPI requestForGetReceiveAddressByUserID:userId];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"area:%@",response.body);
        //        NSArray * blogAricles = nil;
        //        if (error == nil) {
        //            blogAricles = [response.body[@"Variables"][@"postlist"] hb_mappedArrayWithBlock:^id(NSDictionary *blogAricleDictionary) {
        //                return [[self alloc] initWithDictionary:blogAricleDictionary];
        //            }];
        //        }
        //        if (completion) {
        //            completion(error,blogAricles);
        //        }
        
    }];
}
@end

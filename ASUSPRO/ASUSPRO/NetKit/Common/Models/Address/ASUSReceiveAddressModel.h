//
//  ASUSReceiveAddressModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSReceiveAddressModel : HBObject

@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, strong) NSString *is_default;


+(void)addReceiveAddressByUserID:(NSInteger)userID userName:(NSString *)username provinceCode:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default completion:(void (^)(NSError *error, NSString *message))completion;

+(void)editReceiveAddressByAddressID:(NSInteger)addressID userID:(NSInteger)userID userName:(NSString *)username provinceCode:(NSString *)province city:(NSString *)city area:(NSString *)area address:(NSString *)address zip:(NSString *)zip mobile:(NSString *)mobile isDefault:(NSInteger)is_default completion:(void (^)(NSError *error, NSString *message))completion;

+(void)fetchReceiveAddressByUserID:(NSInteger)userID completion:(void (^)(NSError *error, NSArray *list))completion;


+(void)deleteReceiveAddressByUserID:(NSInteger)userID addressID:(NSInteger)addressId completion:(void (^)(NSError *error, NSArray *list))completion;

@end

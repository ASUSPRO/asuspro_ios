//
//  ASUSExchangeModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSExchangeModel : HBObject

@property (nonatomic, assign) long long createTime;
@property (nonatomic, strong) NSString *express;//快递公司名
@property (nonatomic, strong) NSString *expressNo;//快递号
@property (nonatomic, assign) NSInteger giftId;
@property (nonatomic, strong) NSString *giftName;
@property (nonatomic, strong) NSString *giftPic;
@property (nonatomic, assign) NSInteger exchangeId;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, assign) NSInteger userScore;

+(void)fetchExchangeListByUserID:(NSInteger)userID page:(NSInteger)page completion:(void (^)(NSError *error, NSArray *list))completion;

@end

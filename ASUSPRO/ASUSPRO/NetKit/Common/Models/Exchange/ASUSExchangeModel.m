//
//  ASUSExchangeModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeModel.h"
#import "ASUSExchangeAPI.h"
#import "NSValueTransformer+HBTransformers.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSExchangeModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames {
    return @{
             @"createTime" : @"create_time",
             @"express" : @"express",
             @"expressNo" : @"express_no",
             @"giftId" : @"gift_id",
             @"giftName" : @"gift_name",
             @"giftPic" : @"gift_pic",
             @"exchangeId" : @"id",
             @"num" : @"num",
             @"orderId" : @"order_id",
             @"userScore" : @"score"
             };
}

+(void)fetchExchangeListByUserID:(NSInteger)userID page:(NSInteger)page completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSExchangeAPI requestForGetExchangeListByUserID:userID page:page];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"exchange:%@",response.body);
       NSArray * exchangeList = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            if ([response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
                exchangeList = [ response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    ASUSExchangeModel *exchangeModel = [[ASUSExchangeModel alloc] initWithDictionary:orgDict];
                    return exchangeModel;
                }];
            }
        }
        
        if (completion) {
            completion(error,exchangeList);
        }
        
    }];
}
@end

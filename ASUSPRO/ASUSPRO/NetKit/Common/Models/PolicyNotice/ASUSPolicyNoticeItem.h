//
//  ASUSPolicyNoticeItem.h
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASUSPolicyNoticeItem : NSObject
@property (nonatomic, strong) NSString *menuTitle;
@property (nonatomic, assign) NSInteger *menuId;
@end

//
//  ASUSExpressModel.h
//  ASUSPRO
//
//  Created by May on 15-6-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASUSExpressModel : NSObject
@property (nonatomic ,strong) NSString *expressTime;
@property (nonatomic ,strong) NSString *expressCurrentAddress;
@property (nonatomic ,strong) NSString *expressRemark;
@end

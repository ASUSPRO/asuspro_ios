//
//  ASUSProductModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSProductModel : HBObject

@property (nonatomic, strong) NSString *categoryId;

@property (nonatomic, strong) NSString *categoryName;

+(void)fetchProductCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchArticleCategoryByCategoryID:(NSInteger)categoryid  completion:(void (^)(NSError *error, NSArray *list))completion;
@end

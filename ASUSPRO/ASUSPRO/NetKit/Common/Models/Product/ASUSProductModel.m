//
//  ASUSProductModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductModel.h"
#import "ASUSProductAPI.h"
#import "ASUSArticleAPI.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSProductModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames
{
    return @{@"categoryId":@"id",
             @"categoryName":@"category_name"
             };
}

+(void)fetchProductCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSProductAPI requestForProductCategory];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"productCategory:%@",response.body);
        NSMutableArray * categoryArr = [NSMutableArray arrayWithArray:0];
        if (error == nil && [response.body[@"ResultCode"] integerValue]==0) {
            if ([response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
                ASUSProductModel *model = [[ASUSProductModel alloc] init];
                model.categoryId = [NSString stringWithFormat:@"%ld",(long)0];
                model.categoryName = @"全部分类";
                [categoryArr addObject:model];
                NSArray *arr = [NSArray arrayWithArray:response.body[@"ResultContent"]];
                for (int i = 0; i < arr.count; i++) {
                    ASUSProductModel *model = [[ASUSProductModel alloc] init];
                    model.categoryId = arr[i][@"id"];
                    model.categoryName = arr[i][@"category_name"];
                    [categoryArr addObject:model];
                }
            }

        }
        if (completion) {
            completion(error,categoryArr);
        }
        
    }];
}



+(void)fetchArticleCategoryByCategoryID:(NSInteger)categoryid  completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSArticleAPI requestForGetArticleCategoryByCategoryID:categoryid];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"articleCategory:%@",response.body);
        NSMutableArray * categoryArr = [NSMutableArray arrayWithArray:0];
        if (error == nil && [response.body[@"ResultCode"] integerValue]==0) {
            if ([response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
                ASUSProductModel *model = [[ASUSProductModel alloc] init];
                model.categoryId = [NSString stringWithFormat:@"%ld",(long)categoryid];
                model.categoryName = @"全部分类";
                [categoryArr addObject:model];
                NSArray *arr = [NSArray arrayWithArray:response.body[@"ResultContent"]];
                for (int i = 0; i < arr.count; i++) {
                    ASUSProductModel *model = [[ASUSProductModel alloc] init];
                    model.categoryId = arr[i][@"id"];
                    model.categoryName = arr[i][@"name"];
                    [categoryArr addObject:model];
                }
            }
        }
        if (completion) {
            
            completion(error,categoryArr);
        }
        
    }];
    
}

@end

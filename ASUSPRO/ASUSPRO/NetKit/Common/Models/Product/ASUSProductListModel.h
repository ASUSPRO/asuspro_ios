//
//  ASUSProductListModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSProductListModel : HBObject

@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *partNumber;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *stockPosition;
@property (nonatomic, strong) NSString *type;


+(void)fetchProductListByCategoryID:(NSInteger)categoryID page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion;
@end

//
//  ASUSProductListModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductListModel.h"
#import "ASUSProductAPI.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSProductListModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames
{
    return @{@"categoryId":@"cat_id",
             @"productId":@"id",
             @"num":@"num",
             @"partNumber":@"part_number",
             @"price":@"price",
             @"stockPosition":@"stock_position",
             @"type":@"type"
             };
}


+(void)fetchProductListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSProductAPI requestForGetProductListByCategoryID:categoryID page:page];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"productList:%@",response.body);
        NSArray * productList = nil;
        if (error == nil&& [response.body[@"ResultCode"] integerValue]==0&&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            productList = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *productDictionary) {
                return [[self alloc] initWithDictionary:productDictionary];
            }];
        }
        if (completion) {
            completion(error,productList);
        }
        
    }];
}
@end

//
//  ASUSShopModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSShopModel.h"
#import "ASUSShopAPI.h"
#import "NSValueTransformer+HBTransformers.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSShopModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames {
    return @{
             @"shopId" : @"id",
             @"catId" : @"cat_id",
             @"recommendPicUrl" : @"recommend_pic"
             };
}

+(void)fetchShopIndexListWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSShopAPI requestForGetShopIndexList];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"shopIndexList:%@",response.body);
        NSArray *shopIndexList = nil;
        
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                shopIndexList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    ASUSShopModel *shopModel = [[ASUSShopModel alloc] initWithDictionary:orgDict];
                    return shopModel;
                }];
            }
        }
        
        if (completion) {
            completion(error,shopIndexList);
        }
    }];
}
@end

//
//  ASUSShopModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSShopModel : HBObject
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *catId;
@property (nonatomic, strong) NSString *recommendPicUrl;

+(void)fetchShopIndexListWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

@end

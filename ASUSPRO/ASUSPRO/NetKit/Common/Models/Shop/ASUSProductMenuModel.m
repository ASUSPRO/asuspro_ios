//
//  ASUSProdutMenuModel.m
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductMenuModel.h"
#import "NSValueTransformer+HBTransformers.h"
#import "NSArray+HBAdditions.h"
#import "ASUSGiftAPI.h"

@implementation ASUSProductMenuModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames {
    return @{
             @"categoryId" : @"id",
             @"categoryName" : @"category_name"
             };
}

- (void) setNodeLevel:(NSInteger)nodeLevel {
    _nodeLevel = nodeLevel;
}

+(void)fetchGiftCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSGiftAPI requestForGiftCategory];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"giftCategory:%@",response.body);
        NSArray *categoryList = nil;
        
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                categoryList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    ASUSProductMenuModel *productMenuModel = [[ASUSProductMenuModel alloc] initWithDictionary:orgDict];
                    return productMenuModel;
                }];
            }
        }
        
        if (completion) {
            completion(error,categoryList);
        }
        
    }];
}

@end

//
//  ASUSProdutMenuModel.h
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBObject.h"

@interface ASUSProductMenuModel : HBObject
@property (nonatomic, assign) BOOL isFirstCategory;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, assign) NSInteger nodeLevel;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) NSMutableArray *sonNodes;
@property (nonatomic, assign) BOOL isExpanded;

+(void)fetchGiftCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion;
@end

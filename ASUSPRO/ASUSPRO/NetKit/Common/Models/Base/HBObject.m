//
//  HBObject.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 31/03/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBObject.h"

static BAClient *sClient = nil;

@implementation HBObject

+ (BAClient *)client {
  @synchronized(self) {
    BAClient *client = nil;
    
    if (sClient) {
      client = sClient;
    } else {
      client = [BAClient sharedClient];
    }
    
    return client;
  }
}

+ (void)setClient:(BAClient *)client {
  @synchronized(self) {
    sClient = client;
  }
}

@end

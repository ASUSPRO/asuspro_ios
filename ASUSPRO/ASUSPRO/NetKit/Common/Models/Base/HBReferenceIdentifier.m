//
//  HBReferenceIdentifier.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 27/06/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBReferenceIdentifier.h"
#import "NSValueTransformer+HBConstants.h"

@implementation HBReferenceIdentifier

- (instancetype)init {
  return [self initWithReferenceID:0 type:HBReferenceTypeNone];
}

- (instancetype)initWithReferenceID:(NSUInteger)referenceID type:(HBReferenceType)referenceType {
  self = [super init];
  if (!self) return nil;
  
  _referenceID = referenceID;
  _referenceType = referenceType;
  
  return self;
}

+ (instancetype)identifierWithReferenceID:(NSUInteger)referenceID type:(HBReferenceType)referenceType {
  return [[self alloc] initWithReferenceID:referenceID type:referenceType];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone {
  return [[[self class] alloc] initWithReferenceID:self.referenceID type:self.referenceType];
}

#pragma mark - Properties

- (NSString *)referenceTypeString {
  return [NSValueTransformer hb_stringFromReferenceType:self.referenceType];
}

@end

//
//  HBObject.h
//  PodioKit
//
//  Created by Sebastian Rehnby on 31/03/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBModel.h"
#import "BAClient.h"

@interface HBObject : HBModel

+ (BAClient *)client;
+ (void)setClient:(BAClient *)client;

@end

//
//  HBDateRange.m
//  PodioKit
//
//  Created by Sebastian Rehnby on 14/05/14.
//  Copyright (c) 2014 Citrix Systems, Inc. All rights reserved.
//

#import "HBDateRange.h"
//#import "NSValueTransformer+HBTransformers.h"

@implementation HBDateRange

- (instancetype)initWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
  self = [super init];
  if (!self) return nil;
  
  _startDate = [startDate copy];
  _endDate = [endDate copy];
  
  return self;
}

+ (instancetype)rangeWithStartDate:(NSDate *)startDate {
  return [[self alloc] initWithStartDate:startDate endDate:nil];
}

+ (instancetype)rangeWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
  return [[self alloc] initWithStartDate:startDate endDate:endDate];
}

#pragma mark - HBModel

+ (NSDictionary *)dictionaryKeyPathsForPropertyNames {
  return @{
           @"startDate" : @"start_utc",
           @"endDate" : @"end_utc",
           };
}

//+ (NSValueTransformer *)startDateValueTransformer {
//  return [NSValueTransformer hb_dateValueTransformer];
//}
//
//+ (NSValueTransformer *)endDateValueTransformer {
//  return [NSValueTransformer hb_dateValueTransformer];
//}

@end

//
//  ASUSColumnPicModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/20.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSColumnPicModel : HBObject

@property (nonatomic, strong) NSString *picUrl;
@property (nonatomic, strong) NSString *linkUrl;

+(void)fetchColumnPicWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

@end

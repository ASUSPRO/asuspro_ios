//
//  ASUSSearchModel.h
//  ASUSPRO
//
//  Created by May on 15-6-24.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSSearchModel : HBObject
@property (nonatomic, strong) NSString *searchText;
@end

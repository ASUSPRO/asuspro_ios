//
//  ASUSColumnPicModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/20.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSColumnPicModel.h"
#import "ASUSColumnPicAPI.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSColumnPicModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames
{
    return @{@"linkUrl":@"link_url",
             @"picUrl":@"pic_url"
             };
}

+(void)fetchColumnPicWithCompletion:(void (^)(NSError *error,  NSArray *list))completion
{
    HBRequest * request = [ASUSColumnPicAPI requestForColumnPic];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"columnpic:%@",response.body);
        if ([response.body isKindOfClass:[NSData class]]) {
            return ;
        }
        NSArray *columArr = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0 &&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            columArr = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *aricleDictionary) {
                return [[self alloc] initWithDictionary:aricleDictionary];
            }];
        }
        if (completion) {
            completion(error,columArr);
        }
      
    }];
    
}
@end

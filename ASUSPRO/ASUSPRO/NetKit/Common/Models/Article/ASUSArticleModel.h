//
//  ASUSArticleModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/21.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"


@interface ASUSArticleModel : HBObject

@property (nonatomic ,strong) NSString *createTime;
@property (nonatomic ,strong) NSString *articleId;
@property (nonatomic ,strong) NSString *mainTitle;
@property (nonatomic ,strong) NSString *source;
@property (nonatomic ,strong) NSString *columnID;


+(void)fetchArticleListByArticleID:(NSInteger)articleID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchSearchByKeyword:(NSString *)keyword completion:(void (^)(NSError *error, NSArray *list))completion;

@end

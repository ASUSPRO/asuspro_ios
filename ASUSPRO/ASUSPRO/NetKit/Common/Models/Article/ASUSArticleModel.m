//
//  ASUSArticleModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/21.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSArticleModel.h"
#import "ASUSArticleAPI.h"
#import "NSArray+HBAdditions.h"
#import "ASUSSearchAPI.h"

@implementation ASUSArticleModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames
{
    return @{
             @"createTime":@"createtime",
             @"articleId":@"id",
             @"mainTitle":@"maintitle",
             @"source":@"source",
             @"columnID":@"column_id"
             };
}
+(void)fetchArticleListByArticleID:(NSInteger)articleID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSArticleAPI requestForGetArticleListByArticleID:articleID page:page];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"articleList:%@",response.body);
        NSArray * aricles = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0 &&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            aricles = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *aricleDictionary) {
                return [[self alloc] initWithDictionary:aricleDictionary];
            }];
        }
        if (completion) {
            completion(error,aricles);
        }
        
    }];
    
}

+(void)fetchSearchByKeyword:(NSString *)keyword completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSSearchAPI requestForHomeSearchByKeyword:keyword];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"searchList:%@",response.body);
        NSArray * aricles = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0 &&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            aricles = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *aricleDictionary) {
                return [[self alloc] initWithDictionary:aricleDictionary];
            }];
        }
        if (completion) {
            completion(error,aricles);
        }
        
    }];

}

@end

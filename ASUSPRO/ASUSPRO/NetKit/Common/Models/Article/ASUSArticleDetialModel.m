//
//  ASUSArticleDetialModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/3.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSArticleDetialModel.h"
#import "ASUSArticleAPI.h"

@implementation ASUSArticleDetialModel


+(void)fetchArticleContentByArticleID:(NSInteger)articleID  contentID:(NSInteger)contentid completion:(void (^)(NSError *error, ASUSArticleDetialModel *model))completion
{
    HBRequest * request = [ASUSArticleAPI requestForGetArticleContentByArticleID:articleID contentId:contentid];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"articleContent:%@",response.body[@"ResultContent"]);
       if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
           ASUSArticleDetialModel *detailModel = [[ASUSArticleDetialModel alloc] init];
           detailModel.articleDetailId = response.body[@"ResultContent"][@"id"];
           detailModel.createTime = response.body[@"ResultContent"][@"createtime"];
           detailModel.mainTitle = response.body[@"ResultContent"][@"maintitle"];
           detailModel.source = response.body[@"ResultContent"][@"source"];
           detailModel.content = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:response.body[@"ResultContent"][@"content"] options:0] encoding:NSUTF8StringEncoding];
           if (completion) {
               completion(error,detailModel);
           }
       }
    }];
  
}
@end

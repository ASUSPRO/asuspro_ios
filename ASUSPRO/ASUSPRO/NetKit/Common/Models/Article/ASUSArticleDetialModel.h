//
//  ASUSArticleDetialModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/3.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSArticleDetialModel : HBObject

@property (nonatomic ,strong) NSString *articleDetailId;
@property (nonatomic ,strong) NSString *content;
@property (nonatomic ,strong) NSString *createTime;
@property (nonatomic ,strong) NSString *mainTitle;
@property (nonatomic ,strong) NSString *source;

+(void)fetchArticleContentByArticleID:(NSInteger)articleID  contentID:(NSInteger)contentid completion:(void (^)(NSError *error, ASUSArticleDetialModel *model))completion;

@end

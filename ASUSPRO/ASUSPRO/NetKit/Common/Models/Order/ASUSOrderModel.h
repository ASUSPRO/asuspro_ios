//
//  ASUSOrderModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSOrderModel : HBObject
@property (nonatomic, assign) double deliveryFee;
@property (nonatomic, assign) long long createTime;
@property (nonatomic, strong) NSString *express;//快递公司名
@property (nonatomic, strong) NSString *expressNo;//快递号
@property (nonatomic, assign) NSInteger serverId;
@property (nonatomic, assign) NSInteger orderId;
@property (nonatomic, assign) NSInteger receiveId;//收获地址id
@property (nonatomic, assign) long long sendTime;//发货时间
@property (nonatomic, assign) NSInteger status;//status
@property (nonatomic, strong) NSString *statusName;
@property (nonatomic, assign) NSInteger useScore;
@property (nonatomic, assign) NSInteger userId;


+(void)UploadOrderByData:(NSString *)data completion:(void (^)(NSError *error, NSString *success))completion;

+(void)fetchOrderListByUserID:(NSInteger)userID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion;


@end

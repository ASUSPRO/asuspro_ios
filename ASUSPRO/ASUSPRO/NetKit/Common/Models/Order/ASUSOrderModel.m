//
//  ASUSOrderModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSOrderModel.h"
#import "ASUSOrderAPI.h"
#import "NSValueTransformer+HBTransformers.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSOrderModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames {
    return @{
             @"deliveryFee" : @"delivery_fee",
             @"createTime" : @"create_time",
             @"express" : @"express",
             @"expressNo" : @"express_no",
             @"serverId" : @"id",
             @"orderId" : @"order_id",
             @"receiveId" : @"receive_id",
             @"sendTime" : @"send_time",
             @"status" : @"status",
             @"statusName" : @"status_name",
             @"useScore" : @"use_score",
             @"userId" : @"user_id"
             };
}

+(void)UploadOrderByData:(NSString *)data completion:(void (^)(NSError *error, NSString *success))completion
{
    HBRequest * request = [ASUSOrderAPI  requestForUploadOrderByData:data];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"uploadOrder:%@",response.body);
         NSLog(@"uploadOrder:%@",response.body[@"ResultMsg"]);
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            if (completion) {
                completion(error,response.body[@"ResultMsg"]);
            }
        } else {
            if (completion) {
                completion(error,response.body[@"ResultMsg"]);
            }
        }
        
        
    }];
}

+(void)fetchOrderListByUserID:(NSInteger)userID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSOrderAPI requestForGetOrderListByUserID:userID page:page];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"orderList:%@",response.body);
      NSArray * orderList = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                orderList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    ASUSOrderModel *orderModel = [[ASUSOrderModel alloc] initWithDictionary:orgDict];
                    return orderModel;
                }];
            }
        }
        
        if (completion) {
            completion(error,orderList);
        }
        
    }];
}
@end

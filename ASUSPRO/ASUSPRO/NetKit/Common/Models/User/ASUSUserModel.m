//
//  ASUSUserModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSUserModel.h"
#import "ASUSUserAPI.h"

@implementation ASUSUserModel

+(void)updateAvatarByUserID:(NSInteger)userID avatarData:(NSData *)imageData completion:(void (^)(NSError *error, NSString *avatarUrl))completion
{
    HBRequest * request = [ASUSUserAPI requestForUpdateAvatarByUserID:userID];
    request.contentType =  HBRequestContentTypeMultipart;
    request.fileData = [HBRequestFileData fileDataWithData:imageData name:@"imageData"];
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"responseBody:%@",response.body);
        NSString *result = [[NSString alloc] initWithData:response.body  encoding:NSUTF8StringEncoding];
        NSLog(@"avatarUrl:%@",result);
        if (error == nil &&completion) {
            completion(error,result);
        }
        
    }];
    
}

+(void)updateUserInfoByUserID:(NSInteger)userID name:(NSString *)name pay_password:(NSString *)payPassword birthdday:(NSString *)birthday mobile:(NSString *)mobile phone:(NSString *)phone completion:(void (^)(NSError *error, NSString *message))completion
{
    HBRequest * request = [ASUSUserAPI requestForUpdateUserInfoByUserID:userID name:name pay_password:payPassword birthdday:birthday mobile:mobile phone:phone];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"updateInfo:%@",response.body);
        NSLog(@"message:%@",response.body[@"ResultMsg"]);
        if (error == nil &&completion) {
            completion(error,response.body[@"ResultMsg"]);
        }
    }];
}

+(void)checkPayPassByUserID:(NSInteger)userID pay_password:(NSString *)pay_password completion:(void (^)(NSError *error, NSString *message))completion
{
    HBRequest * request = [ASUSUserAPI requestForCheckPayPassByUserID:userID pay_password:pay_password];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"checkPaypassword:%@",response.body);
         NSLog(@"message:%@",response.body[@"ResultMsg"]);
        if (error == nil &&completion) {
            completion(error,response.body[@"ResultMsg"]);
        }
    }];

}

+(void)uploadForUserFeedBackByUserID:(NSInteger)userID userName:(NSString *)username feedBack:(NSString *)feedBack
 completion:(void (^)(NSError *error, NSString *message))completion
{
    HBRequest * request = [ASUSUserAPI requestForUserFeedBackByUserID:userID userName:username feedBack:feedBack];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"feedBack:%@",response.body);
        NSLog(@"suggest:%@",response.body[@"ResultMsg"]);
        if (error == nil &&completion) {
            completion(error,response.body[@"ResultMsg"]);
        }
    }];

}

+(void)sendEmialByEmial:(NSString *)email completion:(void (^)(NSError *error, NSString *message))completion {
    HBRequest * request = [ASUSUserAPI requestForUserSendEmialByEmial:email];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"reBackPass:%@",response.body);
//        NSLog(@"reBackPass:%@",response.body[@"ResultMsg"]);
        if (error == nil &&completion) {
            completion(error,@"发送成功");
        }
    }];
}

@end

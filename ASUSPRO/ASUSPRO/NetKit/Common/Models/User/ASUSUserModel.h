//
//  ASUSUserModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSUserModel : HBObject

+(void)updateAvatarByUserID:(NSInteger)userID avatarData:(NSData *)imageData completion:(void (^)(NSError *error, NSString *avatarUrl))completion;

+(void)updateUserInfoByUserID:(NSInteger)userID name:(NSString *)name pay_password:(NSString *)payPassword birthdday:(NSString *)birthday mobile:(NSString *)mobile phone:(NSString *)phone completion:(void (^)(NSError *error, NSString *message))completion;

+(void)checkPayPassByUserID:(NSInteger)userID pay_password:(NSString *)pay_password completion:(void (^)(NSError *error, NSString *message))completion;

+(void)uploadForUserFeedBackByUserID:(NSInteger)userID userName:(NSString *)username feedBack:(NSString *)feedBack
                          completion:(void (^)(NSError *error, NSString *message))completion;

+(void)sendEmialByEmial:(NSString *)email completion:(void (^)(NSError *error, NSString *message))completion;
@end

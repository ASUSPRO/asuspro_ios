//
//  ASUSGiftModel.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HBObject.h"

@interface ASUSGiftModel : HBObject
@property (nonatomic, assign) NSInteger catId;
@property (nonatomic, assign) long long createTime;
@property (nonatomic, strong) NSString *giftDesc;
@property (nonatomic, strong) NSString *giftName;
@property (nonatomic, strong) NSString *giftNote;
@property (nonatomic, strong) NSString *giftPicUrl;
@property (nonatomic, assign) NSInteger gifId;
@property (nonatomic, assign) NSInteger innerScore;
@property (nonatomic, assign) NSInteger isAsus;
@property (nonatomic, assign) NSInteger gifNum;// 当前库存数
@property (nonatomic, assign) NSInteger outerScore;
@property (nonatomic, assign) NSInteger recommend;
@property (nonatomic, strong) NSString *recommendPicUrl;
@property (nonatomic, assign) NSInteger showFlag;
@property (nonatomic, assign) NSInteger exchangeNum;//兑换数
@property (nonatomic, assign) BOOL isSelected;
+(void)fetchGiftCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchNewGiftListPerMonthWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchAsusGiftListWithCompletion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchGiftListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion;

+(void)fetchGiftDetailByGiftID:(NSInteger)giftID completion:(void (^)(NSError *error, ASUSGiftModel *giftModle))completion;

+(void)fetchSearchGiftByKeyword:(NSString *)keyword completion:(void (^)(NSError *error, NSArray *list))completion;

@end

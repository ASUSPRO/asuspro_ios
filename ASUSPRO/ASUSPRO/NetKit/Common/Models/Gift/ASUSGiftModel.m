//
//  ASUSGiftModel.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/14.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSGiftModel.h"
#import "ASUSGiftAPI.h"
#import "ASUSSearchAPI.h"
#import "NSValueTransformer+HBTransformers.h"
#import "NSArray+HBAdditions.h"

@implementation ASUSGiftModel

+(NSDictionary *)dictionaryKeyPathsForPropertyNames {
    return @{
             @"catId" : @"cat_id",
             @"createTime" : @"create_time",
//             @"giftDesc" : @"gift_desc",
             @"giftName" : @"gift_name",
             @"giftNote" : @"gift_note",
             @"giftPicUrl" : @"gift_pic",
             @"gifId" : @"id",
             @"innerScore" : @"inner_score",
             @"isAsus" : @"is_asus",
             @"gifNum" : @"num",
             @"giftDesc" : @"cat_id",
             @"outerScore" : @"outer_score",
             @"recommend" : @"recommend",
             @"recommendPicUrl" : @"recommend_pic",
             @"showFlag" : @"show_flag"
             };
}

+ (NSString *)getPrimaryKey{
    return @"gifId";
}

+(void)fetchGiftCategoryWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSGiftAPI requestForGiftCategory];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"giftCategory:%@",response.body);
        //        NSArray * blogAricles = nil;
        //        if (error == nil) {
        //            blogAricles = [response.body[@"Variables"][@"postlist"] hb_mappedArrayWithBlock:^id(NSDictionary *blogAricleDictionary) {
        //                return [[self alloc] initWithDictionary:blogAricleDictionary];
        //            }];
        //        }
        //        if (completion) {
        //            completion(error,blogAricles);
        //        }
        
    }];
}


+(void)fetchNewGiftListPerMonthWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSGiftAPI requestForNewGiftListPerMonth];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSArray *newGiftList = nil;
        NSLog(@"newgiftlist:%@",response.body);
//        NSString *ss = [[NSString alloc] initWithData:response.body encoding:NSUTF8StringEncoding];
//        NSLog(@"asusGiftList=====:%@",ss);
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                newGiftList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    ASUSGiftModel *giftModel = [[ASUSGiftModel alloc] initWithDictionary:orgDict];
                    return giftModel;
                }];
            }
        }
        
        if (completion) {
            completion(error,newGiftList);
        }
    }];
}

+(void)fetchAsusGiftListWithCompletion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSGiftAPI requestForAsusGiftList];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"asusGiftList:%@", response.body);
//        NSString *ss = [[NSString alloc] initWithData:response.body encoding:NSUTF8StringEncoding];
//        NSLog(@"asusGiftList=====:%@",ss);
        NSMutableArray *giftList = [[NSMutableArray alloc] initWithCapacity:0];
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                for (NSDictionary *gift in resultArray) {
                    //                giftList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
                    //                    ASUSGiftModel *giftModel = [[ASUSGiftModel alloc] initWithDictionary:orgDict];
                    ASUSGiftModel *giftModel = [[ASUSGiftModel alloc] init];
                    
                    giftModel.catId = [gift[@"cat_id"] integerValue];
                    giftModel.createTime = [gift[@"create_time"] doubleValue];
                    giftModel.giftName = gift[@"gift_name"];
                    giftModel.giftPicUrl = gift[@"gift_pic"];
                    giftModel.gifId = [gift[@"id"] integerValue];
                    giftModel.innerScore = [gift[@"inner_score"] integerValue];
                    giftModel.isAsus = [gift[@"is_asus"] integerValue];
                    giftModel.gifNum = [gift[@"num"] integerValue];
                    giftModel.outerScore = [gift[@"outer_score"] integerValue];
                    giftModel.recommend = [gift[@"recommend"] integerValue];
                    giftModel.recommendPicUrl = gift[@"recommend_pic"];
                    giftModel.showFlag = [gift[@"show_flag"] integerValue];
                    giftModel.giftNote = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:gift[@"gift_note"] options:0] encoding:NSUTF8StringEncoding];
                    giftModel.giftDesc =[[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:gift[@"gift_desc"] options:0] encoding:NSUTF8StringEncoding];
                    [giftList addObject:giftModel];
                    //                }];
                }
            }
        }
        
        if (completion) {
            completion(error,giftList);
        }
    }];
}
+(void)fetchGiftListByCategoryID:(NSInteger)categoryID  page:(NSInteger )page completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSGiftAPI requestForGetGiftListByCategoryID:categoryID page:page];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"giftList:%@",response.body);
//        NSString *ss = [[NSString alloc] initWithData:response.body encoding:NSUTF8StringEncoding];
//         NSLog(@"giftList=====:%@",ss);
//        NSArray *giftList = nil;
        NSMutableArray *giftList = [[NSMutableArray alloc] initWithCapacity:0];
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
             NSArray *resultArray =  response.body[@"ResultContent"];
            if (resultArray && [resultArray isKindOfClass:[NSArray class]] && resultArray.count > 0) {
                for (NSDictionary *gift in resultArray) {
//                giftList = [resultArray hb_mappedArrayWithBlock:^id(NSDictionary *orgDict){
//                    ASUSGiftModel *giftModel = [[ASUSGiftModel alloc] initWithDictionary:orgDict];
                    ASUSGiftModel *giftModel = [[ASUSGiftModel alloc] init];
                    
                    giftModel.catId = [gift[@"cat_id"] integerValue];
                    giftModel.createTime = [gift[@"create_time"] doubleValue];
                    giftModel.giftName = gift[@"gift_name"];
                    giftModel.giftPicUrl = gift[@"gift_pic"];
                    giftModel.gifId = [gift[@"id"] integerValue];
                    giftModel.innerScore = [gift[@"inner_score"] integerValue];
                    giftModel.isAsus = [gift[@"is_asus"] integerValue];
                    giftModel.gifNum = [gift[@"num"] integerValue];
                    giftModel.outerScore = [gift[@"outer_score"] integerValue];
                    giftModel.recommend = [gift[@"recommend"] integerValue];
                    giftModel.recommendPicUrl = gift[@"recommend_pic"];
                    giftModel.showFlag = [gift[@"show_flag"] integerValue];
                    giftModel.giftNote = gift[@"gift_note"];
                    giftModel.giftDesc =gift[@"gift_desc"];
//                    giftModel.giftNote = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:gift[@"gift_note"] options:0] encoding:NSUTF8StringEncoding];
//                    giftModel.giftDesc =[[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:gift[@"gift_desc"] options:0] encoding:NSUTF8StringEncoding];
                    [giftList addObject:giftModel];
//                }];
            }
        }
        }
        
        if (completion) {
            completion(error,giftList);
        }
    }];
    
}

+(void)fetchSearchGiftByKeyword:(NSString *)keyword completion:(void (^)(NSError *error, NSArray *list))completion
{
    HBRequest * request = [ASUSSearchAPI requestForGiftSearchByKeyword:keyword];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"searchGiftList:%@",response.body);
        NSArray * gifts = nil;
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0 &&[response.body[@"ResultContent"] isKindOfClass:[NSArray class]]) {
            gifts = [response.body[@"ResultContent"] hb_mappedArrayWithBlock:^id(NSDictionary *giftDictionary) {
                return [[self alloc] initWithDictionary:giftDictionary];
            }];
        }
        if (completion) {
            completion(error,gifts);
        }
        
    }];

}

+(void)fetchGiftDetailByGiftID:(NSInteger)giftID completion:(void (^)(NSError *error, ASUSGiftModel *giftModle))completion
{
    HBRequest * request = [ASUSGiftAPI requestForGetGiftDetailByGiftID:giftID];
    request.contentType =  HBRequestContentTypeJSON;
    [[self client] performRequest:request completion:^(HBResponse *response, NSError *error) {
        NSLog(@"giftDetail:%@",response.body);
        if (error == nil && [response.body[@"ResultCode"] integerValue] ==0) {
            ASUSGiftModel *giftModle = [[ASUSGiftModel alloc] init];
            giftModle.catId = [response.body[@"ResultContent"][@"cat_id"] integerValue];
            giftModle.createTime = [response.body[@"ResultContent"][@"create_time"] doubleValue];
            giftModle.giftName = response.body[@"ResultContent"][@"gift_name"];
            giftModle.giftPicUrl = response.body[@"ResultContent"][@"gift_pic"];
            giftModle.gifId = [response.body[@"ResultContent"][@"id"] integerValue];
            giftModle.innerScore = [response.body[@"ResultContent"][@"inner_score"] integerValue];
            giftModle.isAsus = [response.body[@"ResultContent"][@"is_asus"] integerValue];
            giftModle.gifNum = [response.body[@"ResultContent"][@"num"] integerValue];
            giftModle.outerScore = [response.body[@"ResultContent"][@"outer_score"] integerValue];
            giftModle.recommend = [response.body[@"ResultContent"][@"recommend"] integerValue];
            giftModle.recommendPicUrl = response.body[@"ResultContent"][@"recommend_pic"];
            giftModle.showFlag = [response.body[@"ResultContent"][@"show_flag"] integerValue];
            giftModle.giftNote = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:response.body[@"ResultContent"][@"gift_note"] options:0] encoding:NSUTF8StringEncoding];
            giftModle.giftDesc =[[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:response.body[@"ResultContent"][@"gift_desc"] options:0] encoding:NSUTF8StringEncoding];
           
            if (completion) {
                completion(error,giftModle);
            }
        }
    }];
}
@end

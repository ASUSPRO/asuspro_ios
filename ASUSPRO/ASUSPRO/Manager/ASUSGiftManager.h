//
//  ASUSGiftManager.h
//  ASUSPRO
//
//  Created by May on 15-6-3.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUSGiftModel.h"
@interface ASUSGiftManager : NSObject
+(NSArray *)fetchShoppingCartGiftList;
+(BOOL)deleteShoppingCartGiftList:(NSArray *)giftModels;
+(BOOL )addGiftToShoppingCart:(ASUSGiftModel *) giftModel;
+(BOOL )modifyGiftToShoppingCart:(ASUSGiftModel *) giftModel;
+ (ASUSGiftModel *)fetchShoppingCartGiftListGiftId:(NSInteger )giftId;
+ (BOOL)isExistsGiftByGiftId:(NSInteger )giftId;
+ (NSInteger)numOfGiftModels;
+ (NSInteger)numOfGiftModelByGiftId:(NSInteger )giftId;
@end

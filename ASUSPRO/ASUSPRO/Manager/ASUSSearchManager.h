//
//  ASUSSearchManager.h
//  ASUSPRO
//
//  Created by May on 15-6-24.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUSSearchModel.h"

@interface ASUSSearchManager : NSObject
+ (NSArray *)fetchSearchTextList;
+ (BOOL)deleteSearchTextList:(NSArray *)searchModels;
+ (BOOL )addSearchText:(ASUSSearchModel *) searchModel;
+ (BOOL)clearSearchTexts;
@end

//
//  ASUSSearchManager.m
//  ASUSPRO
//
//  Created by May on 15-6-24.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSSearchManager.h"
#import "NSObject+LKDBHelper.h"

@implementation ASUSSearchManager
+ (NSArray *)fetchSearchTextList {
    NSArray *result = [ASUSSearchModel searchByPredicate:nil];
    return result;
}

+ (BOOL)deleteSearchTextList:(NSArray *)searchModels {
    BOOL result = NO;
    for (ASUSSearchModel *searchModel in searchModels) {
        result = [ASUSSearchModel deleteToDB:searchModel];
    }
    return result;
}

+ (BOOL )addSearchText:(ASUSSearchModel *) searchModel {
    if (![ASUSSearchManager fetchSearchText:searchModel.searchText]) {
       return [ASUSSearchModel insertToDB:searchModel];
    }
    return YES;
}

+ (BOOL)clearSearchTexts {
    BOOL result = [ASUSSearchModel deleteWithWhere:@""];
    return result;
}

+ (ASUSSearchModel *)fetchSearchText:(NSString *)text {
    ASUSSearchModel *searchModel = (ASUSSearchModel *)[ASUSSearchModel searchSingleWithWhere:[NSString stringWithFormat:@"searchText = \"%@\"", text] orderBy:@"searchText"];
    return searchModel;
}

@end

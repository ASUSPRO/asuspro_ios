//
//  ASUSGiftManager.m
//  ASUSPRO
//
//  Created by May on 15-6-3.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSGiftManager.h"
#import "NSObject+LKDBHelper.h"

@implementation ASUSGiftManager
+(NSArray *)fetchShoppingCartGiftList {
    NSArray *result = [ASUSGiftModel searchByPredicate:nil];
    return result;
} 

+(BOOL)deleteShoppingCartGiftList:(NSArray *)giftModels {
    BOOL result = NO;
    for (ASUSGiftModel *giftModel in giftModels) {
       result = [ASUSGiftModel deleteToDB:giftModel];
    }
    return result;
}

+(BOOL )addGiftToShoppingCart:(ASUSGiftModel *) giftModel {
    giftModel.isSelected = YES;
    return [ASUSGiftModel insertToDB:giftModel];
}

+(BOOL )modifyGiftToShoppingCart:(ASUSGiftModel *) giftModel {
    return [ASUSGiftModel updateToDB:giftModel where:[NSString stringWithFormat:@"gifId = %ld and catId = %ld",(long)giftModel.gifId,(long)giftModel.catId]];
}

+ (ASUSGiftModel *)fetchShoppingCartGiftListGiftId:(NSInteger )giftId {
    ASUSGiftModel *aSUSGiftModel = (ASUSGiftModel *)[ASUSGiftModel searchSingleWithWhere:[NSString stringWithFormat:@"gifId = %ld", (long)giftId] orderBy:@"createTime"];
    return aSUSGiftModel;
}

+ (BOOL)isExistsGiftByGiftId:(NSInteger )giftId {
    ASUSGiftModel *aSUSGiftModel = [ASUSGiftManager fetchShoppingCartGiftListGiftId:giftId];
    if (aSUSGiftModel) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSInteger)numOfGiftModelByGiftId:(NSInteger )giftId {
    NSInteger count = [ASUSGiftModel rowCountWithWhere:[NSString stringWithFormat:@"gifId = %ld", (long)giftId]];
    return count;
}

+ (NSInteger)numOfGiftModels {
    NSInteger count = [ASUSGiftModel rowCountWithWhere:@""];
    return count;
}

@end

//
//  HBPredicate.h
//  HuoBan
//
//  Created by abel on 14/11/5.
//  Copyright (c) 2014年 Huoban inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBPredicate : NSObject

@property (nonatomic, strong) NSString *predicate;
@property (nonatomic, assign) NSInteger offset;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, strong) NSArray * oderBy;

+(instancetype)predicateWithFormat:(NSString *)predicate;

@end

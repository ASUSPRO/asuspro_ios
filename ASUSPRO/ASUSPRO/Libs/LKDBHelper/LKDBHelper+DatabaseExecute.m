//
//  LKDBHelper+DatabaseExecute.m
//  LKDBHelper
//
//  Created by abel on 14-9-29.
//  Copyright (c) 2014年 ljh. All rights reserved.
//

#import "LKDBHelper+DatabaseExecute.h"
#import "LKDBHelper+DatabaseManager.h"

@implementation LKDBHelper(DatabaseExecute)

-(id)modelValueWithProperty:(LKDBProperty *)property model:(NSObject *)model {
    id value = nil;
    if(property.isUserCalculate) {
        value = [model userGetValueForModel:property];
    } else {
        value = [model modelGetValue:property];
    }
    
    if(value == nil) {
        value = @"";
    }
    return value;
}

-(void)asyncBlock:(void(^)(void))block {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),block);
}

#pragma mark - row count operation
-(NSInteger)rowCount:(Class)modelClass where:(id)where {
    return [self rowCountBase:modelClass where:where];
}

-(void)rowCount:(Class)modelClass where:(id)where callback:(void (^)(NSInteger))callback {
    [self asyncBlock:^{
        NSInteger result = [self rowCountBase:modelClass where:where];
        if(callback != nil) {
            callback(result);
        }
    }];
}

-(NSInteger)rowCountBase:(Class)modelClass where:(id)where {
    NSMutableString* rowCountSql = [NSMutableString stringWithFormat:@"select count(rowid) from %@",[modelClass getTableName]];
    
    NSMutableArray* valuesarray = [self extractQuery:rowCountSql where:where];
    NSInteger result = [[self executeScalarWithSQL:rowCountSql arguments:valuesarray] intValue];
    
    return result;
}

#pragma mark- search operation
-(NSMutableArray *)search:(Class)modelClass where:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count {
    return [self searchBase:modelClass columns:nil where:where orderBy:orderBy offset:offset count:count];
}

-(NSMutableArray *)search:(Class)modelClass column:(id)columns where:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count {
    return [self searchBase:modelClass columns:columns where:where orderBy:orderBy offset:offset count:count];
}

-(id)searchSingle:(Class)modelClass where:(id)where orderBy:(NSString *)orderBy {
    NSMutableArray* array = [self searchBase:modelClass columns:nil where:where orderBy:orderBy offset:0 count:1];
    
    if(array.count>0) {
        return [array objectAtIndex:0];
    }
    
    return nil;
}

-(void)search:(Class)modelClass where:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count callback:(void (^)(NSMutableArray *))block {
    [self asyncBlock:^{
        NSMutableArray* array = [self searchBase:modelClass columns:nil where:where orderBy:orderBy offset:offset count:count];
        
        if(block != nil)
            block(array);
    }];
}

-(NSMutableArray *)searchBase:(Class)modelClass columns:(id)columns where:(id)where orderBy:(NSString *)orderBy offset:(NSInteger)offset count:(NSInteger)count {
    NSString* columnsString = nil;
    NSUInteger columnCount = 0;
    if([columns isKindOfClass:[NSArray class]] && [columns count]>0) {
        columnCount = [columns count];
        columnsString = [columns componentsJoinedByString:@","];
    } else if([LKDBUtils checkStringIsEmpty:columns]==NO) {
        columnsString = columns;
        NSArray* array = [columns componentsSeparatedByString:@","];
        
        columnCount = array.count;
    }
    
    if(columnCount > 1) {
        columnsString = [NSString stringWithFormat:@"rowid,%@",columnsString];
    }
    
    if(columnCount==0) {
        columnsString = @"rowid,*";
    }
    
    NSMutableString* query = [NSMutableString stringWithFormat:@"select %@ from @t",columnsString];
    NSMutableArray * values = [self extractQuery:query where:where];
    
    [self sqlString:query AddOder:orderBy offset:offset count:count];
    
    NSString* db_tableName = [modelClass getTableName];
    //replace @t to model table name
    NSString* replaceTableName = [NSString stringWithFormat:@" %@ ",db_tableName];
    if([query hasSuffix:@" @t"]) {
        [query appendString:@" "];
    }
    [query replaceOccurrencesOfString:@" @t " withString:replaceTableName options:NSCaseInsensitiveSearch range:NSMakeRange(0, query.length)];
    
    __block NSMutableArray* results = nil;
    [self executeDB:^(FMDatabase *db) {
        FMResultSet* set = nil;
        if(values == nil) {
            set = [db executeQuery:query];
        } else {
            set = [db executeQuery:query withArgumentsInArray:values];
        }
        
        if(columnCount == 1) {
            results = [self executeOneColumnResult:set];
        } else {
            results = [self executeResult:set Class:modelClass tableName:db_tableName];
        }
        
        [set close];
    }];
    return results;
}

-(NSMutableArray *)searchWithSQL:(NSString *)sql toClass:(Class)modelClass {
    //replace @t to model table name
    NSString* replaceString = [NSString stringWithFormat:@" %@ ",[modelClass getTableName]];
    if([sql hasSuffix:@" @t"]){
        sql = [sql stringByAppendingString:@" "];
    }
    NSString* executeSQL = [sql stringByReplacingOccurrencesOfString:@" @t " withString:replaceString options:NSCaseInsensitiveSearch range:NSMakeRange(0, sql.length)];
    
    __block NSMutableArray* results = nil;
    [self executeDB:^(FMDatabase *db) {
        FMResultSet* set = [db executeQuery:executeSQL];
        results = [self executeResult:set Class:modelClass tableName:nil];
        [set close];
    }];
    return results;
}

-(void)sqlString:(NSMutableString*)sql AddOder:(NSString*)orderby offset:(NSInteger)offset count:(NSInteger)count {
    if([LKDBUtils checkStringIsEmpty:orderby] == NO) {
        [sql appendFormat:@" order by %@",orderby];
    }
    if(count>0) {
        [sql appendFormat:@" limit %ld offset %ld",(long)count,(long)offset];
    } else if(offset > 0) {
        [sql appendFormat:@" limit %d offset %ld",INT_MAX,(long)offset];
    }
}

- (NSMutableArray *)executeOneColumnResult:(FMResultSet *)set {
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:0];
    while ([set next]) {
        NSString* string = [set stringForColumnIndex:0];
        if(string) {
            [array addObject:string];
        } else {
            NSData* data = [set dataForColumnIndex:0];
            if(data) {
                [array addObject:data];
            }
        }
    }
    return array;
}

- (NSMutableArray *)executeResult:(FMResultSet *)set Class:(Class)modelClass tableName:(NSString*)tableName {
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:0];
    LKModelInfos* infos = [modelClass getModelInfos];
    int columnCount = [set columnCount];
    while ([set next]) {
        
        NSObject* bindingModel = [[modelClass alloc]init];
        
        for (int i=0; i<columnCount; i++) {
            
            NSString* sqlName = [set columnNameForIndex:i];
            LKDBProperty* property = [infos objectWithSqlColumnName:sqlName];
            
            if(property == nil) {
                continue;
            }
            
            BOOL isUserCalculate = [property.type isEqualToString:LKSQL_Mapping_UserCalculate];
            if([[sqlName lowercaseString] isEqualToString:@"rowid"] && isUserCalculate==NO) {
                bindingModel.rowid = [set intForColumnIndex:i];
            } else {
                if(property.propertyName && isUserCalculate == NO) {
                    NSString* sqlValue = [set stringForColumnIndex:i];
                    [bindingModel modelSetValue:property value:sqlValue];
                } else {
                    NSData* sqlData = [set dataForColumnIndex:i];
                    NSString* sqlValue = [[NSString alloc] initWithData:sqlData encoding:NSUTF8StringEncoding];
                    [bindingModel userSetValueForModel:property value:sqlValue?:sqlData];
                }
            }
        }
        bindingModel.db_tableName = tableName;
        [modelClass dbDidSeleted:bindingModel];
        [array addObject:bindingModel];
    }
    return array;
}

#pragma mark- insert operation
-(BOOL)insertToDB:(NSObject *)model {
    return [self insertBase:model];
}

-(void)insertToDB:(NSObject *)model callback:(void (^)(BOOL))block {
    [self asyncBlock:^{
        BOOL result = [self insertBase:model];
        if(block != nil) {
            block(result);
        }
    }];
}

-(BOOL)insertWhenNotExists:(NSObject *)model {
    if([self isExistsModel:model]==NO) {
        return [self insertToDB:model];
    }
    return NO;
}

-(void)insertWhenNotExists:(NSObject *)model callback:(void (^)(BOOL))block {
    [self asyncBlock:^{
        if(block != nil) {
            block([self insertWhenNotExists:model]);
        } else {
            [self insertWhenNotExists:model];
        }
    }];
}

-(BOOL)insertBase:(NSObject*)model{
    
    checkModelIsInvalid(model);
    
    Class modelClass = model.class;
    
    // callback
    if([modelClass dbWillInsert:model]==NO) {
        LKErrorLog(@"your cancel %@ insert",model);
        return NO;
    }
    
//    [model performSelector:@selector(setDb_inserting:) withObject:@YES];
    
    NSString* db_tableName = model.db_tableName?:[modelClass getTableName];
    
    // 查看表是否已经创建
    if([self.createdTableNames containsObject:db_tableName] == NO) {
        [self _createTableWithModelClass:modelClass tableName:db_tableName];
    }
    
    // --
    LKModelInfos* infos = [modelClass getModelInfos];
    
    NSMutableString* insertKey = [NSMutableString stringWithCapacity:0];
    NSMutableString* insertValuesString = [NSMutableString stringWithCapacity:0];
    NSMutableArray* insertValues = [NSMutableArray arrayWithCapacity:infos.count];
    
    
    LKDBProperty* primaryProperty = [model singlePrimaryKeyProperty];
    
    for (NSInteger i = 0; i < infos.count; i++) {
        LKDBProperty* property = [infos objectWithIndex:i];
        if([LKDBUtils checkStringIsEmpty:property.sqlColumnName]) {
            continue;
        }
        
        if([property isEqual:primaryProperty]) {
            if([property.sqlColumnType isEqualToString:LKSQL_Type_Int] && [model singlePrimaryKeyValueIsEmpty]) {
                continue;
            }
        }
        
        if(insertKey.length>0) {
            [insertKey appendString:@","];
            [insertValuesString appendString:@","];
        }
        
        [insertKey appendString:property.sqlColumnName];
        [insertValuesString appendString:@"?"];
        
        id value = [self modelValueWithProperty:property model:model];
        
        [insertValues addObject:value];
    }
    
    // 拼接insertSQL 语句  采用 replace 插入
    NSString* insertSQL = [NSString stringWithFormat:@"replace into %@(%@) values(%@)",db_tableName,insertKey,insertValuesString];
    
    __block BOOL execute = NO;
    __block sqlite_int64 lastInsertRowId = 0;
    
    [self executeDB:^(FMDatabase *db) {
        execute = [db executeUpdate:insertSQL withArgumentsInArray:insertValues];
        lastInsertRowId= db.lastInsertRowId;
    }];
    
    model.rowid = (int)lastInsertRowId;
    
//    [model performSelector:@selector(setDb_inserting:) withObject:nil];
    if(execute == NO) {
        LKErrorLog(@"database insert fail %@, sql:%@",NSStringFromClass(modelClass),insertSQL);
    }
    
    // callback
    [modelClass dbDidInserted:model result:execute];
    return execute;
}

#pragma mark- update operation
-(BOOL)updateToDB:(NSObject *)model where:(id)where {
    return [self updateToDBBase:model where:where];
}

-(void)updateToDB:(NSObject *)model where:(id)where callback:(void (^)(BOOL))block {
    [self asyncBlock:^{
        BOOL result = [self updateToDBBase:model where:where];
        if(block != nil)
            block(result);
    }];
}

-(BOOL)updateToDBBase:(NSObject *)model where:(id)where {
    checkModelIsInvalid(model);
    
    Class modelClass = model.class;
    
    // callback
    if([modelClass dbWillUpdate:model] == NO) {
        LKErrorLog(@"you cancel %@ update.",model);
        return NO;
    }
    NSString* db_tableName = model.db_tableName?:[modelClass getTableName];
    
    LKModelInfos* infos = [modelClass getModelInfos];
    
    NSMutableString* updateKey = [NSMutableString string];
    NSMutableArray* updateValues = [NSMutableArray arrayWithCapacity:infos.count];
    for (int i = 0; i<infos.count; i++) {
        
        LKDBProperty* property = [infos objectWithIndex:i];
        
        if(i > 0)
            [updateKey appendString:@","];
        
        [updateKey appendFormat:@"%@=?",property.sqlColumnName];
        
        id value = [self modelValueWithProperty:property model:model];
        
        [updateValues addObject:value];
    }
    
    NSMutableString* updateSQL = [NSMutableString stringWithFormat:@"update %@ set %@ where ",db_tableName,updateKey];
    
    NSLog(@"updateSQL = %@",updateSQL);
    
    //添加where 语句
    if([where isKindOfClass:[NSString class]] && [LKDBUtils checkStringIsEmpty:where]== NO) {
        [updateSQL appendString:where];
    } else if([where isKindOfClass:[NSDictionary class]] && [(NSDictionary*)where count]>0) {
        NSMutableArray* valuearray = [NSMutableArray array];
        NSString* sqlwhere = [self dictionaryToSqlWhere:where andValues:valuearray];
        
        [updateSQL appendString:sqlwhere];
        [updateValues addObjectsFromArray:valuearray];
    } else if(model.rowid > 0) {
        [updateSQL appendFormat:@" rowid=%ld",(long)model.rowid];
    } else {
        //如果不通过 rowid 来 更新数据  那 primarykey 一定要有值
        NSString* pwhere = [self primaryKeyWhereSQLWithModel:model addPValues:updateValues];
        if(pwhere.length ==0) {
            LKErrorLog(@"database update fail : %@ no find primary key!",NSStringFromClass(modelClass));
            return NO;
        }
        [updateSQL appendString:pwhere];
    }
    
    BOOL execute = [self executeSQL:updateSQL arguments:updateValues];
    if(execute == NO) {
        LKErrorLog(@"database update fail : %@   -----> update sql: %@",NSStringFromClass(modelClass),updateSQL);
    }
    
    //callback
    [modelClass dbDidUpdated:model result:execute];
    
    return execute;
}

-(BOOL)updateToDB:(Class)modelClass set:(NSString *)sets where:(id)where {
    checkClassIsInvalid(modelClass);
    
    NSMutableString* updateSQL = [NSMutableString stringWithFormat:@"update %@ set %@ ",[modelClass getTableName],sets];
    NSMutableArray* updateValues = [self extractQuery:updateSQL where:where];
    
    BOOL execute = [self executeSQL:updateSQL arguments:updateValues];
    
    if(execute == NO)
        LKErrorLog(@"database update fail %@   ----->sql:%@",NSStringFromClass(modelClass),updateSQL);
    
    return execute;
}

#pragma mark - delete operation
-(BOOL)deleteToDB:(NSObject *)model {
    return [self deleteToDBBase:model];
}

-(void)deleteToDB:(NSObject *)model callback:(void (^)(BOOL))block {
    [self asyncBlock:^{
        BOOL isDeleted = [self deleteToDBBase:model];
        if(block != nil)
            block(isDeleted);
    }];
}

-(BOOL)deleteToDBBase:(NSObject *)model {
    checkModelIsInvalid(model);
    
    Class modelClass = model.class;
    
    //callback
    if([modelClass dbWillDelete:model] == NO) {
        LKErrorLog(@"you cancel %@ delete",model);
        return NO;
    }
    NSString* db_tableName = model.db_tableName?:[modelClass getTableName];
    
    NSMutableString*  deleteSQL = [NSMutableString stringWithFormat:@"delete from %@ where ",db_tableName];
    NSMutableArray* parsArray = [NSMutableArray array];
    if(model.rowid > 0) {
        [deleteSQL appendFormat:@"rowid = %ld",(long)model.rowid];
    } else {
        NSString* pwhere = [self primaryKeyWhereSQLWithModel:model addPValues:parsArray];
        if(pwhere.length==0) {
            LKErrorLog(@"delete fail : %@ primary value is nil",NSStringFromClass(modelClass));
            return NO;
        }
        [deleteSQL appendString:pwhere];
    }
    
    if(parsArray.count == 0) {
        parsArray = nil;
    }
    
    BOOL execute = [self executeSQL:deleteSQL arguments:parsArray];
    
    //callback
    [modelClass dbDidDeleted:model result:execute];
    
    return execute;
}

-(BOOL)deleteWithClass:(Class)modelClass where:(id)where {
    return [self deleteWithClassBase:modelClass where:where];
}

-(void)deleteWithClass:(Class)modelClass where:(id)where callback:(void (^)(BOOL))block {
    [self asyncBlock:^{
        BOOL isDeleted = [self deleteWithClassBase:modelClass where:where];
        if (block != nil) {
            block(isDeleted);
        }
    }];
}

-(BOOL)deleteWithClassBase:(Class)modelClass where:(id)where {
    checkClassIsInvalid(modelClass);
    
    NSMutableString* deleteSQL = [NSMutableString stringWithFormat:@"delete from %@",[modelClass getTableName]];
    NSMutableArray* values = [self extractQuery:deleteSQL where:where];
    
    BOOL result = [self executeSQL:deleteSQL arguments:values];
    return result;
}

#pragma mark - other operation
-(BOOL)isExistsModel:(NSObject *)model {
    checkModelIsInvalid(model);
    NSString* pwhere = nil;
    if(model.rowid>0) {
        pwhere = [NSString stringWithFormat:@"rowid=%ld",(long)model.rowid];
    } else {
        pwhere = [self primaryKeyWhereSQLWithModel:model addPValues:nil];
    }
    if(pwhere.length == 0) {
        LKErrorLog(@"exists model fail: primary key is nil or invalid");
        return NO;
    }
    return [self isExistsClass:model.class where:pwhere];
}

-(BOOL)isExistsClass:(Class)modelClass where:(id)where {
    return [self isExistsClassBase:modelClass where:where];
}

-(BOOL)isExistsClassBase:(Class)modelClass where:(id)where {
    return [self rowCount:modelClass where:where] > 0;
}

#pragma mark- clear operation

+(void)clearTableData:(Class)modelClass {
    [[self getUsingLKDBHelper] executeDB:^(FMDatabase *db) {
        NSString* delete = [NSString stringWithFormat:@"DELETE FROM %@",[modelClass getTableName]];
        [db executeUpdate:delete];
    }];
}

+(void)clearNoneImage:(Class)modelClass columns:(NSArray *)columns {
    [self clearFileWithTable:modelClass columns:columns type:1];
}

+(void)clearNoneData:(Class)modelClass columns:(NSArray *)columns {
    [self clearFileWithTable:modelClass columns:columns type:2];
}

#define LKTestDirFilename @"LKTestDirFilename111"
+(void)clearFileWithTable:(Class)modelClass columns:(NSArray*)columns type:(int)type {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSString* testpath = nil;
        switch (type) {
            case 1:
                testpath = [modelClass getDBImagePathWithName:LKTestDirFilename];
                break;
            case 2:
                testpath = [modelClass getDBDataPathWithName:LKTestDirFilename];
                break;
        }
        
        if([LKDBUtils checkStringIsEmpty:testpath])
            return ;
        
        NSString* dir  = [testpath stringByReplacingOccurrencesOfString:LKTestDirFilename withString:@""];
        
        NSUInteger count =  columns.count;
        
        // 获取该目录下所有文件名
        NSArray* files = [LKDBUtils getFilenamesWithDir:dir];
        
        NSString* seleteColumn = [columns componentsJoinedByString:@","];
        NSMutableString* whereStr =[NSMutableString string];
        for (int i=0; i<count ; i++) {
            [whereStr appendFormat:@" %@ != '' ",[columns objectAtIndex:i]];
            if(i< count -1) {
                [whereStr appendString:@" or "];
            }
        }
        
        NSString* querySql = [NSString stringWithFormat:@"select %@ from %@ where %@",seleteColumn,[modelClass getTableName],whereStr];
        __block NSArray* dbfiles;
        [[self getUsingLKDBHelper] executeDB:^(FMDatabase *db) {
            NSMutableArray* tempfiles = [NSMutableArray arrayWithCapacity:6];
            FMResultSet* set = [db executeQuery:querySql];
            while ([set next]) {
                for (int j=0; j<count; j++) {
                    NSString* str = [set stringForColumnIndex:j];
                    if([LKDBUtils checkStringIsEmpty:str] == NO) {
                        [tempfiles addObject:str];
                    }
                }
            }
            [set close];
            dbfiles = tempfiles;
        }];
        
        // 遍历  当不在数据库记录中 就删除
        for (NSString* deletefile in files) {
            if([dbfiles indexOfObject:deletefile] == NSNotFound) {
                [LKDBUtils deleteWithFilepath:[dir stringByAppendingPathComponent:deletefile]];
            }
        }
    });
}
@end

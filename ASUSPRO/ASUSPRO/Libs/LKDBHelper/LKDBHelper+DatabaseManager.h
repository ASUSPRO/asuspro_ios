//
//  LKDBHelper+DatabaseManager.h
//  LKDBHelper
//
//  Created by abel on 14-9-29.
//  Copyright (c) 2014年 ljh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"

@interface LKDBHelper(DatabaseManager)

// get table has created
-(BOOL)getTableCreatedWithClass:(Class)model;
-(BOOL)getTableCreatedWithTableName:(NSString*)tableName;

// drop all table
-(void)dropAllTable;

// drop table with entity class
-(BOOL)dropTableWithClass:(Class)modelClass;

-(BOOL)_createTableWithModelClass:(Class)modelClass tableName:(NSString*)tableName;

@end

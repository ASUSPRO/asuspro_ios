//
//  LKDBHelper.h
//  upin
//
//  Created by Fanhuan on 12-12-6.
//  Copyright (c) 2012年 linggan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseQueue.h"
#import "FMDatabase.h"

#import "LKDBUtils.h"

#import "LKDB+Mapping.h"

#import "NSObject+LKModel.h"
#import "NSObject+LKDBHelper.h"

#define checkClassIsInvalid(modelClass)if([LKDBUtils checkStringIsEmpty:[modelClass getTableName]]){\
LKErrorLog(@"model class name %@ table name is invalid!",NSStringFromClass(modelClass));\
return NO;}
#define checkModelIsInvalid(model){if(model == nil){LKErrorLog(@"model is nil");return NO;}\
NSString* _model_tableName = model.db_tableName?:[model.class getTableName];\
if(_model_tableName.length == 0){LKErrorLog(@"model class name %@ table name is invalid!",_model_tableName);return NO;}}

@interface LKDBHelper : NSObject

@property(strong,nonatomic)NSMutableArray* createdTableNames;

@property(assign,nonatomic)FMDatabase* usingdb;
@property(strong,nonatomic)FMDatabaseQueue* bindingQueue;
@property(copy,nonatomic)NSString* dbPath;

@property(strong,nonatomic)NSRecursiveLock* threadLock;

+(instancetype)getUsingLKDBHelper;

/**
 *	@brief  filepath the use of : "documents/db/" + fileName + ".db"
 *  refer:  FMDatabase.h  + (instancetype)databaseWithPath:(NSString*)inPath;
 */
-(instancetype)initWithDBName:(NSString*)dbname;
-(void)setDBName:(NSString*)fileName;

/**
 *	@brief  path of database file
 *  refer:  FMDatabase.h  + (instancetype)databaseWithPath:(NSString*)inPath;
 */
-(instancetype)initWithDBPath:(NSString*)filePath;
-(void)setDBPath:(NSString*)filePath;

/**
 *	@brief  execute database operations synchronously,not afraid of recursive deadlock  同步执行数据库操作 可递归调用
 */
-(void)executeDB:(void (^)(FMDatabase *db))block;

-(BOOL)executeSQL:(NSString *)sql arguments:(NSArray *)args;
-(NSString *)executeScalarWithSQL:(NSString *)sql arguments:(NSArray *)args;
- (NSMutableArray *)extractQuery:(NSMutableString *)query where:(id)where;

-(NSString*)dictionaryToSqlWhere:(NSDictionary*)dic andValues:(NSMutableArray*)values;
-(NSMutableString*)primaryKeyWhereSQLWithModel:(NSObject*)model addPValues:(NSMutableArray*)addPValues;
@end

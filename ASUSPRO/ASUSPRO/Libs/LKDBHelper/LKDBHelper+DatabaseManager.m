//
//  LKDBHelper+DatabaseManager.m
//  LKDBHelper
//
//  Created by abel on 14-9-29.
//  Copyright (c) 2014年 ljh. All rights reserved.
//

#import "LKDBHelper+DatabaseManager.h"


@implementation LKDBHelper(DatabaseManager)

-(void)dropAllTable {
    [self executeDB:^(FMDatabase *db) {
        FMResultSet* set = [db executeQuery:@"select name from sqlite_master where type='table'"];
        NSMutableArray* dropTables = [NSMutableArray arrayWithCapacity:0];
        while ([set next]) {
            [dropTables addObject:[set stringForColumnIndex:0]];
        }
        [set close];
        
        // 删除表
        for (NSString* tableName in dropTables) {
            NSString* dropTable = [NSString stringWithFormat:@"drop table %@",tableName];
            [db executeUpdate:dropTable];
        }
    }];
}

-(BOOL)dropTableWithClass:(Class)modelClass {
    checkClassIsInvalid(modelClass);
    
    NSString* tableName = [modelClass getTableName];
    NSString* dropTable = [NSString stringWithFormat:@"drop table %@",tableName];
    
    BOOL isDrop = [self executeSQL:dropTable arguments:nil];
    return isDrop;
}

-(void)fixSqlColumnsWithClass:(Class)clazz tableName:(NSString*)tableName {
    [self executeDB:^(FMDatabase *db) {
        
        LKModelInfos* infos = [clazz getModelInfos];
        
        NSString* select = [NSString stringWithFormat:@"select * from %@ limit 0",tableName];
        FMResultSet* set = [db executeQuery:select];
        NSArray*  columnArray = set.columnNameToIndexMap.allKeys;
        [set close];
        
        NSMutableArray* alterAddColumns = [NSMutableArray array];
        for (NSInteger i=0; i<infos.count; i++) {
            LKDBProperty* property = [infos objectWithIndex:i];
            if([property.sqlColumnName.lowercaseString isEqualToString:@"rowid"]) {
                continue;
            }
            
            ///数据库中不存在 需要alter add
            if([columnArray containsObject:property.sqlColumnName.lowercaseString] == NO) {
                NSMutableString* addColumePars = [NSMutableString stringWithFormat:@"%@ %@",property.sqlColumnName,property.sqlColumnType];
                [clazz columnAttributeWithProperty:property];
                
                if(property.length>0 && [property.sqlColumnType isEqualToString:LKSQL_Type_Text])
                    [addColumePars appendFormat:@"(%d)",property.length];
                
                if(property.isNotNull)
                    [addColumePars appendFormat:@" %@",LKSQL_Attribute_NotNull];
                
                if(property.checkValue)
                    [addColumePars appendFormat:@" %@(%@)",LKSQL_Attribute_Check,property.checkValue];
                
                if(property.defaultValue)
                    [addColumePars appendFormat:@" %@ %@",LKSQL_Attribute_Default,property.defaultValue];
                
                NSString* alertSQL = [NSString stringWithFormat:@"alter table %@ add column %@",tableName,addColumePars];
                NSString* initColumnValue = [NSString stringWithFormat:@"update %@ set %@=%@",tableName,property.sqlColumnName,[property.sqlColumnType isEqualToString:LKSQL_Type_Text]?@"''":@"0"];
                
                BOOL success = [db executeUpdate:alertSQL];
                if(success) {
                    [db executeUpdate:initColumnValue];
                    [alterAddColumns addObject:property];
                }
            }
        }
        
        if(alterAddColumns.count > 0) {
            [clazz dbDidAlterTable:self tableName:tableName addColumns:alterAddColumns];
        }
    }];
}

-(BOOL)_createTableWithModelClass:(Class)modelClass tableName:(NSString*)tableName {
    checkClassIsInvalid(modelClass);
    if([self getTableCreatedWithTableName:tableName]) {
        // 已创建表 就跳过
        if([self.createdTableNames containsObject:tableName] == NO) {
            [self.createdTableNames addObject:tableName];
        }
        [self fixSqlColumnsWithClass:modelClass tableName:tableName];
        return YES;
    }
    
    LKModelInfos* infos = [modelClass getModelInfos];
    NSArray* primaryKeys = infos.primaryKeys;
    BOOL isAutoinc = NO;
    if(primaryKeys.count == 1) {
        // 主键只有一个 并且是 int 类型 则设置为自增长
        NSString* primaryType = [infos objectWithSqlColumnName:[primaryKeys lastObject]].sqlColumnType;
        if([primaryType isEqualToString:LKSQL_Type_Int]) {
            isAutoinc = YES;
        }
    }
    NSMutableString* table_pars = [NSMutableString string];
    for (NSInteger i = 0; i < infos.count; i++) {
        if(i > 0) {
            [table_pars appendString:@","];
        }
        
        LKDBProperty* property =  [infos objectWithIndex:i];
        [modelClass columnAttributeWithProperty:property];
        
        NSString* columnType = property.sqlColumnType;
        if([columnType isEqualToString:LKSQL_Type_Double]) {
            columnType = LKSQL_Type_Text;
        }
        
        [table_pars appendFormat:@"%@ %@",property.sqlColumnName,columnType];
        
        if([property.sqlColumnType isEqualToString:LKSQL_Type_Text]) {
            if(property.length>0) {
                [table_pars appendFormat:@"(%d)",property.length];
            }
        }
        
        if(property.isNotNull) {
            [table_pars appendFormat:@" %@",LKSQL_Attribute_NotNull];
        }
        
        if(property.isUnique) {
            [table_pars appendFormat:@" %@",LKSQL_Attribute_Unique];
        }
        
        if(property.checkValue) {
            [table_pars appendFormat:@" %@(%@)",LKSQL_Attribute_Check,property.checkValue];
        }
        
        if(property.defaultValue) {
            [table_pars appendFormat:@" %@ %@",LKSQL_Attribute_Default,property.defaultValue];
        }
        
        if(isAutoinc) {
            if([property.sqlColumnName isEqualToString:[primaryKeys lastObject]]) {
                [table_pars appendString:@" primary key autoincrement"];
            }
        }
    }
    NSMutableString* pksb = [NSMutableString string];
    
    ///联合主键
    if(isAutoinc == NO) {
        if(primaryKeys.count>0) {
            pksb = [NSMutableString string];
            for (int i=0; i<primaryKeys.count; i++) {
                NSString* pk = [primaryKeys objectAtIndex:i];
                
                if(pksb.length>0){
                    [pksb appendString:@","];
                }
                
                [pksb appendString:pk];
            }
            if(pksb.length>0) {
                [pksb insertString:@",primary key(" atIndex:0];
                [pksb appendString:@")"];
            }
        }
    }
    
    NSString* createTableSQL = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@%@)",tableName,table_pars,pksb];
    
    BOOL isCreated = [self executeSQL:createTableSQL arguments:nil];
    
    if(isCreated) {
        [self.createdTableNames addObject:tableName];
        [modelClass dbDidCreateTable:self tableName:tableName];
    }
    
    return isCreated;
}

-(BOOL)getTableCreatedWithClass:(Class)modelClass {
    return [self getTableCreatedWithTableName:[modelClass getTableName]];
}

-(BOOL)getTableCreatedWithTableName:(NSString *)tableName {
    __block BOOL isTableCreated = NO;
    [self executeDB:^(FMDatabase *db) {
        FMResultSet* set = [db executeQuery:@"select count(name) from sqlite_master where type='table' and name=?",tableName];
        [set next];
        if([set intForColumnIndex:0]>0) {
            isTableCreated = YES;
        }
        [set close];
    }];
    return isTableCreated;
}
@end

//
//  LKDBHelper.m
//  upin
//
//  Created by Fanhuan on 12-12-6.
//  Copyright (c) 2012年 linggan. All rights reserved.
//

#import "LKDBHelper.h"

@interface LKDBWeakObject : NSObject
@property(assign,nonatomic)LKDBHelper* obj;
@end

@interface LKDBHelper()
@end

@implementation LKDBHelper

+(NSMutableArray*)dbHelperSingleArray {
    static __strong NSMutableArray* dbArray;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dbArray = [NSMutableArray array];
    });
    return dbArray;
}

+(LKDBHelper*)dbHelperWithPath:(NSString*)dbFilePath save:(LKDBHelper*)helper {
    NSMutableArray* dbArray = [self dbHelperSingleArray];
    
    if(helper) {
        LKDBWeakObject* weakObj = [LKDBWeakObject new];
        weakObj.obj = helper;
        [dbArray addObject:weakObj];
    } else if(dbFilePath) {
        for (int i=0; i<dbArray.count; i++) {
            LKDBWeakObject* weakObj = [dbArray objectAtIndex:i];
            if(weakObj.obj == nil) {
                [dbArray removeObject:weakObj];
                continue;
            } else if([weakObj.obj.dbPath isEqualToString:dbFilePath]) {
                return weakObj.obj;
            }
        }
    }
    return nil;
}

+(instancetype)getUsingLKDBHelper {
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* dbpath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"/DataBase/NetChampin.db"];
        db = [[LKDBHelper alloc]initWithDBPath:dbpath];
    });
    return db;
}

- (instancetype)init {
    return [self initWithDBName:@"LKDB"];
}

-(instancetype)initWithDBName:(NSString *)dbname {
    return [self initWithDBPath:[LKDBHelper getDBPathWithDBName:dbname]];
}

-(instancetype)initWithDBPath:(NSString *)filePath {
    if([LKDBUtils checkStringIsEmpty:filePath]) {
        self = nil;
        return self;
    }
    
    LKDBHelper* helper = [LKDBHelper dbHelperWithPath:filePath save:nil];
    if(helper) {
        self = helper;
    } else {
        self = [super init];
        if (self) {
            self.threadLock = [[NSRecursiveLock alloc]init];
            self.createdTableNames = [NSMutableArray array];
            
            [self setDBPath:filePath];
            [LKDBHelper dbHelperWithPath:nil save:self];
        }
    }
    return self;
}

#pragma mark- init FMDB
+(NSString*)getDBPathWithDBName:(NSString*)dbName {
    NSString* fileName = nil;
    if([dbName hasSuffix:@".db"] == NO) {
        fileName = [NSString stringWithFormat:@"%@.db",dbName];
    } else {
        fileName = dbName;
    }
    
    NSString* filePath = [LKDBUtils getPathForDocuments:fileName inDir:@"db"];
    return filePath;
}

-(void)setDBName:(NSString *)dbName {
    [self setDBPath:[LKDBHelper getDBPathWithDBName:dbName]];
}

-(void)setDBPath:(NSString *)filePath {
    if(self.bindingQueue && [self.dbPath isEqualToString:filePath]) {
        return;
    }
    
    //创建数据库目录
    NSRange lastComponent = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
    if(lastComponent.length > 0) {
        NSString* dirPath = [filePath substringToIndex:lastComponent.location];
        BOOL isDir = NO;
        BOOL isCreated = [[NSFileManager defaultManager] fileExistsAtPath:dirPath isDirectory:&isDir];
        if ( isCreated == NO || isDir == NO ) {
            NSError* error = nil;
            BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
            if(success == NO) {
                NSLog(@"create dir error: %@",error.debugDescription);
            }
        }
    }
    self.dbPath = filePath;
    [self.bindingQueue close];
    self.bindingQueue = [[FMDatabaseQueue alloc]initWithPath:filePath];
    
#ifdef DEBUG
    //debug 模式下  打印错误日志
    [_bindingQueue inDatabase:^(FMDatabase *db) {
        db.logsErrors = YES;
    }];
#endif
}

#pragma mark- core
-(void)executeDB:(void (^)(FMDatabase *db))block {
    [_threadLock lock];
    if(self.usingdb != nil) {
        block(self.usingdb);
    } else {
        if(_bindingQueue == nil) {
            _bindingQueue = [[FMDatabaseQueue alloc]initWithPath:_dbPath];
        }
        
        [_bindingQueue inDatabase:^(FMDatabase *db) {
            self.usingdb = db;
            block(db);
            self.usingdb = nil;
        }];
    }
    [_threadLock unlock];
}

-(BOOL)executeSQL:(NSString *)sql arguments:(NSArray *)args {
    __block BOOL execute = NO;
    [self executeDB:^(FMDatabase *db) {
        if(args.count>0)
            execute = [db executeUpdate:sql withArgumentsInArray:args];
        else
            execute = [db executeUpdate:sql];
    }];
    return execute;
}

-(NSString *)executeScalarWithSQL:(NSString *)sql arguments:(NSArray *)args {
    __block NSString* scalar = nil;
    [self executeDB:^(FMDatabase *db) {
        FMResultSet* set = nil;
        if(args.count>0)
            set = [db executeQuery:sql withArgumentsInArray:args];
        else
            set = [db executeQuery:sql];
        
        if([set columnCount]>0 && [set next]) {
            scalar = [set stringForColumnIndex:0];
        }
        [set close];
    }];
    return scalar;
}


// splice 'where' 拼接where语句
- (NSMutableArray *)extractQuery:(NSMutableString *)query where:(id)where {
    NSMutableArray* values = nil;
    if([where isKindOfClass:[NSString class]] && [LKDBUtils checkStringIsEmpty:where]==NO) {
        [query appendFormat:@" where %@",where];
    } else if ([where isKindOfClass:[NSDictionary class]]) {
        NSDictionary* dicWhere = where;
        if(dicWhere.count > 0) {
            values = [NSMutableArray arrayWithCapacity:dicWhere.count];
            NSString* wherekey = [self dictionaryToSqlWhere:where andValues:values];
            [query appendFormat:@" where %@",wherekey];
        }
    }
    return values;
}

// dic where parse
-(NSString*)dictionaryToSqlWhere:(NSDictionary*)dic andValues:(NSMutableArray*)values {
    NSMutableString* wherekey = [NSMutableString stringWithCapacity:0];
    if(dic != nil && dic.count >0 ) {
        NSArray* keys = dic.allKeys;
        for (int i=0; i< keys.count;i++) {
            
            NSString* key = [keys objectAtIndex:i];
            id va = [dic objectForKey:key];
            if([va isKindOfClass:[NSArray class]]) {
                NSArray* vlist = va;
                if(vlist.count==0)
                    continue;
                
                if(wherekey.length > 0)
                    [wherekey appendString:@" and"];
                
                [wherekey appendFormat:@" %@ in(",key];
                
                for (int j=0; j<vlist.count; j++) {
                    
                    [wherekey appendString:@"?"];
                    if(j== vlist.count-1)
                        [wherekey appendString:@")"];
                    else
                        [wherekey appendString:@","];
                    
                    [values addObject:[vlist objectAtIndex:j]];
                }
            } else {
                if(wherekey.length > 0)
                    [wherekey appendFormat:@" and %@=?",key];
                else
                    [wherekey appendFormat:@" %@=?",key];
                
                [values addObject:va];
            }
            
        }
    }
    return wherekey;
}

// where sql statements about model primary keys
-(NSMutableString*)primaryKeyWhereSQLWithModel:(NSObject*)model addPValues:(NSMutableArray*)addPValues
{
    LKModelInfos* infos = [model.class getModelInfos];
    NSArray* primaryKeys = infos.primaryKeys;
    NSMutableString* pwhere = [NSMutableString string];
    if(primaryKeys.count>0) {
        for (int i=0; i<primaryKeys.count; i++) {
            NSString* pk = [primaryKeys objectAtIndex:i];
            if([LKDBUtils checkStringIsEmpty:pk] == NO) {
                LKDBProperty* property = [infos objectWithSqlColumnName:pk];
                id pvalue = nil;
                if(property && [property.type isEqualToString:LKSQL_Mapping_UserCalculate]) {
                    pvalue = [model userGetValueForModel:property];
                } else if(pk && property) {
                    pvalue = [model modelGetValue:property];
                }
                
                if(pvalue) {
                    if(pwhere.length>0) {
                        [pwhere appendString:@"and"];
                    }
                    
                    if(addPValues) {
                        [pwhere appendFormat:@" %@=? ",pk];
                        [addPValues addObject:pvalue];
                    } else {
                        [pwhere appendFormat:@" %@='%@' ",pk,pvalue];
                    }
                }
            }
        }
    }
    return pwhere;
}

#pragma mark- dealloc
-(void)dealloc {
    NSArray* array = [LKDBHelper dbHelperSingleArray];
    for (LKDBWeakObject* weakObject in array) {
        if([weakObject.obj isEqual:self]) {
            weakObject.obj = nil;
        }
    }
    
    [self.bindingQueue close];
    self.usingdb = nil;
    self.bindingQueue = nil;
    self.dbPath = nil;
    self.threadLock = nil;
}

@end



@implementation LKDBWeakObject

@end

//
//  HBPredicate.m
//  HuoBan
//
//  Created by abel on 14/11/5.
//  Copyright (c) 2014年 Huoban inc. All rights reserved.
//

#import "HBPredicate.h"

@implementation HBPredicate

+(instancetype)predicateWithFormat:(NSString *)predicateFormat {
    HBPredicate * predicate = [[HBPredicate alloc] init];
    predicate.predicate = predicateFormat;
    return predicate;
}

@end

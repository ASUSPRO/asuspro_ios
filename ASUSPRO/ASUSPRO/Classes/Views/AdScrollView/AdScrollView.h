//
//  AdScrollView.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/19.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AdPageCallback)(NSInteger clickIndex);

@protocol AdPageViewDelegate <NSObject>
/**
 *  加载网络图片使用回调自行调用SDImage
 *
 *  @param imgView
 *  @param imgUrl
 */
- (void)setWebImage:(UIImageView*)imgView imgUrl:(NSString*)imgUrl;

- (void)clickAdImageWithIndex:(NSInteger)clickIndex;
@end


@interface AdScrollView : UIView
@property(nonatomic,assign)NSInteger                iDisplayTime; //广告图片轮播时停留的时间，默认0秒不会轮播
@property(nonatomic,assign)BOOL                     bWebImage; //设置是否为网络图片
@property(nonatomic,strong)UIPageControl            *pageControl;
@property(nonatomic,assign)id<AdPageViewDelegate>  delegate;
/**
 *  启动函数
 *
 *  @param imageArray 设置图片数组
 *  @param block      block，回调点击
 */
- (void)startAdsWithBlock:(NSArray*)imageArray block:(AdPageCallback)block;

@end

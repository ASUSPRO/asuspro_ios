//
//  ZSDPaymentView.h
//  demo
//
//  Created by shaw on 15/4/11.
//  Copyright (c) 2015年 shaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSDPaymentForm.h"

@protocol PaymentCompleteDelegate

- (void)paymentComplete:(NSString *)payPassword;

@end

@interface ZSDPaymentView : UIView

@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *goodsName;
@property (nonatomic,assign) CGFloat amount;
@property (nonatomic,strong) ZSDPaymentForm *paymentForm;
@property (nonatomic)id<PaymentCompleteDelegate> paymentCompleteDelegate;

-(void)show;
-(void)dismiss;
@end

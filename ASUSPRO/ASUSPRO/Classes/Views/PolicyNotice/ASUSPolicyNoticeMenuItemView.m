//
//  ASUSPolicyNoticeMenuItemView.m
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPolicyNoticeMenuItemView.h"
#import "NCColorUtil.h"

@implementation ASUSPolicyNoticeMenuItemView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.menuButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self.menuButton setTitleColor:[NCColorUtil colorFromHexString:@"#606366"] forState:UIControlStateNormal];
        self.menuButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.menuButton addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.menuButton];
    }
    return self;
}

- (void)click:(UIButton *)button {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickItem: sender:)]) {
        [self.delegate clickItem:[self.menuItem.categoryId integerValue] sender:self];
    }
}

- (void)setMenuItem:(ASUSProductModel *)menuItem {
    if (_menuItem != menuItem) {
        if (_menuItem) {
            _menuItem = nil;
        }
        _menuItem = menuItem;
    }
    [self.menuButton setTitle:menuItem.categoryName forState:UIControlStateNormal];
}
@end

//
//  ASUSPolicyNoticeMenuItemView.h
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUSProductModel.h"
@protocol ASUSPolicyNoticeMenuItemViewDelegate <NSObject>

@optional
- (void)clickItem:(NSInteger)index sender:(id)sender;
- (void)clickItem:(NSString *)title;
- (void)clickView;
- (void)deleteItem:(NSInteger)index;
- (void)downListButtonClicked;
- (void)showMenuView;
- (void)dismissMenuView;
@end
@interface ASUSPolicyNoticeMenuItemView : UIView
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) ASUSProductModel *menuItem;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign)id <ASUSPolicyNoticeMenuItemViewDelegate> delegate;
@end

//
//  ASUSPolicyNoticeMenuView.h
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASUSPolicyNoticeMenuItemView.h"

@interface ASUSPolicyNoticeMenuView : UIView <ASUSPolicyNoticeMenuItemViewDelegate>
@property (nonatomic, assign) NSInteger columnNum;
@property (nonatomic, strong, readonly) NSMutableArray *itemViews;
@property (nonatomic, assign) CGFloat rowWidth;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, strong) NSMutableArray *columeItemArray;
@property (nonatomic, strong) UIButton *downListButton;
@property (nonatomic, strong) UIButton *menuBackgroundView;
@property (nonatomic, strong) UIView *menuContentView;
@property (nonatomic, assign) BOOL isUnfold;     // 展开与否
@property (nonatomic, assign) CGRect menuViewDefaultFrame;
@property (nonatomic, assign)id <ASUSPolicyNoticeMenuItemViewDelegate> delegate;
- (void)layoutColumnViews:(NSMutableArray *)columnViews;
+ (CGFloat)getCellHeight:(NSArray *)columeItemArray;
@end

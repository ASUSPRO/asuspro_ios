//
//  ASUSPolicyNoticeMenuView.m
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPolicyNoticeMenuView.h"
#import "NCImageUtil.h"

#define RowHeight 50
#define ColumeNum 3.0

@interface ASUSPolicyNoticeMenuView ()

@end
@implementation ASUSPolicyNoticeMenuView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _columnNum = ColumeNum;
        _rowWidth = ScreenWidth;
        _rowHeight = RowHeight;
        _itemViews = [[NSMutableArray alloc] initWithCapacity:0];
        [self setupViews:frame];

    }
    return self;
}

- (void)setupViews:(CGRect)frame {
    self.menuBackgroundView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.menuBackgroundView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5];
    [self.menuBackgroundView addTarget:self action:@selector(menuBackgroundViewClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.menuBackgroundView];
    self.menuContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    self.menuContentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.menuContentView];
    self.downListButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.downListButton.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    [self.downListButton setTitle:@"全部分类" forState:UIControlStateNormal];
    [self.downListButton layoutIfNeeded];
    [self.downListButton setTitleColor:[NCColorUtil colorFromHexString:@"#606366"] forState:UIControlStateNormal];
    self.downListButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.downListButton setImage:[NCImageUtil imageFromText:@"\ue602" fontSize:13 color:[NCColorUtil colorFromHexString:@"#606366"]] forState:UIControlStateNormal];
    [self.downListButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - 20, 0, 5)];
    [self.downListButton setImageEdgeInsets:UIEdgeInsetsMake(0, self.downListButton.titleLabel.bounds.size.width, 0, - self.downListButton.titleLabel.bounds.size.width)];
    [self.downListButton addTarget:self action:@selector(downListButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.downListButton];
    self.isUnfold = NO;
    self.menuViewDefaultFrame = frame;
}

- (void)removeAllItemView {
    for (UIView *subView in [self.menuContentView subviews]) {
        if ([subView isKindOfClass:[ASUSPolicyNoticeMenuItemView class]]) {
            [subView removeFromSuperview];
        }
    }
}

- (void)layoutColumnViews:(NSMutableArray *)columnViews {
    if (_itemViews != columnViews) {
        if (_itemViews) {
            _itemViews = nil;
        }
        _itemViews = columnViews;
    }
    
    if (self.isUnfold) {
        [self showMenuView];
    } else {
        [self dismissMenuView];
    }
    
    [self removeAllItemView];
    //2 * SideSpace 两边的间距 (_columnNum - 1) * ItemSpace 除去项与项之间的间距
    CGFloat itemWidth = _rowWidth/_columnNum*1.0;
    for (int i = 0; i < columnViews.count; i++){
        ASUSPolicyNoticeMenuItemView *view = [[ASUSPolicyNoticeMenuItemView alloc] initWithFrame:CGRectMake(itemWidth* (i % _columnNum), self.menuViewDefaultFrame.size.height + _rowHeight * (i / _columnNum) , itemWidth, _rowHeight)];
        view.delegate = self;
        [self.menuContentView addSubview:view];
        ASUSProductModel *item = [_columeItemArray objectAtIndex:i];
        view.delegate = self;
        view.menuItem = item;
        [view setIndex:i];
    }
}

+ (CGFloat)getCellHeight:(NSArray *)columeItemArray {
    return ceil(([columeItemArray count]+1)/ColumeNum) * RowHeight;
}

- (void)dismissMenuView {
    if (_delegate && [_delegate respondsToSelector:@selector(dismissMenuView)]) {
        [_delegate dismissMenuView];
    }
     [self.downListButton setImage:[NCImageUtil imageFromText:@"\ue602" fontSize:13 color:[NCColorUtil colorFromHexString:@"#606366"]] forState:UIControlStateNormal];
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = self.menuViewDefaultFrame;
        self.menuContentView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
        self.menuBackgroundView.frame = CGRectMake(0, 0, self.frame.size.width, 0);
        self.menuContentView.hidden = YES;
        self.menuBackgroundView.hidden = YES;
        self.menuContentView.backgroundColor = [UIColor whiteColor];
    }];
}

- (void)showMenuView {
    if (_delegate && [_delegate respondsToSelector:@selector(showMenuView)]) {
        [_delegate showMenuView];
    }
    self.menuContentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1.0];
//    [UIView animateWithDuration:0.3 animations:^{
     [self.downListButton setImage:[NCImageUtil imageFromText:@"\ue61c" fontSize:11 color:[NCColorUtil colorFromHexString:@"#606366"]] forState:UIControlStateNormal];
        if (_itemViews.count%_columnNum == 0) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _rowWidth,  ScreenHeight - self.frame.origin.y);
            self.menuContentView.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _rowWidth, (_itemViews.count / _columnNum) * _rowHeight + self.menuViewDefaultFrame.size.height);
            self.menuContentView.hidden = NO;
            self.menuBackgroundView.hidden = NO;
        } else {
            self.frame = CGRectMake(self.menuViewDefaultFrame.origin.x, self.menuViewDefaultFrame.origin.y, _rowWidth, ScreenHeight - self.frame.origin.y);
            self.menuContentView.frame = CGRectMake(self.frame.origin.x, 0, _rowWidth, (1 + _itemViews.count / _columnNum) * _rowHeight + self.menuViewDefaultFrame.size.height);
            self.menuContentView.hidden = NO;
            self.menuBackgroundView.hidden = NO;
        }
        self.menuBackgroundView.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, ScreenHeight - self.frame.origin.y);
//    }];
}

#pragma mark -
#pragma mark - UIButton Action

- (void)downListButtonClicked:(UIButton *)button {
//    button.selected = !button.selected;
    self.isUnfold = !self.isUnfold;
    [self layoutColumnViews:self.columeItemArray];
    if (_delegate && [_delegate respondsToSelector:@selector(downListButtonClicked)]) {
        [_delegate downListButtonClicked];
    }
}

- (void)menuBackgroundViewClicked {
    self.isUnfold = !self.isUnfold;
    [self dismissMenuView];
}

#pragma mark -
#pragma mark - ASUSPolicyNoticeMenuItemViewDelegate

- (void)clickItem:(NSInteger)index sender:(id )sender {
    self.downListButton.selected = !self.downListButton.selected;
    if ([sender isKindOfClass:[ASUSPolicyNoticeMenuItemView class]]) {
        [self.downListButton setTitle:((ASUSPolicyNoticeMenuItemView *)sender).menuButton.titleLabel.text forState:UIControlStateNormal];
    }
    [self dismissMenuView];
    if (_delegate && [_delegate respondsToSelector:@selector(clickItem: sender:)]) {
        [_delegate clickItem:index sender:sender];
    }
}
    

@end

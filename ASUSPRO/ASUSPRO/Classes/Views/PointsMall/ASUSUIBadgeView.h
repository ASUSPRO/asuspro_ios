//
//  ASUSUIBadgeView.h
//  ASUSPRO
//
//  Created by May on 15-5-12..

//

#import <UIKit/UIKit.h>

@interface ASUSUIBadgeView : UIImageView
//if you set the maxNumber =99, and when the badgenumber greater than maxNumber ,the lable view will display "maxNumber+"  eg:"99+".
//The maxNumber default value is 99
@property(nonatomic,assign)long long maxNumber;
- (void)setBadgeNumber:(long long)number;
@property(nonatomic,strong)UILabel *badgeNumberLabel;
@end

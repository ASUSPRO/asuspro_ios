//
//  ASUSProductListCellView.h
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASUSGiftModel.h"
@protocol ASUSProductListCellViewDelegete <NSObject>
- (void)tapProductImageViewActionOfCellView:(UITapGestureRecognizer *)tapGesture;
@end

@interface ASUSProductListCellView : UIView
@property (nonatomic, assign) id<ASUSProductListCellViewDelegete> delegate;
@property (nonatomic, strong) ASUSGiftModel *giftModel;
@end

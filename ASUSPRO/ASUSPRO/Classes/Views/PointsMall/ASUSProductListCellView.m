//
//  ASUSProductListCellView.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductListCellView.h"
#import "NCColorUtil.h"
#import "UIImageView+AFNetworking.h"

@interface ASUSProductListCellView ()
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *pointsLabel;
@end

@implementation ASUSProductListCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupViews:frame];
    }
    return self;
}

- (void)setupViews:(CGRect)frame {
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.backgroundView];
    
    self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(((ScreenWidth - 1) / 2 - 135)/2
                                                                          , 10, 135, 135)];
    self.productImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.productImageView.clipsToBounds = YES;
    [self addSubview:self.productImageView];
    
    self.productNameLabel = [self createLabel:CGRectMake(10 ,self.productImageView.frame.origin.y + self.productImageView.frame.size.height, (ScreenWidth - 1)/2 - 20, 40) text:@"苹果（Apple）iphone 6 Plus(A1524) 64GB" font:[UIFont systemFontOfSize:14]];
    self.productNameLabel.numberOfLines = 2;
    self.productNameLabel.textColor = [NCColorUtil colorForPointsMallProductNameColor];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    [self  addSubview:self.productNameLabel];
    
    self.pointsLabel = [self createLabel:CGRectMake(10, self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height + 3, (ScreenWidth - 1)/2 - 20, 20) text:@"2000积分" font:[UIFont systemFontOfSize:14]];
    self.pointsLabel.backgroundColor = [UIColor clearColor];
    self.pointsLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    NSMutableAttributedString *pointsStr = [[NSMutableAttributedString alloc] initWithString:self.pointsLabel.text];
    [pointsStr addAttribute:NSFontAttributeName value:(id)[UIFont systemFontOfSize:16] range:NSMakeRange(0, self.pointsLabel.text.length - 2)];
    [pointsStr addAttribute:NSFontAttributeName value:(id)[UIFont systemFontOfSize:14] range:NSMakeRange(self.pointsLabel.text.length - 2, 2)];
    self.pointsLabel.textAlignment = NSTextAlignmentLeft;
    self.pointsLabel.attributedText = pointsStr;
    [self addSubview:self.pointsLabel];
    
    UIView *verticalSeperator = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/2 - 0.5, 0, 0.5, self.frame.size.height)];
    verticalSeperator.backgroundColor = [NCColorUtil colorFromHexString:@"#d9d9d9"];
    [self addSubview:verticalSeperator];
    
    UIView *horizontalSeperator = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 0.5, self.frame.size.width, 0.5)];
    horizontalSeperator.backgroundColor = [NCColorUtil colorFromHexString:@"#d9d9d9"];
    [self addSubview:horizontalSeperator];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapProductImageViewAction:)];
    [self addGestureRecognizer:tapGesture];
}

- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text font:(UIFont *)font{
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = font;
    tempLabel.textColor = [UIColor blackColor];
    return tempLabel;
}

- (void)setGiftModel:(ASUSGiftModel *)gModel {
    _giftModel = gModel;
    if (self.giftModel.giftPicUrl) {
        [self.productImageView setImageWithURL:[NSURL URLWithString:self.giftModel.giftPicUrl] placeholderImage:nil];
    }
    self.productNameLabel.text = self.giftModel.giftName;
    NSMutableAttributedString *pointsStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld积分",(long)self.giftModel.outerScore]];
    self.pointsLabel.text = [NSString stringWithFormat:@"%ld积分",(long)self.giftModel.outerScore];
    [pointsStr addAttribute:NSFontAttributeName value:(id)[UIFont systemFontOfSize:16] range:NSMakeRange(0, self.pointsLabel.text.length - 2)];
    [pointsStr addAttribute:NSFontAttributeName value:(id)[UIFont systemFontOfSize:14] range:NSMakeRange(self.pointsLabel.text.length - 2, 2)];
    self.pointsLabel.attributedText = pointsStr;
}

#pragma mark - Button Actions

- (void)tapProductImageViewAction:(UITapGestureRecognizer *)tapGesture {
    [_delegate tapProductImageViewActionOfCellView:tapGesture];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.backgroundView.backgroundColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1.0];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.backgroundView.backgroundColor = [UIColor whiteColor];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.backgroundView.backgroundColor = [UIColor whiteColor];
}

@end

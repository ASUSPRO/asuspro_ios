//
//  ASUSUIBadgeView.m
//  ASUSPRO
//
//  Created by May on 15-5-12.

//

#import "ASUSUIBadgeView.h"
#import "NCImageUtil.h"
#import "NCColorUtil.h"

@interface ASUSUIBadgeView()
@end

@implementation ASUSUIBadgeView
@synthesize badgeNumberLabel;
@synthesize maxNumber;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUnReadView];
        self.maxNumber=99;
    }
    return self;
}
-(void)dealloc{
    self.badgeNumberLabel=nil;
}

-(void)initUnReadView{
    
    self.image = [NCImageUtil createImageWithColor:[UIColor whiteColor]];
    self.badgeNumberLabel=[[UILabel alloc]init];
    [self.badgeNumberLabel setTextColor:[NCColorUtil colorForHomeCellTitleColor      ]];
    [self.badgeNumberLabel setBackgroundColor:[UIColor clearColor]];
    [self.badgeNumberLabel setFont:[UIFont systemFontOfSize:10]];
    self.badgeNumberLabel.textAlignment=NSTextAlignmentCenter;
    [self addSubview:self.badgeNumberLabel];
    
}
- (void)setBadgeNumber:(long long)number
{
    if(number<=0){
        self.hidden=YES;
    }else{
        NSString *countStr=[NSString stringWithFormat:@"%lld",number];
        
        CGSize theStringSize=CGSizeMake(20, 14);
        if(number>maxNumber){
            countStr=[[NSString stringWithFormat:@"%lld",maxNumber ] stringByAppendingString:@"+"];
            CGSize size= [countStr sizeWithFont:[UIFont systemFontOfSize:10]];
            theStringSize=CGSizeMake(size.width+8, 14);
        }
        
        self.badgeNumberLabel.text=countStr;
        self.badgeNumberLabel.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        self.hidden=NO;
        self.badgeNumberLabel.center=CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    }
    
}
@end

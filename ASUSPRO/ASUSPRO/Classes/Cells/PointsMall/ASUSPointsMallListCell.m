//
//  ASUSPointsMallListCell.m
//  ASUSPRO
//
//  Created by May on 15-5-18.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPointsMallListCell.h"
#import "UIImageView+AFNetworking.h"

@interface ASUSPointsMallListCell ()
@property (nonatomic, strong) UIImageView *productImageView;
@end

@implementation ASUSPointsMallListCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0
                                                                              , 0, ScreenWidth, 200)];
//        self.productImageView.image = [UIImage imageNamed:@"1.jpg"];
        self.productImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:self.productImageView];

    }
    return self;
}

- (void)refreshUI {
    if (self.shopModel.recommendPicUrl) {
        [self.productImageView setImageWithURL:[NSURL URLWithString:self.shopModel.recommendPicUrl] placeholderImage:[UIImage imageNamed:nil]];
    }
}

@end

//
//  ASUSProductMenuCell.m
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductMenuCell.h"
#import "NCColorUtil.h"

@interface ASUSProductMenuCell ()
@end

@implementation ASUSProductMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews {
    self.menuNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, ScreenWidth - 20, 40)];
    self.menuNameLabel.textAlignment = NSTextAlignmentLeft;
    self.menuNameLabel.font = [UIFont systemFontOfSize:16.0];
    self.menuNameLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    self.menuNameLabel.text = @"";
    [self addSubview:self.menuNameLabel];
}

- (void)configViewsFrame:(BOOL )is {
    self.menuNameLabel.frame = CGRectMake(20, 0, ScreenWidth - 20, 40);
}

- (void)refreshUI {
    if (self.menuModel.type == 1) {
        self.menuNameLabel.frame = CGRectMake(20, 0, ScreenWidth - 20, 40);
        self.menuNameLabel.font = [UIFont systemFontOfSize:16.0];
    } else {
        self.menuNameLabel.frame = CGRectMake(50, 0, ScreenWidth - 20, 40);
        self.menuNameLabel.font = [UIFont systemFontOfSize:15.0];
    }
    self.menuNameLabel.text = self.menuModel.categoryName;
    
}

@end

//
//  ASUSExpressCell.h
//  ASUSPRO
//
//  Created by May on 15-6-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSExpressModel.h"
#define kASUSExpressCellHeight 100
@interface ASUSExpressCell : ASUSBaseCell
@property (nonatomic, strong) ASUSExpressModel *expressModel;
+ (CGFloat)getASUSExpessCellHeight:(ASUSExpressModel *)model;
- (void)refreshFirstTimeLineFrame;
- (void)refreshLastTimeLineFrame;
@end

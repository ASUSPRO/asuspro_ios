//
//  ASUSExpressCell.m
//  ASUSPRO
//
//  Created by May on 15-6-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExpressCell.h"


@interface ASUSExpressCell ()

@property (nonatomic, strong) UILabel *expressAddress;
@property (nonatomic, strong) UILabel *expressTime;
@property (nonatomic, strong) UILabel *expressTimeLine;
@property (nonatomic, strong) UIView *expressTimeIcon;
@end

@implementation ASUSExpressCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.expressAddress = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, ScreenWidth - 50, 30)];
        self.expressAddress.textAlignment = NSTextAlignmentLeft;
        self.expressAddress.font = [UIFont systemFontOfSize:15.0];
        self.expressAddress.numberOfLines = 0;
        self.expressAddress.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
        [self.contentView addSubview:self.expressAddress];
        
        
        self.expressTime = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, ScreenWidth - 50, 15)];
        self.expressTime.textAlignment = NSTextAlignmentLeft;
        self.expressTime.numberOfLines = 0;
        self.expressTime.font = [UIFont systemFontOfSize:12.0];
        self.expressTime.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        [self.contentView addSubview:self.expressTime];
        
        
        self.expressTimeLine = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 1, 0)];
        self.expressTimeLine.textAlignment = NSTextAlignmentLeft;
        self.expressTimeLine.font = [UIFont systemFontOfSize:15.0];
        self.expressTimeLine.backgroundColor = [NCColorUtil colorFromHexString:@"#dfdfdf"];
        [self.contentView addSubview:self.expressTimeLine];
        
        self.expressTimeIcon = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 10, 10)];
        self.expressTimeIcon.backgroundColor = [NCColorUtil colorFromHexString:@"#ff7768"];
        self.expressTimeIcon.layer.cornerRadius = 5;
        self.expressTimeIcon.layer.borderColor = [NCColorUtil colorFromHexString:@"#ffc0b8"].CGColor;
        self.expressTimeIcon.layer.borderWidth = 2;
        [self.contentView addSubview:self.expressTimeIcon];
        
        self.bottomLineView.hidden = YES;
        
    }
    
    return self;
}

+ (CGFloat)getASUSExpessCellHeight:(ASUSExpressModel *)model {
    CGSize size = [model.expressCurrentAddress sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(ScreenWidth - 50, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    CGFloat expressAddressHeight = size.height;
    CGFloat cellHeight =  10 + expressAddressHeight + 12 + 15 + 10;
    if (cellHeight < kASUSExpressCellHeight) {
        return kASUSExpressCellHeight;
    }
    return  cellHeight;
}

- (CGFloat)getStringHeight:(NSString *)text font:(UIFont *)font width:(CGFloat )width{
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    return size.height;
}

- (void)refreshUI {
    CGFloat expressAddressHeight = [self getStringHeight:self.expressAddress.text  font:self.expressAddress.font width:self.expressAddress.frame.size.width];
    self.expressAddress.frame = CGRectMake(40, 30, ScreenWidth - 50, expressAddressHeight);
    self.expressTime.frame = CGRectMake(40, self.expressAddress.frame.size.height + self.expressAddress.frame.origin.y + 12, ScreenWidth - 50, 15);
    CGFloat cellHeight = self.expressAddress.frame.size.height + self.expressAddress.frame.origin.y + 12 + 15 + 10;
    if (cellHeight < kASUSExpressCellHeight) {
        cellHeight = kASUSExpressCellHeight;
    }
    self.expressTimeLine.frame = CGRectMake(20, 0, 1, cellHeight);
    self.expressTimeIcon.frame = CGRectMake(15, (self.expressAddress.frame.size.height + self.expressAddress.frame.origin.y + 12 + 15 + 10)/2 - 10, 10, 10);
    self.expressTimeIcon.center = CGPointMake(20, cellHeight/2 - 5);
    
}

- (void)refreshFirstTimeLineFrame {
    CGFloat cellHeight = self.expressAddress.frame.size.height + self.expressAddress.frame.origin.y + 12 + 15 + 10;
    if (cellHeight < kASUSExpressCellHeight) {
        cellHeight = kASUSExpressCellHeight;
    }
    self.expressTimeLine.frame = CGRectMake(20, cellHeight/2, 1, cellHeight/2);
    self.expressTimeIcon.frame = CGRectMake(13, cellHeight / 2 - 10, 14, 14);
    self.expressTimeIcon.layer.cornerRadius = 7;
}

- (void)refreshLastTimeLineFrame {
    CGFloat cellHeight = self.expressAddress.frame.size.height + self.expressAddress.frame.origin.y + 12 + 15 + 10;
    if (cellHeight < kASUSExpressCellHeight) {
        cellHeight = kASUSExpressCellHeight;
    }
    self.expressTimeLine.frame = CGRectMake(20, 0, 1, cellHeight/2);
}

- (void)setExpressModel:(ASUSExpressModel *)expressModel {
    _expressModel = expressModel;
    if ([expressModel.expressCurrentAddress isEqualToString:@""]) {
         self.expressAddress.text = [NSString stringWithFormat:@"%@",expressModel.expressRemark];
    }else{
         self.expressAddress.text = [NSString stringWithFormat:@"%@  %@",expressModel.expressCurrentAddress,expressModel.expressRemark];
    }
   
    self.expressTime.text = expressModel.expressTime;
    [self refreshUI];
}

@end

//
//  ASUSProductListCell.h
//  ASUSPRO
//
//  Created by May on 15-5-12.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSProductListCellView.h"
static const float  kASUSProductListCellHeight  = 213;
@interface ASUSProductListCell : ASUSBaseCell
- (void)setupViewsWithArray:(NSArray *)array delegate:(id<ASUSProductListCellViewDelegete>)delegate;
@end

//
//  ASUSShopCartCell.m
//  ASUSPRO
//
//  Created by May on 15-5-11.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//
//static const float  kLeftMargin  = 10;

static const float  kProductImageViewHeight  = 65;
static const float  kProductImageViewWidth  = 65;
static const float   kSelectedButtonSize = 15;
static const float   kEditSelectedViewWidth = 20;
static const float   kEditSelectedViewMargin = 10;

#import "ASUSShopCartCell.h"
#import "NCImageUtil.h"
#import "ASUSGiftModel.h"
#import "UIImageView+AFNetworking.h"
#import "ASUSGiftManager.h"
#import "BAAuthenticate.h"

typedef enum
{
    ASUSShopCartCellStatusTypeDone = 0,
    ASUSShopCartCellStatusTypeEdit
}ASUSShopCartCellStatusType;

@interface ASUSShopCartCell()
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *pointsLabel;
@property (nonatomic, strong) UIButton *addButton;
@property (nonatomic, strong) UITextField *numTextField;
@property (nonatomic, strong) UIButton *reduceButton;
@property (nonatomic, strong) UIButton *sureButton;
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) UIView *contentBackView;
@property (nonatomic, assign) ASUSShopCartCellStatusType cellStatusType;
@end

@implementation ASUSShopCartCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    
    self.contentBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, kASUSShopCartCellHeight)];
    [self.contentView addSubview:self.contentBackView];
    
    self.selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.selectedButton.frame = CGRectMake(kEditSelectedViewMargin, 20, kSelectedButtonSize, kSelectedButtonSize);
    self.selectedButton.center = CGPointMake(kEditSelectedViewMargin + kEditSelectedViewWidth/2, kASUSShopCartCellHeight/2);
    self.selectedButton.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.selectedButton.layer.cornerRadius = kSelectedButtonSize/2;
    self.selectedButton.layer.borderWidth = 1.0;
    [self.contentBackView addSubview:self.selectedButton];
    
    UIButton *selectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedBtn.frame = CGRectMake(0, 10, kSelectedButtonSize + 30, kSelectedButtonSize + 30);
    [selectedBtn addTarget:self action:@selector(selectedButtonButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentBackView addSubview:selectedBtn];
    
    self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2*kEditSelectedViewMargin + kSelectedButtonSize
                                                                        , 10, kProductImageViewWidth,kProductImageViewHeight)];
    [self.contentBackView addSubview:self.productImageView];
    self.productImageView.image = [UIImage imageNamed:@"1.jpg"];
    self.productImageView.backgroundColor = [UIColor redColor];
    
    float productNameLabelX = self.productImageView.frame.origin.x + self.productImageView.frame.size.width + 10;
    self.productNameLabel = [self createLabel:CGRectMake(productNameLabelX , 5, ScreenWidth - productNameLabelX - 80, 50) text:@"苹果（Apple）iphone 6 Plus(A1524) 64GB" font:[UIFont systemFontOfSize:14]];
    self.productNameLabel.numberOfLines = 2;
    self.productNameLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [self.contentBackView addSubview:self.productNameLabel];
    
    self.pointsLabel = [self createLabel:CGRectMake(ScreenWidth - 80, 10, 70, 20) text:@"2000分" font:[UIFont systemFontOfSize:14]];
    self.pointsLabel.textColor = RGBCOLOR(253.0, 120.0, 100);
    self.pointsLabel.textAlignment = NSTextAlignmentRight;
    [self.contentBackView addSubview:self.pointsLabel];

    self.reduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.reduceButton setFrame:CGRectMake(productNameLabelX + 5, kASUSShopCartCellHeight - 25 - 10, 25, 25)];
    [self.reduceButton addTarget:self action:@selector(changeProductNumButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.reduceButton.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.reduceButton.layer.borderWidth = 1.0;
    [self.reduceButton setImage:[NCImageUtil imageFromText:@"\ue612" fontSize:20 color:[NCColorUtil colorFromHexString:@"#606366"]] forState:UIControlStateNormal];
//    self.reduceButton.backgroundColor = [UIColor redColor];
    //    self.reduceButton.contentEdgeInsets = UIEdgeInsetsMake(20,-25, 0, 0);
    self.reduceButton.hidden = YES;
    [self.contentBackView addSubview:self.reduceButton];
    
    self.numTextField = [[UITextField alloc]initWithFrame:CGRectMake(self.reduceButton.frame.origin.x + 25 + 2, kASUSShopCartCellHeight - 25 - 10, 80, 25)];
    self.numTextField.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.numTextField.layer.borderWidth = 1.0;
    self.numTextField.borderStyle = UITextBorderStyleLine;
    self.numTextField.font =[UIFont systemFontOfSize:14];
    self.numTextField.textAlignment = NSTextAlignmentCenter;
    [self.numTextField addTarget:self action:@selector(numvalueChanged:) forControlEvents:UIControlEventEditingChanged];;
    self.numTextField.text =[NSString stringWithFormat:@"%ld",(long)self.giftModel.exchangeNum];
    self.numTextField.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    self.numTextField.background =[UIImage imageNamed:@"productNumTextbox"];
    self.numTextField.hidden = YES;
    [self.contentBackView addSubview:self.numTextField];
    
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addButton setFrame:CGRectMake(self.reduceButton.frame.origin.x + 25 + 2 + 2 + 80, kASUSShopCartCellHeight - 25 - 10, 25, 25)];
    [self.addButton setImage:[NCImageUtil imageFromText:@"\ue611" fontSize:20 color:[NCColorUtil colorFromHexString:@"#606366"]] forState:UIControlStateNormal];
    self.addButton.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.addButton.layer.borderWidth = 1.0;
    [self.addButton addTarget:self action:@selector(changeProductNumButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    self.addButton.contentEdgeInsets = UIEdgeInsetsMake(20, 20, 0, 0);
    self.addButton.hidden = YES;
    [self.contentBackView addSubview:self.addButton];
    
    self.totalLabel = [self createLabel:CGRectMake(ScreenWidth - 85, kASUSShopCartCellHeight - 35, 50, 25) text:@"x1" font:[UIFont systemFontOfSize:14]];
    self.totalLabel.text = [NSString stringWithFormat:@"x%ld",(long)self.giftModel.exchangeNum];
   self.totalLabel.textColor = [NCColorUtil colorForTypeOrSubContentFont];
    self.totalLabel.textAlignment = NSTextAlignmentRight;
    self.totalLabel.hidden = NO;
    [self.contentBackView addSubview:self.totalLabel];
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sureButton setFrame:CGRectMake(ScreenWidth - 35, kASUSShopCartCellHeight - 35, 25, 25)];
    [self.sureButton setImage:[NCImageUtil imageFromText:@"\ue610" fontSize:15 color:[NCColorUtil colorForTypeOrSubContentFont]] forState:UIControlStateNormal];
    [self.sureButton addTarget:self action:@selector(sureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    self.sureButton.contentEdgeInsets = UIEdgeInsetsMake(20,-25, 0, 0);
    [self.contentBackView addSubview:self.sureButton];

}

- (void)configSubViewsFrame:(int )type {
    if (type == 0) {
        [self.sureButton setImage:[NCImageUtil imageFromText:@"\ue610" fontSize:20 color:[NCColorUtil colorForSeparatorLine]] forState:UIControlStateNormal];
        self.selectedButton.frame = CGRectMake(10, 20, 20, 20);
        self.productImageView.frame = CGRectMake(self.selectedButton.frame.origin.x + 20 + 10
                                                 , 10, kProductImageViewWidth, kProductImageViewHeight);
        
        float productNameLabelX = self.productImageView.frame.origin.x + self.productImageView.frame.size.width + 10;
        self.productNameLabel.frame = CGRectMake(productNameLabelX , 5, ScreenWidth - productNameLabelX - 80, 50);
        self.pointsLabel.frame = CGRectMake(ScreenWidth - 80, 5, 70, 20);
        self.reduceButton.frame = CGRectMake(productNameLabelX + 5, kASUSShopCartCellHeight - 25 - 10, 25, 25);
        self.numTextField.frame = CGRectMake(self.reduceButton.frame.origin.x + 25 + 2, kASUSShopCartCellHeight - 25 - 10, 80, 25);
        self.addButton.frame = CGRectMake(self.reduceButton.frame.origin.x + 25 + 2 + 2 + 80, kASUSShopCartCellHeight - 25 - 10, 25, 25);
        self.totalLabel.frame = CGRectMake(ScreenWidth - 85, kASUSShopCartCellHeight - 35, 50, 25);
        self.sureButton.frame = CGRectMake(ScreenWidth - 35, kASUSShopCartCellHeight - 35, 25, 25);
    } else {
        [self.sureButton setImage:[NCImageUtil imageFromText:@"\ue613" fontSize:20 color:[NCColorUtil colorFromHexString:@"#ff415b"]] forState:UIControlStateNormal];
    }
}

- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text font:(UIFont *)font{
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = font;
    tempLabel.textColor = [UIColor blackColor];
    return tempLabel;
}

- (void)changeProductNumButtonClicked:(UIButton *)button {
    NSInteger number = [self.numTextField.text integerValue];
    if (self.reduceButton == button) {
        if (number > 1) {
            number --;
        }
    } else {
        number ++;
    }
    self.numTextField.text = [NSString stringWithFormat:@"%ld",(long)number];
    self.totalLabel.text = [NSString stringWithFormat:@"x%ld",(long)number];
    self.giftModel.exchangeNum = number;
     [ASUSGiftManager modifyGiftToShoppingCart:self.giftModel];
    [self.delegate giftModelNumChanged:self];
}

- (void)sureButtonClicked:(UIButton *)button {
    if (self.cellStatusType == ASUSShopCartCellStatusTypeDone) {
        self.reduceButton.hidden = NO;
        self.numTextField.hidden = NO;
        self.addButton.hidden = NO;
        self.totalLabel.hidden = YES;
        self.cellStatusType = ASUSShopCartCellStatusTypeEdit;
        [self.sureButton setImage:[NCImageUtil imageFromText:@"\ue613" fontSize:20 color:[UIColor orangeColor]] forState:UIControlStateNormal];
    } else {
        self.reduceButton.hidden = YES;
        self.numTextField.hidden = YES;
        self.addButton.hidden = YES;
        self.totalLabel.hidden = NO;
        self.cellStatusType = ASUSShopCartCellStatusTypeDone;
        [ASUSGiftManager modifyGiftToShoppingCart:self.giftModel];
        self.giftModel.exchangeNum = [self.numTextField.text integerValue];
        [self.sureButton setImage:[NCImageUtil imageFromText:@"\ue610" fontSize:15 color:[NCColorUtil colorForTypeOrSubContentFont]] forState:UIControlStateNormal];
    }
    
}

- (void)numvalueChanged:(UITextField *)textField {
    if ([textField.text integerValue] < 0) {
        textField.text = [NSString stringWithFormat:@"%ld",(long)self.giftModel.exchangeNum];
        return;
    }
    self.giftModel.exchangeNum = [textField.text integerValue];
     [ASUSGiftManager modifyGiftToShoppingCart:self.giftModel];
    [self.delegate giftModelNumChanged:self];
}

- (void)selectedButtonButtonClicked:(UIButton *)button {
    self.giftModel.isSelected = !self.giftModel.isSelected;
    self.isSelected = self.giftModel.isSelected;
    [self setIsSelected:self.giftModel.isSelected];
//    self.giftModel.isSelected = self.isSelected;
    [self.delegate shopCartCellSelectedStatusChanged:self];
}

#pragma mark - 外部

-(void)changeEditState:(BOOL)isEdit {
    self.isEditState = isEdit;
    [self setEditStateWithAnimated:YES];
}

//isSelected属性
-(void)setIsSelected:(BOOL)isSelected {
    _isSelected  = isSelected;
    if (_isSelected)
    {
        [self.selectedButton setImage:[NCImageUtil imageFromText:@"\ue613" fontSize:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        self.selectedButton.backgroundColor = RGBCOLOR(254, 119, 105);
        self.selectedButton.layer.cornerRadius = kSelectedButtonSize/2;
        self.selectedButton.layer.borderWidth = 0.0;
    } else {
        [self.selectedButton setImage:nil forState:UIControlStateNormal];
        self.selectedButton.backgroundColor = [UIColor whiteColor];
        self.selectedButton.layer.cornerRadius = kSelectedButtonSize/2;
        self.selectedButton.layer.borderWidth = 1.0;
    }
}

#pragma mark - 设置编辑状态
//isEditState属性
-(void)setEditStateWithAnimated:(BOOL)animated
{
    if (self.isEditState)
    {
        if (animated)
        {
//            [UIView animateWithDuration:0.25 animations:^{
//                [self enableEditState];
//            }];
        }
        else
        {
//            [self enableEditState];
        }
//        [self addGestureRecognizer:self.selectTap];
    }
    else
    {
        if (animated)
        {
            [UIView animateWithDuration:0.25 animations:^{
                [self disableEditState];
            }];
        }
        else
        {
            [self disableEditState];
        }
//        [self removeGestureRecognizer:self.selectTap];
    }
    
}

-(void)enableEditState {
//    editingSelectView.frame = CGRectMake(kEditSelectedViewMargin, (NSInteger)(self.frame.size.height - kEditSelectedViewWidth)/2, kEditSelectedViewWidth, kEditSelectedViewWidth);
    self.contentBackView.frame = CGRectMake(kEditSelectedViewWidth+2*kEditSelectedViewMargin, 0, self.frame.size.width - (kEditSelectedViewWidth+2*kEditSelectedViewMargin), self.frame.size.height);
}

-(void)disableEditState {
//    editingSelectView.frame = CGRectMake(-kEditSelectedViewWidth, (NSInteger)(self.frame.size.height - kEditSelectedViewWidth)/2, kEditSelectedViewWidth, kEditSelectedViewWidth);
    self.contentBackView.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.frame.size.height);
    
}

#pragma mark - setters

- (void)setGiftModel:(ASUSGiftModel *)giftModel {
    _giftModel = giftModel;
    self.productNameLabel.text = giftModel.giftName;
    self.totalLabel.text = [NSString stringWithFormat:@"x%ld",(long)self.giftModel.exchangeNum];
    self.numTextField.text =[NSString stringWithFormat:@"%ld",(long)self.giftModel.exchangeNum];
    if([[BAAuthenticate sharedClient].userType integerValue] == 1) {
        self.pointsLabel.text = [NSString stringWithFormat:@"%ld积分",(long)giftModel.innerScore];
    } else {
        self.pointsLabel.text = [NSString stringWithFormat:@"%ld积分",(long)giftModel.outerScore];
    }
        
    if (self.giftModel.giftPicUrl) {
        [self.productImageView setImageWithURL:[NSURL URLWithString:self.giftModel.giftPicUrl] placeholderImage:nil];
    }
    [self setIsSelected:giftModel.isSelected];
}

@end

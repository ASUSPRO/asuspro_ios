//
//  ASUSExchangeCell.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASUSReceiveAddressModel.h"

@interface ASUSExchangeCell : UITableViewCell


- (void)setAddressData:(ASUSReceiveAddressModel *)data;

@end

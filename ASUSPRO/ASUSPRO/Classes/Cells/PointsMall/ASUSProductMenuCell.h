//
//  ASUSProductMenuCell.h
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSProductMenuModel.h"
@interface ASUSProductMenuCell : ASUSBaseCell
@property (nonatomic, strong) UILabel *menuNameLabel;
@property (nonatomic, strong) ASUSProductMenuModel *menuModel;
@end

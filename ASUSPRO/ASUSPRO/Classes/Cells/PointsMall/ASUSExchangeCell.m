//
//  ASUSExchangeCell.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeCell.h"

@interface ASUSExchangeCell ()

@property (nonatomic, strong) UILabel *userLabel;

@property (nonatomic, strong) UILabel *emailLabel;

@property (nonatomic, strong) UILabel *addressLabel;

@end

@implementation ASUSExchangeCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, ScreenWidth-20, 117)];
        contentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1.0];
        [self addSubview:contentView];
        
        self.userLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-40, 30)];
        self.userLabel.textAlignment = NSTextAlignmentLeft;
        self.userLabel.font = [UIFont systemFontOfSize:16.0];
        self.userLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
        [contentView addSubview:self.userLabel];
        
        
        self.emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, ScreenWidth-40, 30)];
        self.emailLabel.textAlignment = NSTextAlignmentLeft;
        self.emailLabel.font = [UIFont systemFontOfSize:16.0];
        self.emailLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
        [contentView addSubview:self.emailLabel];
        
        
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, ScreenWidth-40, 50)];
        self.addressLabel.textAlignment = NSTextAlignmentLeft;
        self.addressLabel.numberOfLines = 0;
        self.addressLabel.font = [UIFont systemFontOfSize:15.0];
        self.addressLabel.textColor = [UIColor colorWithRed:132.0/255 green:137.0/255 blue:143.0/255 alpha:1.0];
        [contentView addSubview:self.addressLabel];

    }
    
    return self;
}
- (void)awakeFromNib {
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setAddressData:(ASUSReceiveAddressModel *)data
{
    if (data) {
        self.userLabel.text = [NSString stringWithFormat:@"%@        %@",data.userName,data.mobile];
        self.emailLabel.text = @"abc@126.com";
        self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",data.province,data.city,data.area,data.address];
    }
}

@end

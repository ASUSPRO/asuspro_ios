//
//  ASUSShopCartCell.h
//  ASUSPRO
//
//  Created by May on 15-5-11.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
@class ASUSGiftModel;
@class ASUSShopCartCell;
static const float  kASUSShopCartCellHeight  = 85;

@protocol ASUSShopCartCellDelegate <NSObject>
- (void)giftModelNumChanged:(ASUSShopCartCell *)shopCartCell;
- (void)shopCartCellSelectedStatusChanged:(ASUSShopCartCell *)shopCartCell;
@end
@interface ASUSShopCartCell : ASUSBaseCell
@property (nonatomic, assign) BOOL isEditState;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) id<ASUSShopCartCellDelegate> delegate ;
@property (nonatomic, strong) ASUSGiftModel *giftModel;
-(void)changeEditState:(BOOL)isEdit;
-(void)setIsSelected:(BOOL)isSelected;
@end

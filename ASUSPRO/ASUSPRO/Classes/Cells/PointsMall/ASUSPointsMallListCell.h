//
//  ASUSPointsMallListCell.h
//  ASUSPRO
//
//  Created by May on 15-5-18.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSShopModel.h"
@interface ASUSPointsMallListCell : ASUSBaseCell
@property (nonatomic, strong) ASUSShopModel *shopModel;
@end

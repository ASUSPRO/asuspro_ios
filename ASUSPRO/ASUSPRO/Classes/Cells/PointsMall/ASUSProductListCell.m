//
//  ASUSProductListCell.m
//  ASUSPRO
//
//  Created by May on 15-5-12.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//


#import "ASUSProductListCell.h"
#import "ASUSProductListCellView.h"

@interface ASUSProductListCell()

@end

@implementation ASUSProductListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        [self setupSubviews];
    }
    return self;
}

- (void)setupViewsWithArray:(NSArray *)array delegate:(id<ASUSProductListCellViewDelegete>)delegate {
    
    for (UIView * subView in self.contentView.subviews) {
        [subView removeFromSuperview];
    }
    for (int i = 0 ; i < array.count; i++) {
        ASUSProductListCellView *productListCellView = [[ASUSProductListCellView alloc] initWithFrame:CGRectMake(ScreenWidth/2.0 * i, 0, (ScreenWidth - 1)/2, kASUSProductListCellHeight)];
        productListCellView.backgroundColor = [UIColor clearColor];
        productListCellView.delegate = delegate;
        productListCellView.giftModel = [array objectAtIndex:i];
        [self.contentView addSubview:productListCellView];
    }
    self.bottomLineView.frame = CGRectMake(0, kASUSProductListCellHeight - 0.5, ScreenWidth, 0.5);
    self.bottomLineView.hidden = NO;
}

@end

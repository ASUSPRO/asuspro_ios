//
//  ASUSExchangeRecordCell.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeRecordCell.h"
#import "NCColorUtil.h"
#import "ASUSExchangeModel.h"
#import "UIImageView+AFNetworking.h"
#import "NCTimeUtil.h"

@interface ASUSExchangeRecordCell ()
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *productNumLabel;
@property (nonatomic, strong) UILabel *pointsLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@end

@implementation ASUSExchangeRecordCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    
    self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10
                                                                          , 10, 80, kASUSExchangeRecordCellHeight - 20)];
    self.productImageView.image = [UIImage imageNamed:@"1.jpg"];
    [self.contentView addSubview:self.productImageView];
    self.productImageView.backgroundColor = [UIColor redColor];
    
    float productNameLabelX = self.productImageView.frame.origin.x + self.productImageView.frame.size.width + 10;
    self.productNameLabel = [self createLabel:CGRectMake(productNameLabelX , 5, ScreenWidth - productNameLabelX - 80, 50) text:@"苹果（Apple）iphone 6 Plus(A1524) 64GB" font:[UIFont systemFontOfSize:14]];
    self.productNameLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    self.productNameLabel.numberOfLines = 2;
    [self.contentView addSubview:self.productNameLabel];
    
    self.pointsLabel = [self createLabel:CGRectMake(ScreenWidth - 80, 10, 70, 20) text:@"-2000分" font:[UIFont systemFontOfSize:14]];
    self.pointsLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    self.pointsLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.pointsLabel];
    
    self.productNumLabel = [self createLabel:CGRectMake(ScreenWidth - 80 , 40, 70, 20) text:@"X1" font:[UIFont systemFontOfSize:14]];
    self.productNumLabel.text = @"X1";
    self.productNumLabel.textAlignment = NSTextAlignmentRight;
    self.productNumLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [self.contentView addSubview:self.productNumLabel];
    
    self.dateLabel = [self createLabel:CGRectMake(productNameLabelX, kASUSExchangeRecordCellHeight - 30, 90, 20) text:@"2014-2-21" font:[UIFont systemFontOfSize:12]];
    self.dateLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    [self.contentView addSubview:self.dateLabel];
    
    UIButton *lookExpress = [UIButton buttonWithType:UIButtonTypeCustom];
    lookExpress.frame = CGRectMake(ScreenWidth - 70, kASUSExchangeRecordCellHeight - 40, 60, 40);
    [lookExpress setImage:[UIImage imageNamed:@"express"] forState:UIControlStateNormal];
    lookExpress.imageEdgeInsets = UIEdgeInsetsMake(10, 0, 5, 0);
    [lookExpress setTitleColor:[NCColorUtil colorFromHexString:@"#84898f"] forState:UIControlStateNormal];
    lookExpress.titleLabel.font = [UIFont systemFontOfSize:14];
    lookExpress.titleLabel.textAlignment = NSTextAlignmentRight;
    [lookExpress addTarget:self action:@selector(lookExpressButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:lookExpress];
    
    self.bottomLineView.frame = CGRectMake(0, kASUSExchangeRecordCellHeight - 0.5, ScreenWidth, 0.5);
}

- (void)setExchangeModel:(ASUSExchangeModel *)exchangeModel {
    _exchangeModel = exchangeModel;
    if (_exchangeModel) {
        [self.productImageView setImageWithURL:[NSURL URLWithString:self.exchangeModel.giftPic] placeholderImage:nil];
        self.productNameLabel.text = self.exchangeModel.giftName;
        self.pointsLabel.text = [NSString stringWithFormat:@"-%ld分",(long)self.exchangeModel.userScore];
        self.productNumLabel.text = [NSString stringWithFormat:@"X%ld",(long)self.exchangeModel.num];
        self.dateLabel.text = [NCTimeUtil convertTimeInterval:[NSString stringWithFormat:@"%lld",self.exchangeModel.createTime]];
    }
}

- (void)lookExpressButtonClicked {
    self.viewExpressBlock(self.exchangeModel);
}

- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text font:(UIFont *)font{
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = font;
    tempLabel.textColor = [UIColor blackColor];
    return tempLabel;
}


- (void)setUpData:(ASUSExchangeModel *)model
{
    if (model) {
        self.exchangeModel = model;
        [self.productImageView setImageWithURL:[NSURL URLWithString:self.exchangeModel.giftPic] placeholderImage:nil];
        self.productNameLabel.text = self.exchangeModel.giftName;
        self.pointsLabel.text = [NSString stringWithFormat:@"%ld积分",(long)self.exchangeModel.userScore];
        self.productNumLabel.text = [NSString stringWithFormat:@"X%ld",(long)self.exchangeModel.num];
        self.dateLabel.text = [NCTimeUtil convertTimeInterval:[NSString stringWithFormat:@"%lld",self.exchangeModel.createTime]];
    }
}
@end

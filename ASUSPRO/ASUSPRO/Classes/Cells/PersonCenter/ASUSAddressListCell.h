//
//  ASUSAddressListCell.h
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSReceiveAddressModel.h"

static const float  kASUSAddressListCellHeight  = 67;
@class ASUSAddressListCell;
@protocol ASUSAddressListCellDelegate<NSObject>
@optional
- (void)addressDelete:(ASUSAddressListCell *)cell;
- (void)detailButtonAction:(ASUSAddressListCell *)cell;
@end
@interface ASUSAddressListCell : ASUSBaseCell
@property (nonatomic,assign) BOOL isActivate;
@property (nonatomic,assign) id<ASUSAddressListCellDelegate> delegate;

-(void)setUpData:(ASUSReceiveAddressModel *)model;
@end

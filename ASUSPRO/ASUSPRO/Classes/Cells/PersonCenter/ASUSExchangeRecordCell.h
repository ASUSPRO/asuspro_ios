//
//  ASUSExchangeRecordCell.h
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
@class ASUSExchangeModel;
static const float  kASUSExchangeRecordCellHeight  = 100;
typedef void(^ViewExpressBlock)(ASUSExchangeModel *exchangeModel);
@interface ASUSExchangeRecordCell : ASUSBaseCell

@property (nonatomic, strong) ASUSExchangeModel *exchangeModel;

@property(nonatomic, copy)ViewExpressBlock viewExpressBlock;

- (void)setUpData:(ASUSExchangeModel *)model;

@end

//
//  ASUSAddressListCell.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSAddressListCell.h"

@interface ASUSAddressListCell ()
@property CGRect kDelBtnFrame;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIView *coverView;
//@property (nonatomic, strong) UIButton *deleteButton;
@end

@implementation ASUSAddressListCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    self.coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, kASUSAddressListCellHeight)];
    self.coverView.autoresizesSubviews = YES;
    self.coverView.backgroundColor = [UIColor whiteColor];
    self.coverView.clipsToBounds = YES;
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth - 40, 20)];
    self.nameLabel.textColor = [NCColorUtil colorForAddressList];
    self.nameLabel.font = [UIFont systemFontOfSize:15];
    [_coverView addSubview:self.nameLabel];
    
    self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, kASUSAddressListCellHeight - 25, ScreenWidth - 40, 20)];
    self.addressLabel.textColor = [NCColorUtil colorForAddressList];
    self.addressLabel.font = [UIFont systemFontOfSize:14];
    [_coverView addSubview:self.addressLabel];
    
    UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(ScreenWidth - 40, 0, 30, 30)];
    detailButton.center = CGPointMake(ScreenWidth - 40 + 30/2, kASUSAddressListCellHeight/2);
    [detailButton setImage:[NCImageUtil imageFromText:@"\ue61a" fontSize:20 color:RGBCOLOR(54.0, 152.0, 250.0)] forState:UIControlStateNormal];
    [detailButton addTarget:self action:@selector(detailButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [_coverView addSubview:detailButton];
    
//    self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(ScreenWidth - kASUSAddressListCellHeight, 0, kASUSAddressDeleteButtonWidth, kASUSAddressListCellHeight)];
//    self.deleteButton.backgroundColor = [UIColor redColor];
//    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
//    [self.deleteButton addTarget:self action:@selector(addressDeleteAction) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:self.deleteButton];
    
//    UISwipeGestureRecognizer *leftSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(displayDeleteButtonAction)];
//    leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//    [self.contentView addGestureRecognizer:leftSwipeGesture];
//    
//    UISwipeGestureRecognizer *rightSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideDeleteButtonAction)];
//    rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.contentView addGestureRecognizer:rightSwipeGesture];
    
//    self.kDelBtnFrame = self.deleteButton.frame;
    [self.contentView addSubview:self.coverView];
    
    self.bottomLineView.frame = CGRectMake(0, kASUSAddressListCellHeight - 0.5, ScreenWidth, 0.5);
    
}

#pragma mark - Public Method
-(void)setUpData:(ASUSReceiveAddressModel *)model
{
    if (model) {
        self.nameLabel.text = model.userName;
        self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.address];
    }
}

#pragma mark -
//#pragma mark ButtonActionsOrGestureActions
//
//- (void)displayDeleteButtonAction {
//    self.isActivate = YES;
//    self.deleteButton.hidden = NO;
//    [UIView animateWithDuration:0.3f animations:^{
//        CGRect deleteButtonFrame = self.deleteButton.frame;
//        deleteButtonFrame.size.width = 0;
//        self.kDelBtnFrame = deleteButtonFrame;
//        _coverView.frame = CGRectMake(- kASUSAddressDeleteButtonWidth, 0, ScreenWidth, kASUSAddressListCellHeight);
//        
//    }];
//}
//
//- (void)hideDeleteButtonAction {
//    if (self.kDelBtnFrame.size.width < kASUSAddressDeleteButtonWidth) {
//        [UIView animateWithDuration:0.3f animations:^{
//            CGRect deleteButtonFrame = self.kDelBtnFrame;
//            deleteButtonFrame.size.width = kASUSAddressDeleteButtonWidth;
//            _coverView.frame = CGRectMake(0, 0, ScreenWidth, kASUSAddressListCellHeight);
//        }];
//    }
//    self.isActivate = NO;
//}
//
//- (void)addressDeleteAction {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(addressDelete:)]) {
//        [self.delegate addressDelete:self];
//    }
//}

- (void)detailButtonAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailButtonAction:)]) {
        [self.delegate addressDelete:self];
    }
}
@end

//
//  ASUSBaseCell.h
//  ASUSPRO
//
//  Created by May on 15-5-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASUSBaseCell : UITableViewCell
@property (nonatomic,strong)UIView *bottomLineView;
- (void)refreshUI;
@end

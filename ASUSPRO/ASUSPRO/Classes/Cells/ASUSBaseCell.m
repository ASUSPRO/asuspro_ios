//
//  ASUSBaseCell.m
//  ASUSPRO
//
//  Created by May on 15-5-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"

@implementation ASUSBaseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0.5)];
        _bottomLineView.backgroundColor = [NCColorUtil colorForCellSeparator];
        [self addSubview:_bottomLineView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)refreshUI {
    
}

@end

//
//  ASUSHomeSearchCell.m
//  ASUSPRO
//
//  Created by May on 15-5-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSHomeSearchCell.h"
@interface ASUSHomeSearchCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIImageView *arrowImageView;
@end

@implementation ASUSHomeSearchCell

@end

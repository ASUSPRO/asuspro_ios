//
//  ASUSPolicyNoticeListCell.m
//  ASUSPRO
//
//  Created by May on 15-5-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPolicyNoticeListCell.h"
#import "NCColorUtil.h"
#import "NCTimeUtil.h"

static const float  kLeftMargin  = 10;
@interface ASUSPolicyNoticeListCell()
@property (nonatomic, strong) UILabel *noticeTitleLabel;
@property (nonatomic, strong) UILabel *noticePublisherLabel;
@property (nonatomic, strong) UILabel *noticeCreateTimeLabel;
@end

@implementation ASUSPolicyNoticeListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    self.noticeTitleLabel = [self createLabel:CGRectMake(kLeftMargin, 8, ScreenWidth - 2*kLeftMargin, 20) text:@"" font:[UIFont systemFontOfSize:16]];
    self.noticeTitleLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [self.contentView addSubview:self.noticeTitleLabel];
    
    self.noticePublisherLabel = [self createLabel:CGRectMake(kLeftMargin, 35, ScreenWidth - 2*kLeftMargin - 150, 20) text:@"" font:[UIFont systemFontOfSize:14]];
    self.noticePublisherLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [self.contentView addSubview:self.noticePublisherLabel];
    
    self.noticeCreateTimeLabel = [self createLabel:CGRectMake(ScreenWidth - kLeftMargin - 150, 35, 150, 20) text:@"" font:[UIFont systemFontOfSize:12]];
    self.noticeCreateTimeLabel.textAlignment = NSTextAlignmentRight;
    self.noticeCreateTimeLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [self.contentView addSubview:self.noticeCreateTimeLabel];
}

- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text font:(UIFont *)font{
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = font;
    tempLabel.textColor = [UIColor blackColor];
    return tempLabel;
}

- (void)refreshUIWithIndex:(NSInteger)row {
    self.bottomLineView.frame = CGRectMake(0, kASUSPolicyNoticeListCellHeight - 0.5, ScreenWidth, 0.5);
    if (row%2) {
        self.noticeTitleLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        self.noticePublisherLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        self.noticeCreateTimeLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    }else {
        self.noticeTitleLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        self.noticePublisherLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        self.noticeCreateTimeLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
    }
}

- (void)configSubviews {
    self.noticeTitleLabel.font = [UIFont systemFontOfSize:17];
    self.noticePublisherLabel.font = [UIFont systemFontOfSize:15];
}


- (void)setUpArticleData:(ASUSArticleModel *)model
{
    if (model) {
        self.noticeTitleLabel.text = model.mainTitle;
        self.noticePublisherLabel.text = model.source;
        self.noticeCreateTimeLabel.text = [NCTimeUtil convertTimeInterval:model.createTime];
    }
}

@end

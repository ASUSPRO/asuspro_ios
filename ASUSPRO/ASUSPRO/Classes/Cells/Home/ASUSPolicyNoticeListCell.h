//
//  ASUSPolicyNoticeListCell.h
//  ASUSPRO
//
//  Created by May on 15-5-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseCell.h"
#import "ASUSArticleModel.h"

static const float  kASUSPolicyNoticeListCellHeight  = 65;

@interface ASUSPolicyNoticeListCell : ASUSBaseCell
- (void)configSubviews;
- (void)refreshUIWithIndex:(NSInteger)row;
- (void)setUpArticleData:(ASUSArticleModel *)model;

@end

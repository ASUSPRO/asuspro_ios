//
//  ASUSPriceArticleTableViewCell.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPriceArticleTableViewCell.h"

@interface ASUSPriceArticleTableViewCell ()
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@end

@implementation ASUSPriceArticleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    self.typeLabel = [self createLabel:CGRectMake(32, 9, 160, 30) text:@""];
    self.numLabel = [self createLabel:CGRectMake(160, 9, 90, 30) text:@""];
    self.numLabel.textAlignment = NSTextAlignmentCenter;
    self.priceLabel = [self createLabel:CGRectMake(250, 9, ScreenWidth - 260, 30) text:@""];
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)refreshUIWithIndex:(NSInteger)row {
    self.bottomLineView.frame = CGRectMake(0, 48 - 0.5, ScreenWidth, 0.5);
    if (row%2) {
        self.typeLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        self.numLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        self.priceLabel.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    }else {
        self.typeLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        self.numLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        self.priceLabel.textColor = [NCColorUtil colorFromHexString:@"#ffffff"];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text {
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = [UIFont systemFontOfSize:13];
    tempLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [self.contentView addSubview:tempLabel];
    return tempLabel;
}

- (void)setUpData:(ASUSProductListModel *)model
{
    if (model) {
        self.typeLabel.text = model.type;
        self.numLabel.text = model.num;
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@",model.price];
    }
}
@end

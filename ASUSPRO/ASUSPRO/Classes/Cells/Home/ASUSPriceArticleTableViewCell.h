//
//  ASUSPriceArticleTableViewCell.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASUSProductListModel.h"
#import "ASUSBaseCell.h"

@interface ASUSPriceArticleTableViewCell : ASUSBaseCell

- (void)setUpData:(ASUSProductListModel *)model;
- (void)refreshUIWithIndex:(NSInteger)row;

@end

//
//  ASUSPolicyNoticeDetailViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSArticleViewController.h"

@interface ASUSArticleDetailViewController : ASUSBaseViewController

- (id)initWithArticleId:(NSInteger)articleId articleType:(ARTICLETYPE)type;
@end

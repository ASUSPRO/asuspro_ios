//
//  ASUSPolicyNoticeDetailViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSArticleDetailViewController.h"
#import "ASUSPolicyNoticeListCell.h"
#import "NCImageUtil.h"
#import "ASUSArticleDetialModel.h"
#import "NCTimeUtil.h"

@interface ASUSArticleDetailViewController ()<UIWebViewDelegate>
{
    NSInteger articleID;
    ARTICLETYPE articleType;
    BOOL isLoadingFinished;
}
@property (nonatomic, strong) UIWebView *articleWebView;
@property (nonatomic, strong) ASUSArticleDetialModel *articleDetailModel;
@property (nonatomic, strong) UILabel *articleTitleLabel;
@property (nonatomic, strong) UILabel *articleSourceLabel;
@property (nonatomic, strong) UILabel *createTimeLabel;

@end

@implementation ASUSArticleDetailViewController

#pragma mark - lifeCycle 

- (id)initWithArticleId:(NSInteger)articleId articleType:(ARTICLETYPE)type
{
    if (self = [super init]) {
        articleID = articleId;
        articleType = type;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigationBar];
    
    [self setUpTopView];
    
    [self setupWebView];
   
    [self loadData];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    if (articleType == POLICYNOTICE) {
        titleLabel.text = @"政策通知";
    }else {
        titleLabel.text = @"技术分享";
    }
    self.navigationItem.titleView = titleLabel;
}

- (void)setUpTopView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 65)];
    [self.view addSubview:topView];
    
    self.articleTitleLabel =[[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth - 20, 30)];
    self.articleTitleLabel.font = [UIFont systemFontOfSize:17];
    self.articleTitleLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [topView addSubview:self.articleTitleLabel];
    
    self.articleSourceLabel =[[UILabel alloc] initWithFrame:CGRectMake(10, 40, ScreenWidth - 160, 20)];
    self.articleSourceLabel.font = [UIFont systemFontOfSize:15];
    self.articleSourceLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.articleSourceLabel];
    
    self.createTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth - 160, 40, 150, 20)];
    self.createTimeLabel.font  = [UIFont systemFontOfSize:15];
    self.createTimeLabel.textAlignment = NSTextAlignmentRight;
    self.createTimeLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.createTimeLabel];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 65-0.5,ScreenWidth, 0.5)];
    bottomLineView.backgroundColor = [NCColorUtil colorForCellSeparator];
    [topView addSubview:bottomLineView];

}

- (void)setupWebView {
    isLoadingFinished = NO;
    self.articleWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 65, ScreenWidth, ScreenHeight - 65)];
    [self.articleWebView setScalesPageToFit:NO];
    self.articleWebView.hidden = YES;
    self.articleWebView.delegate = self;
    [self.view addSubview:self.articleWebView];
}

#pragma mark -RequestData
- (void)loadData
{
    if (articleType == POLICYNOTICE) {
         [self showHud:HBHudShowTypeActivityOnly withText:nil];
        [ASUSArticleDetialModel fetchArticleContentByArticleID:108 contentID:articleID completion:^(NSError *error, ASUSArticleDetialModel *model) {
            [self hideHud];
            if (model) {
                self.articleDetailModel = model;
                self.articleTitleLabel.text = model.mainTitle;
                self.articleSourceLabel.text = model.source;
                self.createTimeLabel.text = [NCTimeUtil convertTimeInterval:model.createTime];
                [self.articleWebView loadHTMLString:model.content baseURL:nil];
            }
        }];

    }else {
         [self showHud:HBHudShowTypeActivityOnly withText:nil];
        [ASUSArticleDetialModel fetchArticleContentByArticleID:109 contentID:articleID completion:^(NSError *error, ASUSArticleDetialModel *model) {
            [self hideHud];
            if (model) {
                self.articleDetailModel = model;
                self.articleTitleLabel.text = model.mainTitle;
                self.articleSourceLabel.text = model.source;
                self.createTimeLabel.text = [NCTimeUtil convertTimeInterval:model.createTime];
                [self.articleWebView loadHTMLString:model.content baseURL:nil];
            }
        }];
 
    }
    
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //若已经加载完成，则显示webView并return
    if(isLoadingFinished)
    {
        [self.articleWebView setHidden:NO];
        return;
    }
           //js获取body宽度
    NSString *bodyWidth= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollWidth"];
    
    int widthOfBody = [bodyWidth intValue];
    
    //获取实际要显示的html
    NSString *html = [self htmlAdjustWithPageWidth:widthOfBody
                                              html:self.articleDetailModel.content
                                           webView:webView];
    
    //设置为已经加载完成
    isLoadingFinished = YES;
    //加载实际要现实的html
    [self.articleWebView loadHTMLString:html baseURL:nil];
    [self.articleWebView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'gray'"];
}

//获取宽度已经适配于webView的html。这里的原始html也可以通过js从webView里获取
- (NSString *)htmlAdjustWithPageWidth:(CGFloat )pageWidth
                                 html:(NSString *)html
                              webView:(UIWebView *)webView
{
    NSMutableString *str = [NSMutableString stringWithString:html];
    //计算要缩放的比例
    CGFloat initialScale = webView.frame.size.width/pageWidth;
    //将</head>替换为meta+head
    NSString *stringForReplace = [NSString stringWithFormat:@"<meta name=\"viewport\" content=\" initial-scale=%f, minimum-scale=0.1, maximum-scale=2.0, user-scalable=yes\"></head>",initialScale];
    
    NSRange range =  NSMakeRange(0, str.length);
    //替换
    [str replaceOccurrencesOfString:@"</head>" withString:stringForReplace options:NSLiteralSearch range:range];
    return str;
}




@end

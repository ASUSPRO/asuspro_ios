//
//  ASUSSearchViewController.m
//  ASUSPRO
//
//  Created by May on 15-6-23.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSSearchViewController.h"
#import "ASUSPolicyNoticeListCell.h"
#import "ASUSArticleDetailViewController.h"
#import "ASUSArticleModel.h"

@interface ASUSSearchViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSString *searchWord;
@property (nonatomic, strong) UITableView *searchTableView;
@property (nonatomic, strong) NSArray *searchArr;

@end

@implementation ASUSSearchViewController

#pragma mark - lifecycle
- (id)initWithKeyword:(NSString *)keyword
{
    if (self = [super init]) {
        self.searchWord = keyword;
        [self loadData];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configNavigationBar];
}

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"搜索列表";
    
    self.navigationItem.titleView = titleLabel;
}

- (void)setupTableView {
    self.searchTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    self.searchTableView.delegate = self;
    self.searchTableView.dataSource = self;
    self.searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.searchTableView];
}

- (void)loadData
{
    [ASUSArticleModel fetchSearchByKeyword:self.searchWord completion:^(NSError *error, NSArray *list) {
        if (list) {
            self.searchArr = [NSArray arrayWithArray:list];
            [self.searchTableView reloadData];
        }
    }];
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchArr count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSPolicyNoticeListCell";
    ASUSPolicyNoticeListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSPolicyNoticeListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        if (indexPath.row%2) {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        }else {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        }
    }
    
    [cell refreshUIWithIndex:indexPath.row];
    ASUSArticleModel *model = (ASUSArticleModel *)self.searchArr[indexPath.row];
    [cell setUpArticleData:model];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ASUSArticleModel *model = (ASUSArticleModel *)self.searchArr[indexPath.row];
    if ([model.columnID integerValue]== 108) {
        ASUSArticleDetailViewController *detailVC = [[ASUSArticleDetailViewController alloc] initWithArticleId:[model.articleId integerValue]articleType:POLICYNOTICE];
        [self.navigationController pushViewController:detailVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }else if([model.columnID integerValue] == 109){
        ASUSArticleDetailViewController *detailVC = [[ASUSArticleDetailViewController alloc] initWithArticleId:[model.articleId integerValue] articleType:TECHNIQUESHARE];
        [self.navigationController pushViewController:detailVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
    
    
}

@end

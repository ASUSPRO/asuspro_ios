//
//  ASUSPolicyNoticeViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSArticleViewController.h"
#import "ASUSPolicyNoticeListCell.h"
#import "ASUSPolicyNoticeMenuView.h"
#import "ASUSArticleDetailViewController.h"
#import "NCColorUtil.h"
#import "NCImageUtil.h"

#import "ASUSProductModel.h"
#import "ASUSArticleModel.h"

@interface ASUSArticleViewController ()<UITableViewDelegate, UITableViewDataSource,ASUSPolicyNoticeMenuItemViewDelegate>
{
    ARTICLETYPE  articleType;
}
@property (nonatomic, strong) UITableView *articleTableView;
@property (nonatomic, strong) ASUSPolicyNoticeMenuView *articleMenuView;
@property (nonatomic, strong) NSMutableArray *articleMenuItems;
@property (nonatomic, strong) NSArray *articleArr;

@end

@implementation ASUSArticleViewController

#pragma mark - lifecycle method
- (id) initWithType:(ARTICLETYPE)type
{
    if (self = [super init]) {
        articleType = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigationBar];
    [self setupTableView];
    [self loadCategoryData];
    [self loadArticleData];
}

#pragma mark -RequestData
- (void)loadCategoryData
{
    if (articleType == POLICYNOTICE) {
        [ASUSProductModel fetchArticleCategoryByCategoryID:108 completion:^(NSError *error, NSArray *list) {
          if (list)
          {
              self.articleMenuItems = [NSMutableArray arrayWithArray:list];
              [self setupPolicyNoticeMenu];

          }
        }];
    }else if(articleType == TECHNIQUESHARE){
        [ASUSProductModel fetchArticleCategoryByCategoryID:109 completion:^(NSError *error, NSArray *list) {
            if (list) {
                self.articleMenuItems = [NSMutableArray arrayWithArray:list];
                [self setupPolicyNoticeMenu];

                }
        }];
    }else {
        
    }
}

- (void)loadArticleData
{
    if (articleType == POLICYNOTICE) {
        [self showHud:HBHudShowTypeActivityOnly withText:nil];
        [ASUSArticleModel fetchArticleListByArticleID:108 page:0 completion:^(NSError *error, NSArray *list) {
            [self hideHud];
            if (list) {
                self.articleArr = [NSArray arrayWithArray:list];
                [self.articleTableView reloadData];
            }
        }];
    }else if(articleType == TECHNIQUESHARE){
        [self showHud:HBHudShowTypeActivityOnly withText:nil];
        [ASUSArticleModel fetchArticleListByArticleID:109 page:0 completion:^(NSError *error, NSArray *list) {
            [self hideHud];
            if (list) {
                self.articleArr = [NSArray arrayWithArray:list];
                [self.articleTableView reloadData];
            }
        }];

    }else {
        
    }
    
}

#pragma mark - views

- (void)configNavigationBar {

    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    if (articleType == POLICYNOTICE) {
        titleLabel.text = @"政策通知";
    }else if(articleType == TECHNIQUESHARE){
        titleLabel.text = @"技术分享";
    }
    self.navigationItem.titleView = titleLabel;
    
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
}


- (void)setupPolicyNoticeMenu {
    self.articleMenuView = [[ASUSPolicyNoticeMenuView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 39)];
    self.articleMenuView.delegate = self;
    self.articleMenuView.columeItemArray = self.articleMenuItems;
    [self.view addSubview:self.articleMenuView];
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenWidth, 0.5)];
    horizontalLine.backgroundColor = [NCColorUtil colorForCellSeparator];
    [self.view addSubview:horizontalLine];
    
}

- (void)setupTableView {
    if (articleType == HOMESEARCH) {
        self.articleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    }else {
        self.articleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
    }
    
    self.articleTableView.delegate = self;
    self.articleTableView.scrollEnabled = YES;
    self.articleTableView.dataSource = self;
    self.articleTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.articleTableView];
}

#pragma mark - Event response

- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.articleArr count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kASUSPolicyNoticeListCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSPolicyNoticeListCell";
    ASUSPolicyNoticeListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSPolicyNoticeListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        if (indexPath.row%2) {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        }else {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        }
    }
    
    [cell refreshUIWithIndex:indexPath.row];
    ASUSArticleModel *model = (ASUSArticleModel *)self.articleArr[indexPath.row];
    [cell setUpArticleData:model];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ASUSArticleModel *model = (ASUSArticleModel *)self.articleArr[indexPath.row];
    if (articleType == POLICYNOTICE) {
        ASUSArticleDetailViewController *detailVC = [[ASUSArticleDetailViewController alloc] initWithArticleId:[model.articleId integerValue]articleType:POLICYNOTICE];
        [self.navigationController pushViewController:detailVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }else if(articleType == TECHNIQUESHARE){
        ASUSArticleDetailViewController *detailVC = [[ASUSArticleDetailViewController alloc] initWithArticleId:[model.articleId integerValue] articleType:TECHNIQUESHARE];
        [self.navigationController pushViewController:detailVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
   
    
}

#pragma mark - ASUSPolicyNoticeMenuItemViewDelegate

- (void)clickItem:(NSInteger)index sender:(id)sender {
    [self showHud:HBHudShowTypeActivityOnly withText:nil];
    [ASUSArticleModel fetchArticleListByArticleID:index page:0 completion:^(NSError *error, NSArray *list) {
        [self hideHud];
        self.articleArr = list;
        [self.articleTableView reloadData];
    }];
    
}

- (void)showMenuView {
    self.articleTableView.userInteractionEnabled = NO;
}

- (void)dismissMenuView {
    self.articleTableView.userInteractionEnabled = YES;
}
@end

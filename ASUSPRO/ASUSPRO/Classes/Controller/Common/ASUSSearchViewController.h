//
//  ASUSSearchViewController.h
//  ASUSPRO
//
//  Created by May on 15-6-23.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"

@interface ASUSSearchViewController : ASUSBaseViewController

- (id)initWithKeyword:(NSString *)keyword;

@end

//
//  ASUSPolicyNoticeViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-7.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"

typedef enum {
    POLICYNOTICE,
    TECHNIQUESHARE,
    HOMESEARCH
}ARTICLETYPE;

@interface ASUSArticleViewController : ASUSBaseViewController

- (id) initWithType:(ARTICLETYPE)type;

@end

//
//  ASUSPriceArticleViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPriceArticleViewController.h"
#import "ASUSPolicyNoticeMenuView.h"
#import "ASUSProductListModel.h"
#import "ASUSPriceArticleTableViewCell.h"

@interface ASUSPriceArticleViewController ()<UITableViewDelegate, UITableViewDataSource,ASUSPolicyNoticeMenuItemViewDelegate>

@property (nonatomic, strong) UITableView *articleTableView;
@property (nonatomic, strong) ASUSPolicyNoticeMenuView *articleMenuView;
@property (nonatomic, strong) NSMutableArray *articleMenuItems;
@property (nonatomic, strong) NSArray *articleArr;

@end

@implementation ASUSPriceArticleViewController


#pragma mark - lifecycle method

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigationBar];
    [self setupTableView];
    [self loadCategoryData];
    [self loadArticleData];
}

#pragma mark -RequestData
- (void)loadCategoryData
{
    [ASUSProductModel fetchProductCategoryWithCompletion:^(NSError *error, NSArray *list) {
        if (list) {
            self.articleMenuItems = [NSMutableArray arrayWithArray:list];
            [self setupPolicyNoticeMenu];
        }
        
    }];
}

- (void)loadArticleData
{
    [self showHud:HBHudShowTypeActivityOnly withText:nil];
    [ASUSProductListModel fetchProductListByCategoryID:0 page:0 completion:^(NSError *error, NSArray *list) {
        [self hideHud];
        if (list) {
            self.articleArr = [NSArray arrayWithArray:list];
            [self.articleTableView reloadData];
        }
    }];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"配件价格";
    
    self.navigationItem.titleView = titleLabel;
}


- (void)setupPolicyNoticeMenu {
    self.articleMenuView = [[ASUSPolicyNoticeMenuView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 39)];
    self.articleMenuView.delegate = self;
    self.articleMenuView.columeItemArray = self.articleMenuItems;
    [self.view addSubview:self.articleMenuView];
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenWidth, 0.5)];
    horizontalLine.backgroundColor = [NCColorUtil colorForCellSeparator];
    [self.view addSubview:horizontalLine];
    
}

- (void)setupTableView {
    self.articleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
    self.articleTableView.delegate = self;
    self.articleTableView.scrollEnabled = YES;
    self.articleTableView.dataSource = self;
    self.articleTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.articleTableView];
}

- (UILabel *)createHeadViewLabel:(CGRect) frame text:(NSString *)text {
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = [UIFont systemFontOfSize:15];
    tempLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    return tempLabel;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.articleArr count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    UILabel *typeLabel = [self createHeadViewLabel:CGRectMake(32, 4, 160, 30) text:@"型号"];
    [view addSubview:typeLabel];
    UILabel *numLabel = [self createHeadViewLabel:CGRectMake(160, 4, 90, 30) text:@"数量"];
    [view addSubview:numLabel];
    numLabel.textAlignment = NSTextAlignmentCenter;
    UILabel *priceLabel = [self createHeadViewLabel:CGRectMake(250, 4, ScreenWidth - 282, 30) text:@"单价"];
    [view addSubview:priceLabel];
    priceLabel.textAlignment = NSTextAlignmentCenter;
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 38-0.5, ScreenWidth, 0.5)];
    bottomLineView.backgroundColor = [NCColorUtil colorForCellSeparator];
    [view addSubview:bottomLineView];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSPolicyNoticeListCell";
    ASUSPriceArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSPriceArticleTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        if (indexPath.row%2) {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#ffffff"];
        }else {
            cell.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell refreshUIWithIndex:indexPath.row];
    ASUSProductListModel *model = (ASUSProductListModel *)self.articleArr[indexPath.row];
    [cell setUpData:model];
    return cell;
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - ASUSPolicyNoticeMenuItemViewDelegate

- (void)clickItem:(NSInteger)index sender:(id)sender {
    [self showHud:HBHudShowTypeActivityOnly withText:nil];
    [ASUSProductListModel fetchProductListByCategoryID:index page:0 completion:^(NSError *error, NSArray *list) {
        [self hideHud];
        self.articleArr = [NSArray arrayWithArray:list];
        [self.articleTableView reloadData];
    }];
    
}

- (void)showMenuView {
    self.articleTableView.userInteractionEnabled = NO;
}

- (void)dismissMenuView {
    self.articleTableView.userInteractionEnabled = YES;
}

@end

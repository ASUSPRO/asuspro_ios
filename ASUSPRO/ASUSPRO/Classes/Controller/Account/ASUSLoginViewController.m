//
//  ASUSLoginViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-4.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSLoginViewController.h"
#import "NCImageUtil.h"
#import "HomeViewController.h"
#import "HuoBanKit.h"
#import "ASUSUserModel.h"
#import "BAAuthenticate.h"
#import "ASUSInputEmailViewController.h"

@interface ASUSLoginViewController ()<UITextFieldDelegate>
{
    UIButton *loginButton;
    UIButton *rememberButton ;
}
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, assign) BOOL isRememberAccount;
@end

@implementation ASUSLoginViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisAppearance) name:UIKeyboardWillHideNotification object:nil];
    [self setBackgroudView];
    
    [self setupViews];
}

- (void)setBackgroudView
{
    self.backgroundView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.backgroundView setImage:[UIImage imageNamed:@"login_bg"]];
    [self.view addSubview:self.backgroundView];
}
- (void)setupViews {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [self.view addGestureRecognizer:tap];
    
    NSMutableAttributedString *welcomStr = [[NSMutableAttributedString alloc] initWithString:@"Welcome to ASUSPRO"];
    UILabel *welcomeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 79, ScreenWidth, 40)];
    welcomeLabel.textColor = [UIColor whiteColor];
    welcomeLabel.textAlignment = NSTextAlignmentCenter;
    welcomeLabel.attributedText = welcomStr;
    [self.view addSubview:welcomeLabel];
    
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake((ScreenWidth - 56)/2, welcomeLabel.frame.size.height + welcomeLabel.frame.origin.y + 20, 56, 2)];
    horizontalLine.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:horizontalLine];
    
    float emailBgOriginY = ScreenHeight - 300;
    
    UIImageView *emailBg = [[UIImageView alloc] initWithFrame:CGRectMake(17, emailBgOriginY, ScreenWidth - 34, 40)];
    [self.view addSubview:emailBg];
    emailBg.userInteractionEnabled = YES;
    emailBg.layer.cornerRadius = 2;
    emailBg.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    UIImageView *emialIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 20, 20)];
    emialIcon.image = [NCImageUtil imageFromText:@"\ue60b" fontSize:20 color:[UIColor whiteColor]];
    [emailBg addSubview:emialIcon];
    self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, emailBg.frame.size.width - 50, 40)];
    self.emailTextField.borderStyle = UITextBorderStyleNone;
    self.emailTextField.delegate = self;
    self.emailTextField.placeholder = @"E-mail address";
    [emailBg addSubview:self.emailTextField];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"accountName"]) {
        self.emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"accountName"];
    }
    
    UIImageView *passwordBg = [[UIImageView alloc] initWithFrame:CGRectMake(17, emailBg.frame.origin.y + 40 + 10, ScreenWidth - 34, 40)];
    passwordBg.userInteractionEnabled = YES;
    passwordBg.layer.cornerRadius = 2;
    passwordBg.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [self.view addSubview:passwordBg];
    UIImageView *passwordIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 20, 20)];
    passwordIcon.image = [NCImageUtil imageFromText:@"\ue609" fontSize:20 color:[UIColor whiteColor]];
    [passwordBg addSubview:passwordIcon];
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, passwordBg.frame.size.width - 50, 40)];
    self.passwordTextField.borderStyle = UITextBorderStyleNone;
    self.passwordTextField.delegate = self;
    self.passwordTextField.secureTextEntry = YES;
    [passwordBg addSubview:self.passwordTextField];
    
    loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.frame = CGRectMake(17, passwordBg.frame.origin.y + 60, ScreenWidth - 34, 40);
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(loginButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorFromHexString:@"#ff415b"]] forState:UIControlStateNormal];
    [loginButton setBackgroundImage:[NCImageUtil createImageWithColor:[[NCColorUtil colorFromHexString:@"#ff415b"] colorWithAlphaComponent:0.8]] forState:UIControlStateHighlighted];
    loginButton.layer.cornerRadius = 3;
    [self.view addSubview:loginButton];
    
    rememberButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rememberButton.frame = CGRectMake(17, passwordBg.frame.origin.y + 129, 20, 20);
    [rememberButton addTarget:self action:@selector(rememberAccountButton:) forControlEvents:UIControlEventTouchUpInside];
     [rememberButton setBackgroundImage:[NCImageUtil imageFromText:@"\ue620" fontSize:20 color:[NCColorUtil backGroundColorForView]] forState:UIControlStateNormal];
    [self.view addSubview:rememberButton];
    
    UILabel *accountLabel = [[UILabel alloc] initWithFrame: CGRectMake(45, passwordBg.frame.origin.y + 120, ScreenWidth - 34, 40)];
    accountLabel.text = @"记住账户";
    accountLabel.textColor = [NCColorUtil backGroundColorForView];
    accountLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:accountLabel];
    
    UIButton *forgetPasswordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    forgetPasswordButton.frame = CGRectMake(20, ScreenHeight - 60, 100, 40);
    [forgetPasswordButton setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [forgetPasswordButton addTarget:self action:@selector(forgetPasswordButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [forgetPasswordButton setTitleColor:[NCColorUtil colorFromHexString:@"#5c6061"] forState:UIControlStateNormal];
    [forgetPasswordButton setTitleColor:[[NCColorUtil colorFromHexString:@"#5c6061"] colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
    forgetPasswordButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    forgetPasswordButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:forgetPasswordButton];
}

#pragma mark - Private Method

-(BOOL)checkParametersIsValid
{
    if (self.emailTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写用户名"];
        [self hideHudAfter:1];
        return NO;
    }
    if (self.passwordTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写密码"];
        [self hideHudAfter:1];
        return NO;
    }
    
    return YES;
}

#pragma mark - UIButton Actions 

- (void)loginButtonAction:(UIButton *)loginButton {
    if ([self checkParametersIsValid]) {
        
        [self showHud:HBHudShowTypeActivityOnly withText:nil];
        
        [HuoBanKit authenticateAsUserWithUserName:self.emailTextField.text password:self.passwordTextField.text completion:^(HBResponse *response, NSError *error) {
            [self hideHud];
            if (error == nil) {
//                [[BAClient sharedClient] authenticateCookieWithCompletion:nil];
              
                HomeViewController *hvc = [[HomeViewController alloc] init];
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hvc];
                [nav.navigationBar setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorFromHexString:@"#363A3F"] ]forBarMetrics:UIBarMetricsDefault];
                [self presentViewController:nav animated:YES completion:nil];
                
                if (self.isRememberAccount) {
                    [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"accountName"];
                } else {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"accountName"];
                }
            } else {
                NSString * meesage = error.userInfo[@"message"];
                if (meesage == nil) {
                    meesage = @"暂无网络，请检查您的网络……";
                }
                [self showHud:HBHudShowTypeCaptionOnly withText:meesage];
                [self hideHudAfter:3];
            }
            
        }];
    }
}

- (void)rememberAccountButton:(UIButton *)button {
    button.selected = !button.selected;
    self.isRememberAccount = button.selected;
    if (self.isRememberAccount) {
        [rememberButton setBackgroundImage:[NCImageUtil imageFromText:@"\ue621" fontSize:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"accountName"];
    } else {
         [rememberButton setBackgroundImage:[NCImageUtil imageFromText:@"\ue620" fontSize:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"accountName"];
    }
    
}

- (void)forgetPasswordButtonAction:(UIButton *)forgetPasswordButton {
    ASUSInputEmailViewController *forgetPassword = [[ASUSInputEmailViewController alloc] init];
    UINavigationController *forgetPasswordNC = [[UINavigationController alloc] initWithRootViewController:forgetPassword];
    [forgetPasswordNC.navigationBar setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorFromHexString:@"#363A3F"] ]forBarMetrics:UIBarMetricsDefault];
    [self presentViewController:forgetPasswordNC animated:YES completion:nil];
}

- (void)hideKeyBoard
{
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

#pragma mark - keyboard Delegate

- (void)keyboardWillDisAppearance {
   self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [self hideKeyBoard];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        [self.passwordTextField becomeFirstResponder];
    }else {
        [self loginButtonAction:loginButton];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.emailTextField) {
        self.view.frame = CGRectMake(0, - 70, ScreenWidth, ScreenHeight);
    } else if (textField == self.passwordTextField) {
        self.view.frame = CGRectMake(0, - 70, ScreenWidth, ScreenHeight);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.emailTextField) {
        self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    } else if (textField == self.passwordTextField) {
        self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    }
}
@end

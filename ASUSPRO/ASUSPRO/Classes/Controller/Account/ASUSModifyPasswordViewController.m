//
//  ASUSModifyPasswordViewController.m
//  ASUSPRO
//
//  Created by May on 15-6-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSModifyPasswordViewController.h"
#import "NCDeviceUtil.h"
#import "ASUSUserModel.h"

@interface ASUSModifyPasswordViewController ()<UITextFieldDelegate>
{
    NSInteger secondsCountDown;
    NSTimer *countDownTimer;
}
@property (nonatomic, strong) UIView *nextStepView;
@property (nonatomic, strong) UITextField *oldPasswordTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *rePasswordTextField;

@property (nonatomic, strong) UIControl *hideKeyBoardControl;
@property (nonatomic, strong) UIButton *sendButton;

@end

@implementation ASUSModifyPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createNavigationBar];
    
    [self createNextStepView];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)createNavigationBar
{
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"忘记密码";
    self.navigationItem.titleView = titleLabel;
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
}

- (void)createNextStepView
{
    
    self.hideKeyBoardControl = [[UIControl alloc] initWithFrame:self.view.bounds];
    [self.hideKeyBoardControl addTarget:self action:@selector(hideKeyBoardAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.hideKeyBoardControl];
    
    self.nextStepView = [[UIView  alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    self.nextStepView.hidden = NO;
    self.nextStepView.userInteractionEnabled = YES;
    [self.hideKeyBoardControl addSubview:self.nextStepView];
    
    UIImageView *inputBackGroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 153)];
    inputBackGroundView.userInteractionEnabled = YES;
    [inputBackGroundView setImage:[UIImage imageNamed:@"modify_form_bg"]];
    [self.nextStepView addSubview:inputBackGroundView];
    
    
    self.oldPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width - 70, 50)];
    self.oldPasswordTextField.borderStyle = UITextBorderStyleNone;
    self.oldPasswordTextField.delegate = self;
    self.oldPasswordTextField.placeholder = @"请输入旧密码";
    self.oldPasswordTextField.textColor = [NCColorUtil colorForTitleOrNickFont];
    self.oldPasswordTextField.font = [UIFont systemFontOfSize:[NCFontUtil fontForPasswordText]];
    self.oldPasswordTextField.secureTextEntry = YES;
    [inputBackGroundView addSubview:self.oldPasswordTextField];
    
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 51, self.view.frame.size.width - 20, 50)];
    self.passwordTextField.borderStyle = UITextBorderStyleNone;
    self.passwordTextField.delegate = self;
    self.passwordTextField.placeholder = @"请输入新密码";
    self.passwordTextField.textColor = [NCColorUtil colorForTitleOrNickFont];
    self.passwordTextField.font = [UIFont systemFontOfSize:[NCFontUtil fontForPasswordText]];
    self.passwordTextField.secureTextEntry = YES;
    [inputBackGroundView addSubview: self.passwordTextField];
    
    self.rePasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 102, self.view.frame.size.width -20, 50)];
    self.rePasswordTextField.borderStyle = UITextBorderStyleNone;
    self.rePasswordTextField.delegate = self;
    self.rePasswordTextField.placeholder = @"请确认新密码";
    self.rePasswordTextField.textColor = [NCColorUtil colorForTitleOrNickFont];
    self.rePasswordTextField.font = [UIFont systemFontOfSize:[NCFontUtil fontForPasswordText]];
    self.rePasswordTextField.secureTextEntry = YES;
    [inputBackGroundView addSubview:self.rePasswordTextField];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(15, 170, ScreenWidth- 30, 40);
    [sureButton setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorForNavgationBarBackground]] forState:UIControlStateNormal];
    [sureButton setBackgroundImage:[NCImageUtil createImageWithColor:[[NCColorUtil colorForNavgationBarBackground] colorWithAlphaComponent:0.9]] forState:UIControlStateHighlighted];
    sureButton.layer.cornerRadius = 3;
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    [sureButton setBackgroundColor:[NCColorUtil colorForNavgationBarBackground]];
    [self.nextStepView addSubview:sureButton];

    
}

#pragma mark - Action Method
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkParametersIsValid
{
    [self hideKeyBoardAction];
    if (self.oldPasswordTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入旧密码"];
        [self hideHudAfter:1.0];
        return NO;
    }
    if (self.passwordTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入新密码"];
        [self hideHudAfter:1.0];
        return NO;
    }
    if (self.rePasswordTextField.text.length <1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入确认密码"];
        [self hideHudAfter:1.0];
        return NO;
    }
    if (![self.passwordTextField.text isEqual:self.rePasswordTextField.text]) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"您两次输入的密码不一致！请重新输入"];
        [self hideHudAfter:1.0];

        return NO;
    }
    return YES;
}

- (void)sureClick
{
    if ([self checkParametersIsValid]) {
        [self showHud:HBHudShowTypeActivityOnly withText:nil];
        
    }else {
        return;
    }
}

- (void)clearTextField
{
    [self.oldPasswordTextField becomeFirstResponder];
    self.oldPasswordTextField.text = nil;
    self.passwordTextField.text = nil;
    self.rePasswordTextField.text = nil;
}

- (void)hideKeyBoardAction
{
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.rePasswordTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end

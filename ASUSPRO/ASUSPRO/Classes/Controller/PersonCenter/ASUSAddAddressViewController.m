//
//  ASUSAddAddressViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSAddAddressViewController.h"

#import "ASUSReceiveAddressModel.h"
#import "BAAuthenticate.h"

@interface ASUSAddAddressViewController ()<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) UITableView *addAddressTableView;

@property (nonatomic, strong) NSArray *addAddressArr;

@property (nonatomic, strong) UITextField *userTextField;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *areaTextField;
@property (nonatomic, strong) UITextField *addressTextField;
@property (nonatomic, strong) UITextField *postCodeTextField;

@property (nonatomic, strong) UIPickerView *areaPickView;
@property (nonatomic, strong) UIToolbar *pickerToolbar;

@property (strong, nonatomic) NSDictionary *pickerDic;
@property (nonatomic, strong) NSArray *provinceArr;
@property (nonatomic, strong) NSArray *cityArr;
@property (nonatomic, strong) NSArray *townArr;
@property (nonatomic, strong) NSArray *selectedArray;

@end

@implementation ASUSAddAddressViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    
    [self setUpPickerView];
    
    [self createPickerToolBar];
    
    [self setUpView];
}

#pragma mark - RequestData

- (void)loadData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Address" ofType:@"plist"];
    self.pickerDic = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.provinceArr = [self.pickerDic allKeys];
    self.selectedArray = [self.pickerDic objectForKey:[[self.pickerDic allKeys] objectAtIndex:0]];
    
    if (self.selectedArray.count > 0) {
        self.cityArr = [[self.selectedArray objectAtIndex:0] allKeys];
    }
    
    if (self.cityArr.count > 0) {
        self.townArr = [[self.selectedArray objectAtIndex:0] objectForKey:[self.cityArr objectAtIndex:0]];
    }
}

#pragma mark - views

- (void)configNavigationBar {
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    if (!self.addressModel) {
        titleLabel.text = @"新增地址";
    }else{
        titleLabel.text = @"编辑地址";
    }
    self.navigationItem.titleView = titleLabel;
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
    
}

#pragma mark - Event Response
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -  SetupView

- (void)setUpView
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoardAction)];
    [self.view addGestureRecognizer:tap];
    
    self.userTextField = [self setUpCellWithTitle:@"收货人" placeHolder:@"名字" height:0];
    
    self.phoneTextField = [self setUpCellWithTitle:@"手机号码" placeHolder:@"11位手机号码" height:45];
    self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    self.areaTextField = [self setUpCellWithTitle:@"选择地区" placeHolder:@"地区信息" height:45*2];
    self.areaTextField.inputView = self.areaPickView;
    self.areaTextField.inputAccessoryView = self.pickerToolbar;
    
    self.addressTextField = [self setUpCellWithTitle:@"详细地址"  placeHolder:@"街道门牌信息" height:45*3];
    
    self.postCodeTextField =  [self setUpCellWithTitle:@"邮编" placeHolder:@"邮政编码" height:45*4];
    self.postCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    if (self.addressModel) {
        self.userTextField.text = self.addressModel.userName;
        self.phoneTextField.text = self.addressModel.mobile;
        self.areaTextField.text =  [NSString stringWithFormat:@"%@%@%@",self.addressModel.province,self.addressModel.city,self.addressModel.address];
        self.addressTextField.text = self.addressModel.address;
        self.postCodeTextField.text = self.addressModel.postCode;
    }
    
}

- (UITextField *)setUpCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder height:(CGFloat)height
{
    UIView *backgroupView = [[UIView alloc] initWithFrame:CGRectMake(0, height, ScreenWidth, 44)];
    backgroupView.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, ScreenWidth-200, 30)];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont systemFontOfSize:16.0];
    titleLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    titleLabel.text = title;
    [backgroupView addSubview:titleLabel];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(ScreenWidth-200, 7, 180, 30)];
    textField.textAlignment = NSTextAlignmentRight;
    textField.delegate = self;
    textField.placeholder = placeholder;
    textField.font = [UIFont systemFontOfSize:14.0];
    textField.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    [backgroupView addSubview:textField];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, ScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
    [backgroupView addSubview:lineView];
    
    [self.view addSubview:backgroupView];
    
    return textField;
}

- (void)setUpPickerView
{
    self.areaPickView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
    self.areaPickView.tintColor = [UIColor blackColor];
    self.areaPickView.alpha = 0.85;
    self.areaPickView.delegate = self;
    self.areaPickView.dataSource = self;
    self.areaPickView.showsSelectionIndicator = YES;
    //    [self.areaPickView selectRow:0 inComponent:3 animated:NO];
}

- (void)createPickerToolBar
{
    self.pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.pickerToolbar.barTintColor = [UIColor whiteColor];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(10, 5, 60, 34);
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerToolbar addSubview:cancelButton];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(self.view.frame.size.width - 70, 5, 60, 34);
    [sureButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerToolbar addSubview:sureButton];
}
#pragma mark -Private Method

- (BOOL)checkParametersIsValid
{
    if (self.userTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写收货人"];
        [self hideHudAfter:1];
        return NO;
    }
    if (self.phoneTextField.text.length < 11) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写11位手机号码"];
        [self hideHudAfter:1];
        return NO;
    }
    if (self.areaTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写地区信息"];
        [self hideHudAfter:1];
        return NO;
    }
    if (self.addressTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写街道门牌信息"];
        [self hideHudAfter:1];
        return NO;
    }
    if (self.postCodeTextField.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写邮政编码"];
        [self hideHudAfter:1];
        return NO;
    }
    return YES;
    
}

- (void)clearTextField
{
    self.userTextField.text = nil;
    self.phoneTextField.text = nil;
    self.areaTextField.text = nil;
    self.addressTextField.text = nil;
    self.postCodeTextField.text = nil;
}

#pragma mark - Action Method

- (void)cancelAction
{
    [self.areaTextField endEditing:NO];
    
}
- (void)sureAction
{
    [self.areaTextField endEditing:YES];
    self.areaTextField.text = [NSString stringWithFormat:@"%@%@%@",[self.provinceArr objectAtIndex:[self.areaPickView selectedRowInComponent:0]],[self.cityArr objectAtIndex:[self.areaPickView selectedRowInComponent:1]],[self.townArr objectAtIndex:[self.areaPickView selectedRowInComponent:2]]];
}

- (void)saveAction
{
    [self hideKeyBoardAction];
    
    if ([self checkParametersIsValid]) {
        if (!self.addressModel) {
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSReceiveAddressModel addReceiveAddressByUserID:[[BAAuthenticate sharedClient].userId integerValue] userName:self.userTextField.text provinceCode:[self.provinceArr objectAtIndex:[self.areaPickView selectedRowInComponent:0]] city:[self.cityArr objectAtIndex:[self.areaPickView selectedRowInComponent:1]] area:[self.townArr objectAtIndex:[self.areaPickView selectedRowInComponent:2]] address:self.addressTextField.text zip:self.postCodeTextField.text mobile:self.phoneTextField.text  isDefault:1 completion:^(NSError *error, NSString *message) {
                [self hideHud];
                [self clearTextField];
                [self showHud:HBHudShowTypeCaptionOnly withText:message];
                [self hideHudAfter:2.0f];
                if ([message isEqualToString:@"收货地址信息添加成功"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
         }else{
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSReceiveAddressModel editReceiveAddressByAddressID:self.addressModel.addressId userID:[[BAAuthenticate sharedClient].userId integerValue] userName:self.userTextField.text provinceCode:[self.provinceArr objectAtIndex:[self.areaPickView selectedRowInComponent:0]] city:[self.cityArr objectAtIndex:[self.areaPickView selectedRowInComponent:1]] area:[self.townArr objectAtIndex:[self.areaPickView selectedRowInComponent:2]] address:self.addressTextField.text zip:self.postCodeTextField.text mobile:self.phoneTextField.text  isDefault:1 completion:^(NSError *error, NSString *message) {
                [self hideHud];
                [self clearTextField];
                [self showHud:HBHudShowTypeCaptionOnly withText:message];
                [self hideHudAfter:2.0f];
                NSLog(@"editmessage:%@",message);
                if ([message isEqualToString:@"收货地址信息修改成功"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];

        }
    }
    
}



- (void)hideKeyBoardAction
{
    [self.userTextField resignFirstResponder];
    [self.phoneTextField resignFirstResponder];
    [self.areaTextField resignFirstResponder];
    [self.addressTextField resignFirstResponder];
    [self.postCodeTextField resignFirstResponder];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.userTextField)
    {
        [self.phoneTextField becomeFirstResponder];
    }else if(textField == self.phoneTextField){
        [self.areaTextField becomeFirstResponder];
    }else if(textField == self.areaTextField){
        [self.addressTextField becomeFirstResponder];
    }else if(textField == self.addressTextField){
        [self.postCodeTextField becomeFirstResponder];
    }else {
        [self saveAction];
    }
    
    return YES;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.provinceArr count];
    }else if (component == 1){
        return [self.cityArr count];
    }else {
        return [self.townArr count];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 100;
}

#pragma mark - UIPickerViewDelegate
-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.provinceArr objectAtIndex:row];
    }else if (component == 1){
        return [self.cityArr objectAtIndex:row];
    }else {
        return [self.townArr objectAtIndex:row];
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        self.selectedArray = [self.pickerDic objectForKey:[self.provinceArr objectAtIndex:row]];
        if (self.selectedArray.count > 0) {
            self.cityArr = [[self.selectedArray objectAtIndex:0] allKeys];
        } else {
            self.cityArr = nil;
        }
        if (self.cityArr.count > 0) {
            self.townArr = [[self.selectedArray objectAtIndex:0] objectForKey:[self.cityArr objectAtIndex:0]];
        } else {
            self.townArr = nil;
        }
    }
    [pickerView selectedRowInComponent:1];
    [pickerView reloadComponent:1];
    [pickerView selectedRowInComponent:2];
    
    if (component == 1) {
        if (self.selectedArray.count > 0 && self.cityArr.count > 0) {
            self.townArr = [[self.selectedArray objectAtIndex:0] objectForKey:[self.cityArr objectAtIndex:row]];
        } else {
            self.townArr = nil;
        }
        [pickerView selectRow:1 inComponent:2 animated:YES];
    }
    
    [pickerView reloadComponent:2];
}

@end

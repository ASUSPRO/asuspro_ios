//
//  ASUSEditInfoViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSEditInfoViewController.h"
#import "BAAuthenticate.h"


@interface ASUSEditInfoViewController ()<UITextFieldDelegate>
{
    USERINFO userinfo;
}
@property (nonatomic, strong) UITextField *editTextField;


@end

@implementation ASUSEditInfoViewController

- (id)initWithText:(NSString *)text userInfoType:(USERINFO)type
{
    if(self = [super init]){
        [self setUpViewWithText:text userInfoType:type];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configNavigationBar];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    saveBtn.frame = CGRectMake(0, 7, 60, 30);
    [saveBtn addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightButtonItem= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"编辑";
    self.navigationItem.titleView = titleLabel;
}

- (void)setUpViewWithText:(NSString *)text userInfoType:(USERINFO)type
{
    userinfo = type;
    
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
    
    self.editTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    self.editTextField.backgroundColor = [UIColor whiteColor];
    self.editTextField.textAlignment = NSTextAlignmentLeft;
    self.editTextField.layer.borderWidth = 1.0f;
    self.editTextField.text = text;
    self.editTextField.delegate = self;
    self.editTextField.layer.borderColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0].CGColor;
    self.editTextField.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    self.editTextField.font = [UIFont systemFontOfSize:16.0];
    [self.view addSubview:self.editTextField];
    
    UIView *paddingView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 45)];
    self.editTextField.leftView = paddingView;
    self.editTextField.leftViewMode = UITextFieldViewModeAlways;
    
}


#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveAction
{
    if (userinfo == USER_NAME) {
        [[BAAuthenticate sharedClient] setName:self.editTextField.text];
    }
    if (userinfo == USER_MOBILE) {
        [[BAAuthenticate sharedClient] setMobile:self.editTextField.text];
    }
    if (userinfo == USER_PHONE) {
        [[BAAuthenticate sharedClient] setPhone:self.editTextField.text];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITouch

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.editTextField becomeFirstResponder];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.editTextField resignFirstResponder];
}

@end

//
//  ASUSExchangeRecordViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeRecordViewController.h"
#import "ASUSExchangeRecordCell.h"
#import "ASUSProductDetailViewController.h"
#import "NCImageUtil.h"

#import "HuoBanKit.h"
#import "ASUSLoginViewController.h"

#import "BAAuthenticate.h"
#import "ASUSExchangeModel.h"
#import "ASUSOrderModel.h"
#import "ASUSLookExpressViewController.h"
#import "ASUSExchangeDetailViewController.h"
#import "ASUSProductDetailViewController.h"

@interface ASUSExchangeRecordViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    EXCHANGERECORDTYPE recordType;
}
@property (nonatomic, strong) UITableView *exchangeRecordTableView;
@property (nonatomic, strong) NSArray *exchangeList;
@end

@implementation ASUSExchangeRecordViewController

#pragma mark - lifeCycle Method
- (id)initWithType:(EXCHANGERECORDTYPE)type
{
    if (self = [super init]) {
        recordType = type;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigationBar];
    [self setupTableView];
    [self loadData];
    // Do any additional setup after loading the view.
}

#pragma mark - RequestData
- (void)loadData
{
    self.exchangeList = [[NSMutableArray alloc] initWithCapacity:0];
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error||[message isEqualToString:@"用户token有效"]) {
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSExchangeModel fetchExchangeListByUserID:[[BAAuthenticate sharedClient].userId integerValue] page:0 completion:^(NSError *error, NSArray *list) {
                [self hideHud];
                if (list) {
                    self.exchangeList = [NSArray arrayWithArray:list];
                    [self.exchangeRecordTableView reloadData];
                }
               
            }];
            
        }else {
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
    
    
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"兑换记录";
    self.navigationItem.titleView = titleLabel;
}

- (void)setupTableView {
    self.exchangeRecordTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kNavigationBarHeight)];
    self.exchangeRecordTableView.delegate = self;
    self.exchangeRecordTableView.scrollEnabled = YES;
    self.exchangeRecordTableView.dataSource = self;
    self.exchangeRecordTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.exchangeRecordTableView];
}
#pragma mark - Event Response
- (void)backAction
{
    if (recordType == DECORDTYPE) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[ASUSProductDetailViewController class]]) {
                [self.navigationController popToViewController:controller animated:NO];
            }
        }
    }else {
      [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.exchangeList.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kASUSExchangeRecordCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSExchangeRecordCell";
    ASUSExchangeRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSExchangeRecordCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    ASUSExchangeModel *model = (ASUSExchangeModel *)[self.exchangeList objectAtIndex:indexPath.row];
    [cell setUpData:model];
    cell.viewExpressBlock = ^(ASUSExchangeModel *model) {
        ASUSLookExpressViewController *lookExpressVc = [[ASUSLookExpressViewController alloc] initWithModel:model];
        [self.navigationController pushViewController:lookExpressVc animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    };
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ASUSExchangeDetailViewController *exchangeDetailVC = [[ASUSExchangeDetailViewController alloc] initWithModel:(ASUSExchangeModel *)[self.exchangeList objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:exchangeDetailVC animated:YES];
}



@end

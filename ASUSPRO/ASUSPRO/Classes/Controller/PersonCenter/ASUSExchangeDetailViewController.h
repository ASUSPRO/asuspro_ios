//
//  ASUSExchangeDetailViewController.h
//  ASUSPRO
//
//  Created by May on 15-6-22.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSExchangeModel.h"

@interface ASUSExchangeDetailViewController : ASUSBaseViewController
- (id)initWithModel:(ASUSExchangeModel *)model;
@end

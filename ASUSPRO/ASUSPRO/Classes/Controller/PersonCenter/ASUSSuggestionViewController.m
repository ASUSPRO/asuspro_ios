//
//  ASUSSuggestionViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSSuggestionViewController.h"
#import "BAAuthenticate.h"
#import "ASUSUserModel.h"

@interface ASUSSuggestionViewController ()<UITextViewDelegate>

@property (nonatomic, strong) UITextView *feedBackTextView;
@property (nonatomic, strong) UILabel *hintLabel;
@property (nonatomic, strong) UIButton *sureButton;
@end

@implementation ASUSSuggestionViewController

#pragma mark - lifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0];
    
    [self configNavigationBar];
    
    [self createTextView];
    // Do any additional setup after loading the view.
}

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"建议";
    self.navigationItem.titleView = titleLabel;
    
    
//    UIButton *finishButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    finishButton.frame = CGRectMake(0, 7, 60, 30) ;
//    [finishButton setTitle:@"完成" forState:UIControlStateNormal];
//    [finishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [finishButton.titleLabel setFont:[UIFont systemFontOfSize:[NCFontUtil fontForTitle]]];
//    [finishButton addTarget:self action:@selector(finishAction) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:finishButton];
//    self.navigationItem.rightBarButtonItem = rightButtonItem;

}

- (void)createTextView
{
//    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 10, ScreenWidth, 0.5)];
//    topLine.backgroundColor = [UIColor lightGrayColor];
//    [self.view addSubview:topLine];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, ScreenWidth, 210)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    self.feedBackTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)]; //初始化大小
    self.feedBackTextView.textColor = [NCColorUtil colorForHomeCellTitleColor];//设置textview里面的字体颜色
    self.feedBackTextView.font = [UIFont systemFontOfSize:14];//设置字体名字和字体大小
    self.feedBackTextView.delegate = self;//设置它的委托方法
    self.feedBackTextView.editable = YES;
    self.feedBackTextView.backgroundColor = [UIColor whiteColor];//设置它的背景颜色
    self.feedBackTextView.returnKeyType = UIReturnKeyDefault;//返回键的类型
    self.feedBackTextView.keyboardType = UIKeyboardTypeDefault;//键盘类型
    self.feedBackTextView.scrollEnabled = YES;//是否可以拖动
    self.feedBackTextView.textContainerInset = UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 0.0f);
    self.feedBackTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
    [bgView addSubview:self.feedBackTextView];
    
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 12, self.view.frame.size.width - 40, 30)];
    [self.view addSubview:_hintLabel];
    self.hintLabel.font = [UIFont systemFontOfSize:14];
    self.hintLabel.numberOfLines = 0;
    self.hintLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    self.hintLabel.text = @"请写下您的意见建议。";
    
//    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 209.5, ScreenWidth, 0.5)];
//    bottomLine.backgroundColor = [UIColor lightGrayColor];
//    [self.view addSubview:bottomLine];
    
    self.sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(15, 230, ScreenWidth- 30, 40);
    [self.sureButton setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorForNavgationBarBackground]] forState:UIControlStateNormal];
    [self.sureButton setBackgroundImage:[NCImageUtil createImageWithColor:[[NCColorUtil colorForNavgationBarBackground] colorWithAlphaComponent:0.9]] forState:UIControlStateHighlighted];
    self.sureButton.layer.cornerRadius = 3;
    [self.sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [self.sureButton addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    [self.sureButton setBackgroundColor:[NCColorUtil colorForNavgationBarBackground]];
    self.sureButton.enabled = NO;
    [self.view addSubview:self.sureButton];
    
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length == 0) {
        [_hintLabel setHidden:NO];
    } else {
        [_hintLabel setHidden:YES];
    }
    
    NSString *tempText=[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (tempText.length < 1) {
        [self.sureButton setEnabled:NO];
    } else {
        [self.sureButton setEnabled:YES];
    }
}


#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)sureClick
{
    [self.feedBackTextView resignFirstResponder];
    if (self.feedBackTextView.text.length < 1) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入建议"];
        [self hideHudAfter:2];
        return;
    }
    [self showHud:HBHudShowTypeActivityOnly withText:nil];
    [ASUSUserModel uploadForUserFeedBackByUserID:[[BAAuthenticate sharedClient].userId integerValue] userName:[BAAuthenticate sharedClient].userName feedBack:self.feedBackTextView.text completion:^(NSError *error, NSString *message) {
         [self hideHud];
        if (!error && [message isEqualToString:@"用户反馈上报成功"]) {
            self.feedBackTextView.text = @"";
            [self showHud:HBHudShowTypeCaptionOnly withText:message];
            [self hideHudAfter:2];
            _hintLabel.hidden = NO;
        }
    }];
}


@end

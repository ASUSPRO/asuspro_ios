//
//  ASUSAddressListViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSReceiveAddressModel.h"

typedef enum {
    EXCHANGE,
    SETTING
}PUSHTYPE;

typedef void(^ChooseAddressBlock)(ASUSReceiveAddressModel *addressModel);

@interface ASUSAddressListViewController : ASUSBaseViewController

- (id)initWithType:(PUSHTYPE)type;

@property(nonatomic, copy)ChooseAddressBlock chooseAddressBlock;

@end

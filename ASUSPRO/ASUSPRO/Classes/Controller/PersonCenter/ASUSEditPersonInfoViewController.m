//
//  ASUSEditPersonInfoViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSEditPersonInfoViewController.h"
#import "ASUSEditInfoViewController.h"
#import "BAAuthenticate.h"
#import "ASUSUserModel.h"
#import "HuoBanKit.h"
#import "ASUSLoginViewController.h"

@interface ASUSEditPersonInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) UITableView *editPersonInfoTableView;

@property (nonatomic, strong) NSArray *personInfoDetailArr;
@property (nonatomic, strong) NSArray *personInfoTitleArr;

@property (nonatomic, strong) UITextField *birthdayTextField;
@property (nonatomic, strong) UIDatePicker *birthdayPicker;
@property (nonatomic, strong) UIToolbar *birthdayToolbar;

@property (nonatomic, strong) NSDate *selecteDate;

@end

@implementation ASUSEditPersonInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigationBar];
    
    [self createPickerView];
    
    [self createBirthdayToolBar];
    
    [self setUpTextField];
    
    [self setUpTableView];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"个人资料";
    self.navigationItem.titleView = titleLabel;
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setTitle:@"完成" forState:UIControlStateNormal];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    saveBtn.frame = CGRectMake(0, 7, 60, 30);
    [saveBtn addTarget:self action:@selector(updateAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightButtonItem= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;

}

- (void)setUpTableView
{
    self.editPersonInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    self.editPersonInfoTableView.delegate = self;
    self.editPersonInfoTableView.dataSource = self;
    self.editPersonInfoTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.editPersonInfoTableView];
}

- (void)setUpTextField
{
    self.birthdayTextField = [[UITextField alloc] initWithFrame:CGRectMake(100, 5, self.view.frame.size.width-130, 45)];
    self.birthdayTextField.textAlignment = NSTextAlignmentRight;
    self.birthdayTextField.delegate = self;
    self.birthdayTextField.textColor = [UIColor colorWithRed:132.0/255 green:137.0/255 blue:143.0/255 alpha:1.0];
    self.birthdayTextField.font = [UIFont systemFontOfSize:16.0];
    self.birthdayTextField.inputView = self.birthdayPicker;
    self.birthdayTextField.inputAccessoryView = self.birthdayToolbar;
}

- (void)createPickerView
{
    self.birthdayPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 216)];
    [self.birthdayPicker setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [self.birthdayPicker setMaximumDate:[NSDate date]];
    [self.birthdayPicker setDatePickerMode:UIDatePickerModeDate];
    [self.birthdayPicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
}


- (void)createBirthdayToolBar
{
    self.birthdayToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    self.birthdayToolbar.barTintColor = [UIColor whiteColor];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(10, 5, 60, 34);
    [cancelButton setTitleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0] forState:UIControlStateNormal];
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(birthdayCancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.birthdayToolbar addSubview:cancelButton];
    
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = CGRectMake(self.view.frame.size.width - 70, 5, 60, 34);
    [sureButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(birthdaySureAction) forControlEvents:UIControlEventTouchUpInside];
    [self.birthdayToolbar addSubview:sureButton];
    
}

- (void)loadData
{
    self.personInfoTitleArr = @[@[@"账号",@"名字",@"出生日期",@"性别",@"手机"],@[@"公司",@"公司座机"]];
    if ([BAAuthenticate sharedClient].userId) {
        NSString *sex;
        if ([[BAAuthenticate sharedClient].gender integerValue]==0) {
            sex = @"男";
        }else {
            sex = @"女";
        }
        self.personInfoDetailArr = @[@[[BAAuthenticate sharedClient].userName,[BAAuthenticate sharedClient].name,[BAAuthenticate sharedClient].brithday,sex,[BAAuthenticate sharedClient].mobile],@[[BAAuthenticate sharedClient].company,[BAAuthenticate sharedClient].phone]];
    }
    [self.editPersonInfoTableView reloadData];
}


#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateAction{
    [self.birthdayTextField resignFirstResponder];
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error||[message isEqualToString:@"用户token有效"]) {
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSUserModel updateUserInfoByUserID:[[BAAuthenticate sharedClient].userId integerValue] name:[BAAuthenticate sharedClient].name pay_password:[BAAuthenticate sharedClient].payPassword birthdday:[BAAuthenticate sharedClient].brithday mobile:[BAAuthenticate sharedClient].mobile phone:[BAAuthenticate sharedClient].phone completion:^(NSError *error, NSString *message) {
                [self hideHud];
                if (!error) {
                    [self showHud:HBHudShowTypeCaptionOnly withText:message];
                    [self hideHudAfter:1.0f];
                }
            }];
        }else {
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
}


- (void)datePickerValueChanged
{
    // 获得当前UIPickerDate所在的时间
    self.selecteDate = [self.birthdayPicker date];
}

- (void)birthdayCancelAction
{
    [self.birthdayTextField endEditing:NO];
}
- (void)birthdaySureAction
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if(self.selecteDate) {
        self.birthdayTextField.text = [formatter stringFromDate:self.selecteDate];
    }else {
        self.birthdayTextField.text = [formatter stringFromDate:[NSDate date]];
    }
    [[BAAuthenticate sharedClient] setBrithday:self.birthdayTextField.text];
    [self.birthdayTextField endEditing:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 5;
    }else {
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return 0;
//    }else {
//        return 15;
//    }
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 15)];
//    headerView.backgroundColor = [UIColor clearColor];
//    if (section == 0) {
//        headerView.frame = CGRectMake(0, 0, ScreenWidth, 0);
//    }
//    headerView.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
//    headerView.layer.borderWidth = 0.5;
//    return headerView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"EditPersonInfoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    if (indexPath.section == 0) {
        if (indexPath.row == 0 || indexPath.row == 3) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else {
           cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }else {
        if (indexPath.row == 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    cell.textLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0];
    cell.textLabel.text = self.personInfoTitleArr[indexPath.section][indexPath.row];
    if (indexPath.section == 0 &&indexPath.row == 2 ) {
        self.birthdayTextField.text = self.personInfoDetailArr[indexPath.section][indexPath.row];
        [cell addSubview:self.birthdayTextField];
    }else {
        cell.detailTextLabel.textColor = [UIColor colorWithRed:132.0/255 green:137.0/255 blue:143.0/255 alpha:1.0];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
        cell.detailTextLabel.text = self.personInfoDetailArr[indexPath.section][indexPath.row];
    }
   
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 1) {
        ASUSEditInfoViewController *vc = [[ASUSEditInfoViewController alloc] initWithText:self.personInfoDetailArr[indexPath.section][indexPath.row] userInfoType:USER_NAME];
         [self.navigationController pushViewController:vc animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
    
    if (indexPath.section == 0 && indexPath.row == 4) {
        ASUSEditInfoViewController *vc = [[ASUSEditInfoViewController alloc] initWithText:self.personInfoDetailArr[indexPath.section][indexPath.row] userInfoType:USER_MOBILE];
        [self.navigationController pushViewController:vc animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
   
    if (indexPath.section == 1 && indexPath.row == 1) {
        ASUSEditInfoViewController *vc = [[ASUSEditInfoViewController alloc] initWithText:self.personInfoDetailArr[indexPath.section][indexPath.row] userInfoType:USER_PHONE];
        [self.navigationController pushViewController:vc animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}

#pragma mark - UITouch

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.birthdayTextField becomeFirstResponder];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.birthdayTextField resignFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

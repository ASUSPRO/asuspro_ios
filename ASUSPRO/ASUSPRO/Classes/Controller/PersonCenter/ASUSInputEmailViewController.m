//
//  ASUSInputEmailViewController.m
//  ASUSPRO
//
//  Created by May on 15-6-6.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSInputEmailViewController.h"
#import "ASUSUserModel.h"

@interface ASUSInputEmailViewController ()
@property (nonatomic, strong) UIView *inputEmailView;
@property (nonatomic, strong) UITextField *emailTextField;
@end

@implementation ASUSInputEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    [self setupViews];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:15 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"忘记密码";
    self.navigationItem.titleView = titleLabel;
}

- (void)setupViews {
    self.inputEmailView = [[UIView alloc] initWithFrame:CGRectMake(-1, 10, ScreenWidth+2, 45)];
    self.inputEmailView.backgroundColor = [UIColor whiteColor];
    self.inputEmailView.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.inputEmailView.layer.borderWidth = 0.5;
    [self.view addSubview:self.inputEmailView];
    
    self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, self.inputEmailView.frame.size.width - 10, 45)];
    self.emailTextField.borderStyle = UITextBorderStyleNone;
    self.emailTextField.placeholder = @"";
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    self.emailTextField.font = [UIFont systemFontOfSize:[NCFontUtil fontForPasswordText]];
    [self.view addSubview:self.emailTextField];
    
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 75, ScreenWidth - 20, 15)];
    tipLabel.backgroundColor = [UIColor clearColor];
    tipLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    tipLabel.font = [UIFont systemFontOfSize:14];
    tipLabel.text = @"请输入登记的邮箱账号";
    [self.view addSubview:tipLabel];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(10, 112, ScreenWidth - 20, 40);
    nextButton.layer.cornerRadius = 2;
    [nextButton setTitle:@"发送" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorForNavgationBarBackground]] forState:UIControlStateNormal];
    [nextButton setBackgroundImage:[NCImageUtil createImageWithColor:[[NCColorUtil colorForNavgationBarBackground] colorWithAlphaComponent:0.9]] forState:UIControlStateHighlighted];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton addTarget:self action:@selector(nextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    
}

#pragma mark - UIButton Actions

- (void)nextButtonClicked {
    [self.emailTextField resignFirstResponder];
    if (self.emailTextField.text.length <= 0) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入邮箱"];
        [self hideHudAfter:1.0];
        return;
    }
    if (![self isValidateEmail:self.emailTextField.text]) {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"邮箱不合法"];
        [self hideHudAfter:1.0];
        return;
    }
    [ASUSUserModel sendEmialByEmial:self.emailTextField.text completion:^(NSError *error, NSString *message) {
        if (error == nil) {
            [self showHud:HBHudShowTypeCaptionOnly withText:@"发送成功"];
            [self hideHudAfter:1.0];
        } else {
            [self showHud:HBHudShowTypeCaptionOnly withText:message];
            [self hideHudAfter:1.0];
        }
    }];
}

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Utils
- (BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailTextField resignFirstResponder];
}

@end

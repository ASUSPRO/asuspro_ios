//
//  ASUSEditInfoViewController.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

typedef  enum {
    USER_NAME,
    USER_BIRTHDAY,
    USER_MOBILE,
    USER_PHONE
}USERINFO;

#import "ASUSBaseViewController.h"

@interface ASUSEditInfoViewController : ASUSBaseViewController

- (id)initWithText:(NSString *)text userInfoType:(USERINFO)type;

@end

//
//  ASUSPersonCenterViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPersonCenterViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "ASUSExchangeRecordViewController.h"
#import "NCImageUtil.h"
#import "ASUSAddressListViewController.h"
#import "ASUSSettingViewController.h"
#import "ASUSInfoPlatformViewController.h"
#import "ASUSEditPersonInfoViewController.h"
#import "ASUSUserModel.h"
#import "BAAuthenticate.h"
#import "HuoBanKit.h"
#import "ASUSLoginViewController.h"

#import "UIImageView+AFNetworking.h"

@interface ASUSPersonCenterViewController () <UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITableView *personInformationTableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *cellIconArray;
@property (nonatomic, strong) NSString *pointsNum;
@property (nonatomic, strong) UIImageView *avatar;

@end

@implementation ASUSPersonCenterViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createNavigationBar];
    [self initData];
    [self setUpTableView];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    self.navigationController.navigationBar.clipsToBounds = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorFromHexString:@"#363A3F"] ]forBarMetrics:UIBarMetricsDefault];

}

- (void)initData
{
    self.titleArray = @[@"华硕商用资料平台",@"兑换记录",@"我的积分",@"地址",@"设置"];
    self.cellIconArray = @[@"\ue619",@"\ue618",@"\ue608",@"\ue616",@"\ue617"];
    self.pointsNum = [BAAuthenticate sharedClient].score;
}


- (NSString *)replaceWithString:(NSString *)str
{
    if (str && ![str isEqualToString:@""]) {
        NSString *replaceStr = [str substringWithRange:NSMakeRange(3, 4)];
        return [str stringByReplacingOccurrencesOfString:replaceStr withString:@"****"];
    }
    return str;
}

- (void)createNavigationBar
{
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue607" fontSize:20 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(editAction)];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
}

- (UIView *)createHeadView
{
    UIImageView *headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 215)];
    headView.image = [UIImage imageNamed:@"PersonBackground"];
    headView.userInteractionEnabled = YES;
    
    UIImageView *avatarBg = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-36, 68 - 35, 72, 72)];
    avatarBg.layer.cornerRadius = 34;
    avatarBg.layer.masksToBounds = YES;
    avatarBg.backgroundColor = [UIColor grayColor];
    [headView addSubview:avatarBg];
    
    self.avatar = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-34, 70 - 35, 68, 68)];
    self.avatar.userInteractionEnabled = YES;
    self.avatar.layer.cornerRadius = 34;
    self.avatar.layer.masksToBounds = YES;
    [self.avatar setImageWithURL:[NSURL URLWithString:[BAAuthenticate sharedClient].avatarLink] placeholderImage:[UIImage imageNamed:@""]];
    [headView addSubview:self.avatar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateAvatar)];
    [self.avatar addGestureRecognizer:tap];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 160 - 35, self.view.frame.size.width - 20, 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont systemFontOfSize:16];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.text = [BAAuthenticate sharedClient].userName;
    [headView addSubview:nameLabel];
    
    return headView;
}

- (void)setUpTableView {
    self.personInformationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64)];
    self.personInformationTableView.delegate = self;
    self.personInformationTableView.dataSource = self;
    self.personInformationTableView.tableHeaderView = [self createHeadView];
    self.personInformationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.personInformationTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Method
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateAvatar
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
        if (authStatus == ALAuthorizationStatusDenied) {
//            [self alert:@"请授权本App可以访问相册\n设置方式:手机设置->隐私->照片\n允许本App访问相册"];
            return;
        }
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"拍照", @"从手机相册选择",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
    
}

- (void)editAction {
    ASUSEditPersonInfoViewController *editVC = [[ASUSEditPersonInfoViewController alloc] init];
    [self.navigationController pushViewController:editVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    [picker dismissViewControllerAnimated:YES completion:Nil];
    NSData *imageData = UIImageJPEGRepresentation(image,0.5);
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error ||[message isEqualToString:@"用户token有效"]) {
            if ([[BAAuthenticate sharedClient] userId]) {
                [ASUSUserModel updateAvatarByUserID:[[[BAAuthenticate sharedClient] userId] integerValue] avatarData:imageData completion:^(NSError *error, NSString *avatarUrl) {
                    if (error == nil) {
                        [[BAAuthenticate sharedClient] setAvatarLink:avatarUrl];
                        [self.avatar setImageWithURL:[NSURL URLWithString:avatarUrl]];
                    } else {
                        //                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"无法访问" message:@"相机被禁用，请在iPhone的“设置-隐私-相机”选项中，允许网聚昌平访问你的相机"  delegate:nil cancelButtonTitle:@"确定"  otherButtonTitles:nil];
                        //                [alertView show];
                    }
                }];
            }
        }else{
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
 
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusAuthorized || authStatus == AVAuthorizationStatusNotDetermined) {
                
            } else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"无法访问" message:@"相机被禁用，请在iPhone的“设置-隐私-相机”选项中，允许网聚昌平访问你的相机"  delegate:nil cancelButtonTitle:@"确定"  otherButtonTitles:nil];
                [alertView show];
                return;
            }
        }
        UIImagePickerController *camera = [[UIImagePickerController alloc] init];
        camera.delegate = self;
        camera.allowsEditing = NO;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            camera.sourceType = UIImagePickerControllerSourceTypeCamera;
            //此处设置只能使用相机，禁止使用视频功能
            camera.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage,nil];
        } else {
            NSLog(@"相机功能不可用");
            return;
        }
        [self presentViewController:camera animated:YES completion:nil];
    } else if (buttonIndex == 1) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
            if (authStatus == ALAuthorizationStatusDenied) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"无法访问" message:@"请在“设置”-“隐私”-“照片”打开照片访问权限"  delegate:nil cancelButtonTitle:@"确定"  otherButtonTitles:nil];
                [alertView show];
                return;
            }
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        //从相册列表选取
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            //此处设置只能使用相机，禁止使用视频功能
            picker.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage,nil];
        }
        [self presentViewController:picker animated:YES completion:nil];
    } else if(buttonIndex == 2) {
        //取消
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 50;
    }
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"NCPersionInformationCell";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(nil == cell){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    cell.detailTextLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForContent]];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    cell.detailTextLabel.numberOfLines = 0;
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 49.5, ScreenWidth, 0.5)];
    separator.backgroundColor = [NCColorUtil colorForSeparatorLine];
    [cell.contentView addSubview:separator];
    if (indexPath.row != 2) {
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
        arrow.image = [NCImageUtil imageFromText:@"\ue60d" fontSize:20 color:[NCColorUtil colorFromHexString:@"#cfcfcf"]];
        cell.accessoryView = arrow;
    }else{
        cell.detailTextLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
        cell.detailTextLabel.text = self.pointsNum;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    float fontSize = 20;
    if (indexPath.row == 1) {
        fontSize = 18;
    }
    cell.imageView.image = [NCImageUtil imageFromText:[self.cellIconArray objectAtIndex:indexPath.row] fontSize:fontSize color:[NCColorUtil colorFromHexString:@"#84898f"]];
    cell.textLabel.text = self.titleArray[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *vcArray = @[@"ASUSInfoPlatformViewController",@"ASUSExchangeRecordViewController",@"",@"ASUSAddressListViewController",@"ASUSSettingViewController"];
    if(indexPath.row != 2 && indexPath.row != 3) {
        id obj = vcArray[indexPath.row];
        Class class = NSClassFromString(obj);
        UIViewController *viewController = [[class alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    } else if (indexPath.row == 3) {
        ASUSAddressListViewController *viewController = [[ASUSAddressListViewController alloc] initWithType:SETTING];
        [self.navigationController pushViewController:viewController animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}


@end

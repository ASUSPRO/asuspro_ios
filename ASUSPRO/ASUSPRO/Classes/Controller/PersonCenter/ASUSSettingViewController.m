//
//  ASUSSettingViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSSettingViewController.h"
#import "ASUSBaseCell.h"
#import "HuoBanKit.h"
#import "ASUSLoginViewController.h"
#import "ASUSSuggestionViewController.h"
#import "ASUSModifyPasswordViewController.h"

//#import "SDImageCache.h"

@interface ASUSSettingViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *settingTableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSString *cacheSize;
@end

@implementation ASUSSettingViewController

//#pragma mark - life cycle
//
//#pragma mark - UITableviewDataSource
//
//
//#pragma mark - CustomDelegate
//
//#pragma mark - Event Response
//
//#pragma mark - Private Method
//
//#pragma mark - Getter And Setter


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    [self setupTableView];
    self.cacheSize = @"";
    self.titleArray = @[@[@"修改密码",@"赏个好评",@"清除缓存",@"反馈建议"],@[@"退出登录"]];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"设置";
    self.navigationItem.titleView = titleLabel;
}


- (void)setupTableView {
    self.settingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    self.settingTableView.delegate = self;
    self.settingTableView.scrollEnabled = YES;
    self.settingTableView.dataSource = self;
    self.settingTableView.backgroundColor = [UIColor clearColor];
//    self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.settingTableView];
}

#pragma mark - Event Response
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.titleArray objectAtIndex:section] count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return kASUSExchangeRecordCellHeight;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSBaseCell";
    ASUSBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSBaseCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [NCColorUtil colorForHomeCellTitleColor];
        cell.bottomLineView.hidden = YES;
    }
    cell.textLabel.text = [[self.titleArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.section == 0 && indexPath.row == 2) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *path = [paths lastObject];
        self.cacheSize = [NSString stringWithFormat:@"%.1fM", [self folderSizeAtPath:path]];
        cell.detailTextLabel.text = self.cacheSize;
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        cell.detailTextLabel.textColor = [NCColorUtil colorFromHexString:@"#cfcfcf"];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 0 && (indexPath.row == 3 || indexPath.row == 0)) {
        UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
        arrow.image = [NCImageUtil imageFromText:@"\ue60d" fontSize:20 color:[NCColorUtil colorFromHexString:@"#cfcfcf"]];
        cell.accessoryView = arrow;
    }
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            ASUSModifyPasswordViewController *modifyPwdVC = [[ASUSModifyPasswordViewController alloc] init];
            [self.navigationController pushViewController:modifyPwdVC animated:YES];
            if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                self.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        } else if (indexPath.row == 1) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",@"123456"]]];
         } else if (indexPath.row == 2) {
            [self clearCache];
             UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
             cell.detailTextLabel.text = @"0.0M";
//            [self showHud:HBHudShowTypeCaptionOnly withText:@"无缓存"];
//            [self hideHud];
        } else if (indexPath.row == 3) {
            ASUSSuggestionViewController *suVC = [[ASUSSuggestionViewController alloc] init];
            [self.navigationController pushViewController:suVC animated:YES];
        }
       
    }
    if (indexPath.section == 1) {
        [HuoBanKit logout];
        ASUSLoginViewController *lvc = [[ASUSLoginViewController alloc] init];
        [self presentViewController:lvc animated:YES completion:nil];
    }
}

#pragma mark - private method

- (long long)fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

//遍历文件夹获得文件夹大小，返回多少M
- (float )folderSizeAtPath:(NSString*) folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath])
        return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0*1024.0);
}

- (void)clearCache {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *path = [paths lastObject];
    NSString *str = [NSString stringWithFormat:@"缓存已清除%@", self.cacheSize];
    [self showHud:HBHudShowTypeCaptionOnly withText:str];
    [self hideHudAfter:2];
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:path];
    for (NSString *p in files) {
        NSError *error;
        NSString *Path = [path stringByAppendingPathComponent:p];
        if ([[NSFileManager defaultManager] fileExistsAtPath:Path]) {
            [[NSFileManager defaultManager] removeItemAtPath:Path error:&error];
        }
    }
}




@end

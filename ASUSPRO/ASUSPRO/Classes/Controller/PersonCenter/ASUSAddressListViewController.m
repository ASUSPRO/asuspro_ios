//
//  ASUSAddressListViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSAddressListViewController.h"
#import "NCImageUtil.h"
#import "ASUSAddressListCell.h"
#import "ASUSAddAddressViewController.h"
#import "ASUSLoginViewController.h"

#import "ASUSReceiveAddressModel.h"
#import "HuoBanKit.h"
#import "BAAuthenticate.h"

@interface ASUSAddressListViewController ()<UITableViewDataSource, UITableViewDelegate,ASUSAddressListCellDelegate>
{
    PUSHTYPE pushType;
}
@property (nonatomic, strong) UITableView *addressListTableView;
@property (nonatomic, strong) NSArray *addressList;
@end

@implementation ASUSAddressListViewController

#pragma mark - liftCycle

- (id)initWithType:(PUSHTYPE)type
{
    if (self = [super init]) {
        pushType = type;
        [self loadData];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    [self setupTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - RequestData
- (void)loadData
{
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error &&[message isEqualToString:@"用户token有效"]) {
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSReceiveAddressModel fetchReceiveAddressByUserID:[[BAAuthenticate sharedClient].userId integerValue] completion:^(NSError *error, NSArray *list) {
                [self hideHud];
                self.addressList = [NSArray arrayWithArray:list];
                [self.addressListTableView reloadData];
            }];
        }else {
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
   
}

- (void)deleteDataWithAddressId:(NSInteger)addressId
{
    [ASUSReceiveAddressModel deleteReceiveAddressByUserID:[[BAAuthenticate sharedClient].userId integerValue] addressID:addressId completion:^(NSError *error, NSArray *list) {
        NSLog(@"%@",list);
        NSMutableArray *adds = [[NSMutableArray alloc] initWithCapacity:0];
        for (ASUSReceiveAddressModel *addressModel in self.addressList) {
            if (addressId != addressModel.addressId) {
                [adds addObject:addressModel];
            }
        }
        self.addressList = adds;
        [self.addressListTableView reloadData];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASUSReceiveAddress"];
        ASUSReceiveAddressModel *addModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (addModel.addressId == addressId) {
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"ASUSReceiveAddress"];
        }
    }];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"地址";
    self.navigationItem.titleView = titleLabel;
}

- (void)setupTableView {
    self.addressListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    self.addressListTableView.delegate = self;
    self.addressListTableView.scrollEnabled = YES;
    self.addressListTableView.dataSource = self;
    self.addressListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.addressListTableView.tableFooterView = [self createTableViewFooterView];
    [self.view addSubview:self.addressListTableView];
}


- (UIView *)createTableViewFooterView {
    UIButton *addAddressButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addAddressButton.frame = CGRectMake(0, 0, ScreenWidth, 40);
    [addAddressButton setTitle:@"新增地址" forState:UIControlStateNormal];
    [addAddressButton setTitleColor:RGBCOLOR(54.0, 152.0, 250.0) forState:UIControlStateNormal];
    addAddressButton.titleLabel.font =[UIFont systemFontOfSize:14];
    [addAddressButton addTarget:self action:@selector(addAdressAction) forControlEvents:UIControlEventTouchUpInside];
    UIView *bottonLine = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenWidth, 0.5)];
    bottonLine.backgroundColor = [NCColorUtil colorForCellSeparator];
    [addAddressButton addSubview:bottonLine];
    return addAddressButton;
}

#pragma mark - Event Response
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.addressList count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kASUSAddressListCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ASUSAddressListCell";
    ASUSAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSAddressListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.delegate = self;
    [cell setUpData:[self.addressList objectAtIndex:indexPath.row]];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (pushType == EXCHANGE) {
        if (self.chooseAddressBlock) {
            ASUSReceiveAddressModel *model = (ASUSReceiveAddressModel *)[self.addressList objectAtIndex:indexPath.row];
            self.chooseAddressBlock(model);
        }
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        ASUSAddAddressViewController *addAddressVC = [[ASUSAddAddressViewController alloc] init];
        addAddressVC.addressModel = [self.addressList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:addAddressVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
         ASUSReceiveAddressModel *model = (ASUSReceiveAddressModel *)[self.addressList objectAtIndex:indexPath.row];
        [self deleteDataWithAddressId:model.addressId];
//        [tableView beginUpdates];
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
//        [tableView endUpdates];
    }
}

#pragma mark - ASUSAddressListCellDelegate
- (void)addressDelete:(ASUSAddressListCell *)cell
{
    NSIndexPath *indexPath = [self.addressListTableView indexPathForCell:cell];
    ASUSAddAddressViewController *addAddressVC = [[ASUSAddAddressViewController alloc] init];
    addAddressVC.addressModel = [self.addressList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:addAddressVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark - UIbutton  Ations

- (void)addAdressAction {
    ASUSAddAddressViewController *addAddressVC = [[ASUSAddAddressViewController alloc] init];
    [self.navigationController pushViewController:addAddressVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)detailButtonAction:(ASUSAddressListCell *)cell {
    NSIndexPath *indexPath = [self.addressListTableView indexPathForCell:cell];
    ASUSAddAddressViewController *addAddressVC = [[ASUSAddAddressViewController alloc] init];
    addAddressVC.addressModel = [self.addressList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:addAddressVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

@end

//
//  ASUSInfoPlatformViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSInfoPlatformViewController.h"
#import "NCImageUtil.h"

@interface ASUSInfoPlatformViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *infoPlatformWebView;
@end

@implementation ASUSInfoPlatformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigationBar];
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"华硕商用资料平台";
    self.navigationItem.titleView = titleLabel;
}

- (void)setupView {
    self.infoPlatformWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [self.infoPlatformWebView setScalesPageToFit:YES];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://www.ahaokang.com/Product/index"]];
    [self.infoPlatformWebView loadRequest:req];
    self.infoPlatformWebView.delegate = self;
    [self.view addSubview:self.infoPlatformWebView];
}

#pragma mark - Event Response
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self showHud:HBHudShowTypeActivityOnly withText:@""];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self showHud:HBHudShowTypeCaptionOnly withText:@"网络不给力！"];
    [self hideHud];
}

@end

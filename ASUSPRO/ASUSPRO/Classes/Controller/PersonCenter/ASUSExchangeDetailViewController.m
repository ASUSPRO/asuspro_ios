//
//  ASUSExchangeDetailViewController.m
//  ASUSPRO
//
//  Created by May on 15-6-22.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeDetailViewController.h"
#import "JHAPISDK.h"
#import "JHOpenidSupplier.h"
#import "ASUSExpressCell.h"
#import "NCTimeUtil.h"
#import "UIImageView+AFNetworking.h"

@interface ASUSExchangeDetailViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIImageView *productImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *productNumLabel;
@property (nonatomic, strong) UILabel *pointsLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong)ASUSExchangeModel *exchangeModel;
@property (nonatomic, strong)UITableView *expressTableView;
@property (nonatomic, strong)UILabel *expressStatusLabel;
@property (nonatomic, strong)UILabel *expressNoLabel;
@property (nonatomic, strong)UILabel *expressCompanyLabel;
@property (nonatomic, strong)NSArray *expressList;
@end

@implementation ASUSExchangeDetailViewController

#pragma mark - lifeCycle

- (id)initWithModel:(ASUSExchangeModel *)model
{
    if (self = [super init]) {
        self.exchangeModel = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigationBar];
    
    [[JHOpenidSupplier shareSupplier] registerJuheAPIByOpenId:@"e90971b00452d8079c08c047208647ea"];
    ASUSExpressModel *express1 = [[ASUSExpressModel alloc] init];
    express1.expressCurrentAddress = @"福建省泉州市晋江市公司 派件人：路先生 派件中 派件员电话18050882881";
    express1.expressTime = @"2015年5月9日 上午8:32:10";
    
    ASUSExpressModel *express2 = [[ASUSExpressModel alloc] init];
    express2.expressCurrentAddress = @"福建省泉州市晋江市公司 已收入";
    express2.expressTime = @"2015年5月8日 上午8:32:10";
    
    ASUSExpressModel *express3 = [[ASUSExpressModel alloc] init];
    express3.expressCurrentAddress = @"泉州市转运公司 已发出";
    express3.expressTime = @"2015年5月8日 上午7:32:10";
    
    self.expressList = @[express1,express2,express3];
    
    [self setUpView];
    [self loadExpressData];
    // Do any additional setup after loading the view.
}

#pragma mark -- Private Method

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"查看物流";
    self.navigationItem.titleView = titleLabel;
}

- (UIView *)creatTableViewHeaderView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 240)];
    [self.view addSubview:topView];
    
    self.productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10
                                                                          , 10, 80, 100 - 20)];
    self.productImageView.image = [UIImage imageNamed:@"1.jpg"];
    [topView addSubview:self.productImageView];
    self.productImageView.backgroundColor = [UIColor redColor];
    
    float productNameLabelX = self.productImageView.frame.origin.x + self.productImageView.frame.size.width + 10;
    self.productNameLabel = [self createLabel:CGRectMake(productNameLabelX , 5, ScreenWidth - productNameLabelX - 80, 50) text:@"" font:[UIFont systemFontOfSize:14]];
    self.productNameLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    self.productNameLabel.numberOfLines = 2;
    [topView addSubview:self.productNameLabel];
    
    self.pointsLabel = [self createLabel:CGRectMake(ScreenWidth - 80, 10, 70, 20) text:@"" font:[UIFont systemFontOfSize:14]];
    self.pointsLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    self.pointsLabel.textAlignment = NSTextAlignmentRight;
    [topView addSubview:self.pointsLabel];
    
    self.productNumLabel = [self createLabel:CGRectMake(productNameLabelX , 70, 70, 20) text:@"X1" font:[UIFont systemFontOfSize:14]];
    self.productNumLabel.text = @"X1";
    self.productNumLabel.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [topView addSubview:self.productNumLabel];
    
    [self.productImageView setImageWithURL:[NSURL URLWithString:self.exchangeModel.giftPic] placeholderImage:nil];
    self.productNameLabel.text = self.exchangeModel.giftName;
    self.pointsLabel.text = [NSString stringWithFormat:@"-%ld分",(long)self.exchangeModel.userScore];
    self.productNumLabel.text = [NSString stringWithFormat:@"X%ld",(long)self.exchangeModel.num];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 99.5,ScreenWidth, 0.5)];
    lineView.backgroundColor = [NCColorUtil colorForCellSeparator];
    [topView addSubview:lineView];
    
    UILabel *dateTextLable =[[UILabel alloc] initWithFrame:CGRectMake(10, 120, ScreenWidth - 20, 20)];
    dateTextLable.font = [UIFont systemFontOfSize:15];
    dateTextLable.text = @"兑换日期:";
    dateTextLable.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [topView addSubview:dateTextLable];
    
    self.dateLabel = [self createLabel:CGRectMake(80, 120, 90, 20) text:@"2014-2-21" font:[UIFont systemFontOfSize:15]];
    self.dateLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    self.dateLabel.text = [NCTimeUtil convertTimeInterval:[NSString stringWithFormat:@"%lld",self.exchangeModel.createTime]];
    [topView addSubview:self.dateLabel];
    
    UILabel *expressStatus =[[UILabel alloc] initWithFrame:CGRectMake(10, 160, ScreenWidth - 20, 20)];
    expressStatus.font = [UIFont systemFontOfSize:15];
    expressStatus.text = @"物流状态:";
    expressStatus.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [topView addSubview:expressStatus];
    
    self.expressStatusLabel =[[UILabel alloc] initWithFrame:CGRectMake(80, 160, ScreenWidth - 20, 20)];
    self.expressStatusLabel.font = [UIFont systemFontOfSize:16];
    self.expressStatusLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    self.expressStatusLabel.text = @"配送中";
    [topView addSubview:self.expressStatusLabel];
    
    UILabel *expressNoText =[[UILabel alloc] initWithFrame:CGRectMake(10, 200, 80, 20)];
    expressNoText.font = [UIFont systemFontOfSize:15];
    expressNoText.text = @"运单号:";
    expressNoText.textColor = [NCColorUtil colorFromHexString:@"#606366"];
    [topView addSubview:expressNoText];
    
    self.expressNoLabel =[[UILabel alloc] initWithFrame:CGRectMake(65, 200, ScreenWidth - 160, 20)];
    self.expressNoLabel.font = [UIFont systemFontOfSize:15];
    self.expressNoLabel.text = self.exchangeModel.expressNo;
    self.expressNoLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.expressNoLabel];
    
    self.expressCompanyLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth - 160, 200, 150, 20)];
    self.expressCompanyLabel.text = self.exchangeModel.express;
    self.expressCompanyLabel.font  = [UIFont systemFontOfSize:14];
    self.expressCompanyLabel.textAlignment = NSTextAlignmentRight;
    self.expressCompanyLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.expressCompanyLabel];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(10, 240 - 0.5,ScreenWidth, 0.5)];
    bottomLineView.backgroundColor = [NCColorUtil colorForCellSeparator];
    [topView addSubview:bottomLineView];
    
    return topView;
}

- (UILabel *)createLabel:(CGRect) frame text:(NSString *)text font:(UIFont *)font{
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:frame];
    tempLabel.text = text;
    tempLabel.font = font;
    tempLabel.textColor = [UIColor blackColor];
    return tempLabel;
}


- (void)setUpView {
    self.expressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight)];
    self.expressTableView.delegate = self;
    self.expressTableView.dataSource = self;
    self.expressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.expressTableView.tableHeaderView = [self creatTableViewHeaderView];
    [self.view addSubview:self.expressTableView];
}



#pragma mark -- loadRequestData
- (void)loadExpressData
{
    NSLog(@"nihao:%@",self.exchangeModel.express);
    NSLog(@"nihao:%@",self.exchangeModel.expressNo);
    NSString *path = @"http://v.juhe.cn/exp/index";
    NSString *api_id = @"43";
    NSString *method = @"POST";
    NSDictionary *param = @{@"key":@"e90971b00452d8079c08c047208647ea",@"com":self.exchangeModel.express,@"no":self.exchangeModel.expressNo,@"dtype":@"json"};
    JHAPISDK *juheapi = [JHAPISDK shareJHAPISDK];
    
    [juheapi executeWorkWithAPI:path
                          APIID:api_id
                     Parameters:param
                         Method:method
                        Success:^(id responseObject){
                            if ([[param objectForKey:@"dtype"] isEqualToString:@"xml"]) {
                                NSLog(@"***xml*** \n %@", responseObject);
                            }else{
                                int error_code = [[responseObject objectForKey:@"error_code"] intValue];
                                if (!error_code) {
                                    NSLog(@" %@", responseObject);
                                }else{
                                    NSLog(@" %@", responseObject);
                                }
                            }
                        } Failure:^(NSError *error) {
                            NSLog(@"error:   %@",error.description);
                        }];
}

#pragma mark - Action Method
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.expressList.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ASUSExpressCell getASUSExpessCellHeight:[self.expressList objectAtIndex:indexPath.row]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ASUSExpressCell";
    ASUSExpressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ASUSExpressCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.expressModel = [self.expressList objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {
        [cell refreshFirstTimeLineFrame];
    } else if (indexPath.row == self.expressList.count - 1) {
        [cell refreshLastTimeLineFrame];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

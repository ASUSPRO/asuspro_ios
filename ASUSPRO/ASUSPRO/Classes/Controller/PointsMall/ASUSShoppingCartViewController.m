//
//  ASUSShoppingCartViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSShoppingCartViewController.h"
#import "ASUSShopCartCell.h"
#import "ASUSExchangeViewController.h"
#import "BAAuthenticate.h"
#import "ASUSGiftManager.h"
#import "NCImageUtil.h"

@interface ASUSShoppingCartViewController ()<UITableViewDataSource, UITableViewDelegate, ASUSShopCartCellDelegate>
@property (nonatomic, strong) UITableView *shoppingCartTableView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *totalPointsLabel;
@property (nonatomic, strong) NSArray *giftModels;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) BOOL isCheckAll;
@property (nonatomic, strong) UIButton *exchangeButton;
@end

@implementation ASUSShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isCheckAll = YES;
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    self.view.backgroundColor = [NCColorUtil backGroundColorForView];
    [self setupTableView];
    [self setupBottomView];
    [self setupData];
}

#pragma mark - setup data

- (void)setupData {
    self.giftModels = [ASUSGiftManager fetchShoppingCartGiftList];
    self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];;
    [self.shoppingCartTableView reloadData];
}

- (long)getTotalPointsNum {
    long totalPointsNum = 0;
    for (ASUSGiftModel *giftModel in self.giftModels) {
        if (giftModel.isSelected) {
            if([[BAAuthenticate sharedClient].userType integerValue] == 1){
                totalPointsNum = totalPointsNum + (giftModel.innerScore * giftModel.exchangeNum);
            } else {
                totalPointsNum = totalPointsNum + (giftModel.outerScore * giftModel.exchangeNum);
            }
        }
    }
    return totalPointsNum;
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"购物车";
    self.navigationItem.titleView = titleLabel;
    
//    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightButton.frame = CGRectMake(0, 0, 50, 44);
//    rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
//    [rightButton setTitle:@"编辑" forState:UIControlStateNormal];
//    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [rightButton addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
//    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(editAction)];
//    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

- (void)setupTableView {
    self.shoppingCartTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-114)];
    self.shoppingCartTableView.delegate = self;
    self.shoppingCartTableView.dataSource = self;
    self.shoppingCartTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.shoppingCartTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shoppingCartTableView];
}

- (void)setupBottomView {
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-114, ScreenWidth, 49)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bottomView];
    [self.view bringSubviewToFront:self.bottomView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0.5)];
    line.backgroundColor = [NCColorUtil colorForSeparatorLine];
    [self.bottomView addSubview:line];
    
    UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectedButton addTarget:self action:@selector(checkedAllButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    selectedButton.frame = CGRectMake(10, (49 - 15)/2, 15, 15);
    selectedButton.tag = 1000;
//    selectedButton.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
//    selectedButton.layer.borderWidth = 1.0;
    selectedButton.backgroundColor = RGBCOLOR(254, 119, 105);
    [selectedButton setImage:[NCImageUtil imageFromText:@"\ue613" fontSize:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    selectedButton.layer.borderWidth = 0;
    selectedButton.layer.cornerRadius = 15/2;
    
    [self.bottomView addSubview:selectedButton];
    
    UILabel *selectedAllLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 60, 49)];
    selectedAllLabel.text = @"全选";
    selectedAllLabel.backgroundColor = [UIColor clearColor];
    selectedAllLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    selectedAllLabel.font = [UIFont systemFontOfSize:15];
    [self.bottomView addSubview:selectedAllLabel];
    
    self.totalPointsLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, ScreenWidth - 220, 49)];
    self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];
    self.totalPointsLabel.font = [UIFont systemFontOfSize:13];
    self.totalPointsLabel.textAlignment = NSTextAlignmentRight;
    self.totalPointsLabel.backgroundColor = [UIColor clearColor];
    self.totalPointsLabel.textColor = [NCColorUtil colorFromHexString:@"#ff415b"];
    [self.bottomView addSubview:self.totalPointsLabel];
    
    self.exchangeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.exchangeButton.frame = CGRectMake(ScreenWidth - 120, (49 - 34)/2, 110, 34);
    self.exchangeButton.backgroundColor = RGBCOLOR(254, 119, 105);
    self.exchangeButton.titleLabel.font = [UIFont systemFontOfSize:14];
    self.exchangeButton.layer.cornerRadius = 2;
    [self.exchangeButton addTarget:self action:@selector(exchangeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.exchangeButton setTitle:@"兑换" forState:UIControlStateNormal];
    [self.bottomView addSubview:self.exchangeButton];
}

- (NSMutableAttributedString *)getTotalPointsLabelText {
    NSMutableAttributedString *pointsStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计：%ld分",(long)[self getTotalPointsNum]]];
    self.totalPointsLabel.text = [NSString stringWithFormat:@"合计：%ld分",(long)[self getTotalPointsNum]];
    [pointsStr addAttribute:NSForegroundColorAttributeName value:(id)[NCColorUtil colorFromHexString:@"#84898f"] range:NSMakeRange(0, 3)];
    [pointsStr addAttribute:NSForegroundColorAttributeName value:(id)[NCColorUtil colorFromHexString:@"#fd7864"] range:NSMakeRange(3, self.totalPointsLabel.text.length - 3)];
    return pointsStr;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.giftModels.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kASUSShopCartCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ASUSBaseCell *cell = nil;
    static NSString *identifier = @"ASUSShopCartCell";
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ASUSShopCartCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    ((ASUSShopCartCell *)cell).giftModel = [self.giftModels objectAtIndex:indexPath.row];
    ((ASUSShopCartCell *)cell).delegate = self;
    [cell refreshUI];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray *gifts = [NSArray arrayWithObjects:[self.giftModels objectAtIndex:indexPath.row], nil];
        [ASUSGiftManager deleteShoppingCartGiftList:gifts];
        [self setupData];
    }
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)exchangeButtonClicked {
    if(self.isEdit) {
        [ASUSGiftManager deleteShoppingCartGiftList:self.giftModels];
        [self setupData];
        if (self.giftModels.count == 0) {
            self.exchangeButton.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
            self.exchangeButton.enabled = NO;
        }
    } else {
        ASUSExchangeViewController *exchangeVC = [[ASUSExchangeViewController alloc] initWithModels:self.giftModels];
        [self.navigationController pushViewController:exchangeVC animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}

- (void)editAction:(UIButton *)rightButton {
    self.isEdit = !self.isEdit;
    for (ASUSGiftModel *giftModel in self.giftModels) {
            giftModel.isSelected = NO;
    }
    if (self.isEdit) {
        [rightButton setTitle:@"完成" forState:UIControlStateNormal];
        [self.exchangeButton setTitle:@"删除" forState:UIControlStateNormal];
        self.totalPointsLabel.hidden = YES;
    } else {
        [rightButton setTitle:@"编辑" forState:UIControlStateNormal];
        [self.exchangeButton setTitle:@"兑换" forState:UIControlStateNormal];
        self.totalPointsLabel.hidden = NO;
        self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];
    }
    self.exchangeButton.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    UIButton *selectedAll = (UIButton *)[self.bottomView viewWithTag:1000];
    selectedAll.backgroundColor = [UIColor whiteColor];
    for (UITableViewCell *cell in self.shoppingCartTableView.visibleCells) {
        if ([cell isKindOfClass:[ASUSShopCartCell class]]) {
            ASUSShopCartCell *theCell = (ASUSShopCartCell *)cell;
            [theCell changeEditState:self.isEdit];
            [theCell setIsSelected:NO];
        }
    }
}

- (void)checkedAllButtonClicked:(UIButton *)button  {
    self.isCheckAll = !self.isCheckAll;
    if (self.isCheckAll) {
        button.backgroundColor = RGBCOLOR(254, 119, 105);
        [button setImage:[NCImageUtil imageFromText:@"\ue613" fontSize:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        button.layer.borderWidth = 0;
        self.exchangeButton.enabled = YES;
        self.exchangeButton.backgroundColor = RGBCOLOR(254, 119, 105);
    } else {
        button.backgroundColor = [UIColor whiteColor];
        button.layer.borderWidth = 1;
        button.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
         self.exchangeButton.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    }
    for (ASUSGiftModel *giftModel in self.giftModels) {
        if (self.isCheckAll) {
            giftModel.isSelected = YES;
        } else {
            giftModel.isSelected = NO;
        }
    }
    for (ASUSShopCartCell *cell in [self.shoppingCartTableView visibleCells]) {
        if (self.isCheckAll) {
            cell.isSelected = YES;
        } else {
            cell.isSelected = NO;
        }
        self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];
    }
}

#pragma mark - ASUSShopCartCellDelegate

- (void)giftModelNumChanged:(ASUSShopCartCell *)shopCartCell {
    [self setupData];
    self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];
}

- (void)shopCartCellSelectedStatusChanged:(ASUSShopCartCell *)shopCartCell {
    NSInteger seletedNum = 0;
    for (ASUSGiftModel *giftModel in self.giftModels) {
        if (giftModel.gifId == shopCartCell.giftModel.gifId) {
            giftModel.isSelected = shopCartCell.giftModel.isSelected;
        }
        if (giftModel.isSelected) {
            seletedNum ++;
        }
    }
    self.totalPointsLabel.attributedText = [self getTotalPointsLabelText];
    if (seletedNum == 0) {
        self.exchangeButton.backgroundColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
        self.exchangeButton.enabled = NO;
    } else  {
        self.exchangeButton.backgroundColor = RGBCOLOR(254, 119, 105);
        self.exchangeButton.enabled = YES;
    }
}

@end

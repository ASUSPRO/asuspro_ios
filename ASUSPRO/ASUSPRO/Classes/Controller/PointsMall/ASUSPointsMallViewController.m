//
//  ASUSPointsMallViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSPointsMallViewController.h"
#import "ASUSProductMenuCell.h"
#import "ASUSProductDetailViewController.h"
#import "NCImageUtil.h"
#import "ASUSShoppingCartViewController.h"
#import "ASUSProductListViewController.h"
#import "ASUSUIBadgeView.h"
#import "ASUSPointsMallListCell.h"
#import "ASUSShopModel.h"
#import "ASUSGiftModel.h"
#import "ASUSProductMenuModel.h"
#import "ASUSBlurView.h"
#import "ASUSGiftManager.h"

@interface ASUSPointsMallViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    UISearchDisplayController *searchDisplayController;
}
@property (nonatomic, strong) UITableView *productListTableView;
@property (nonatomic, strong) NSMutableArray *productList;
@property (nonatomic, strong) ASUSBlurView *productMenuBackgroundView;
@property (nonatomic, strong) UITableView *productMenuTableView;
@property (nonatomic, strong) NSMutableArray *productMenuList;
@property (nonatomic, assign) BOOL menuIsShow;
@property (nonatomic, strong) UISearchBar *productSearchBar;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UIButton *shoppingCartButton;
@property (nonatomic, strong) ASUSUIBadgeView *shoppingCartBadgeView;
@property (nonatomic, strong) NSMutableArray *productCategoryList;
@property(strong,nonatomic) NSMutableArray *allMenuModelList; //保存全部数据的数组
@property(strong,nonatomic) NSArray *displayModelList;   //保存要显示在界面上的数据的数组
@property (nonatomic, strong) UITextField *searchTextField;
@end

@implementation ASUSPointsMallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self configNavigationBar];
    
    self.productMenuList = [[NSMutableArray alloc] initWithCapacity:0];
    self.productList = [[NSMutableArray alloc] initWithCapacity:0];
    self.productCategoryList = [[NSMutableArray alloc] initWithCapacity:0];

    [self setupProductMenuList];
    [self setupProductList];
    [self setupTableView];
    [self setupMenuView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.shoppingCartBadgeView setBadgeNumber:[ASUSGiftManager numOfGiftModels]];
}

#pragma mark - views

- (void)configNavigationBar {
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"积分商城";
    self.navigationItem.titleView = titleLabel;
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    self.shoppingCartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.shoppingCartButton.frame = CGRectMake(0, 2, 28, 28);
    [self.shoppingCartButton addTarget:self action:@selector(shopCartButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.shoppingCartButton setBackgroundImage:[NCImageUtil imageFromText:@"\ue601" fontSize:25 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    self.shoppingCartBadgeView = [[ASUSUIBadgeView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    [self.shoppingCartBadgeView setBadgeNumber:[ASUSGiftManager numOfGiftModels]];
    self.shoppingCartBadgeView.hidden = YES;
    self.shoppingCartBadgeView.layer.cornerRadius =7;
    self.shoppingCartBadgeView.layer.masksToBounds = YES;
    [self.shoppingCartButton addSubview:self.shoppingCartBadgeView];
    
    UIBarButtonItem *shoppingBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.shoppingCartButton];
    self.navigationItem.rightBarButtonItem = shoppingBarItem;
    
    UIButton *productMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    productMenuButton.frame = CGRectMake(0, 2, 30, 30);
    [productMenuButton addTarget:self action:@selector(productMenuButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [productMenuButton setBackgroundImage:[NCImageUtil imageFromText:@"\ue615" fontSize:25 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    UIBarButtonItem *productBarItem = [[UIBarButtonItem alloc] initWithCustomView:productMenuButton];
    self.navigationItem.rightBarButtonItem = productBarItem;
    self.navigationItem.rightBarButtonItems = @[productBarItem,shoppingBarItem];
}

- (UIView *)createTableViewHeaderView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth,40)];
    headerView.userInteractionEnabled = YES;
    
    UIImageView *searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 20, 20)];
    searchImageView.image = [NCImageUtil imageFromText:@"\ue60a" fontSize:20 color:[NCColorUtil colorForCellSeparator]];
    [headerView addSubview:searchImageView];
    
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, ScreenWidth - 90, 40)];
    self.searchTextField.placeholder = @"please input keywords";
    self.searchTextField.font = [UIFont systemFontOfSize:14.0];
    self.searchTextField.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    self.searchTextField.delegate = self;
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    [headerView addSubview:self.searchTextField];
    
    UIButton *recentlySearchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    recentlySearchButton.frame = CGRectMake(ScreenWidth - 40, 5, 40, 30);
    recentlySearchButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [recentlySearchButton setTitle:@"搜索" forState:UIControlStateNormal];
    [recentlySearchButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [recentlySearchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:recentlySearchButton];
    
//    self.productSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-40,40)];
//    self.productSearchBar.delegate = self;
//    self.productSearchBar.backgroundColor = [UIColor clearColor];
//    self.productSearchBar.backgroundImage = [UIImage imageNamed:@"searchBarbg"];
//    [self.productSearchBar setPlaceholder:@"please input keywords"];
//    [self.productSearchBar setValue:[NCColorUtil colorFromHexString:@"#b5bdc5"] forKeyPath:@"_placeholderLabel.textColor"];
//    
//    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.productSearchBar contentsController:self];
//    searchDisplayController.active = NO;
//    searchDisplayController.searchResultsDataSource = self;
//    searchDisplayController.searchResultsDelegate = self;
//    [headerView addSubview:self.productSearchBar];
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, ScreenWidth, 0.5)];
    horizontalLine.backgroundColor = [NCColorUtil colorForCellSeparator];
    [headerView addSubview:horizontalLine];
    return headerView;
    
}


- (void)setupTableView {
    self.productListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
    self.productListTableView.delegate = self;
    self.productListTableView.scrollEnabled = YES;
    self.productListTableView.dataSource = self;
    self.productListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.productListTableView];
}

- (void)setupMenuView {
    self.productMenuBackgroundView = [[ASUSBlurView alloc] initWithFrame:CGRectMake(ScreenWidth, 0, ScreenWidth, ScreenHeight)];
    [self.productMenuBackgroundView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.productMenuBackgroundView];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 215, ScreenHeight)];
    leftView.backgroundColor = [UIColor clearColor];
    [self.productMenuBackgroundView addSubview:leftView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productMenuBackgroundViewClicked:)];
    [leftView addGestureRecognizer:tapGesture];

    self.productMenuTableView = [[UITableView alloc] initWithFrame:CGRectMake(ScreenWidth - 215, 0, 215, ScreenHeight)];
    self.productMenuTableView.delegate = self;
    self.productMenuTableView.scrollEnabled = YES;
    self.productMenuTableView.dataSource = self;
    self.productMenuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.productMenuTableView.tableHeaderView = [self createTableViewHeaderView];
    [self.productMenuBackgroundView addSubview:self.productMenuTableView];
}

- (void)showMenuView:(BOOL)isShow {
    if (isShow) {
        [self changeCellExpandedStatus:NO];
        [UIView animateWithDuration:0.3 animations:^{
            self.productMenuBackgroundView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            self.productMenuBackgroundView.frame = CGRectMake(ScreenWidth, 0, ScreenWidth, ScreenHeight);
        }];
    }
}

#pragma mark - setup Data

- (void)setupProductList {
    [ASUSShopModel fetchShopIndexListWithCompletion:^(NSError *error, NSArray *list) {
        [self.productList addObjectsFromArray:list];
        [self.productListTableView reloadData];
    }];
}

//添加演示数据
-(void)setupProductMenuList{
    ASUSProductMenuModel *node0 = [[ASUSProductMenuModel alloc]init];
    node0.nodeLevel = 0;//根层cell
    node0.type = 1;//type 1的cell
    node0.sonNodes = nil;
    node0.isExpanded = FALSE;//关闭状态
    node0.categoryName = @"本月新品";
    
    ASUSProductMenuModel *node1 = [[ASUSProductMenuModel alloc]init];
    node1.nodeLevel = 0;
    node1.type = 1;
    node1.sonNodes = nil;
    node1.isExpanded = FALSE;
    node1.categoryName = @"华硕产品";
    
    __block ASUSProductMenuModel *node2 = [[ASUSProductMenuModel alloc]init];
    node2.nodeLevel = 0;
    node2.type = 1;
    node2.sonNodes = nil;
    node2.isExpanded = FALSE;
    node2.categoryName = @"数码产品";
    
//    ASUSProductMenuModel *node3 = [[ASUSProductMenuModel alloc]init];
//    node3.nodeLevel = 1;//第一层节点
//    node3.type = 1;//type 2的cell
//    node3.sonNodes = nil;
//    node3.isExpanded = FALSE;
//    node3.categoryName = @"商务箱包";
//    
//    ASUSProductMenuModel *node4 = [[ASUSProductMenuModel alloc]init];
//    node4.nodeLevel = 1;
//    node4.type = 1;
//    node4.sonNodes = nil;
//    node4.isExpanded = FALSE;
//    node4.categoryName = @"生活用品";
//    
//    ASUSProductMenuModel *node5 = [[ASUSProductMenuModel alloc]init];
//    node5.nodeLevel = 1;
//    node5.type = 1;
//    node5.sonNodes = nil;
//    node5.isExpanded = FALSE;
//    node5.categoryName = @"电子兑换券";
    
    [ASUSProductMenuModel fetchGiftCategoryWithCompletion:^(NSError *error, NSArray *list) {
        for (ASUSProductMenuModel *menu in list) {
            menu.nodeLevel = 1;
            menu.type = 2;//type 2的cell
            menu.sonNodes = nil;
            menu.isExpanded = FALSE;
        }
         node2.sonNodes = [NSMutableArray arrayWithArray:list];
        _allMenuModelList = [NSMutableArray arrayWithObjects:node0,node1,node2, nil];
        [self reloadDataForDisplayArray];//初始化将要显示的数据
    }];
}

/*---------------------------------------
 为不同类型cell填充数据
 --------------------------------------- */
-(void) loadDataForTreeViewCell:(UITableViewCell*)cell with:(ASUSProductMenuModel*) node{
    if(node.type == 0){
        ((ASUSProductMenuCell*)cell).menuNameLabel.text = node.categoryName;
        [((ASUSProductMenuCell*)cell) refreshUI];
        
    }
    
    else if(node.type == 1){
        ((ASUSProductMenuCell*)cell).menuNameLabel.text = node.categoryName;
        [((ASUSProductMenuCell*)cell) refreshUI];
    }
}

#pragma mark - UITapGestureRecognizer

- (void)productMenuBackgroundViewClicked:(UITapGestureRecognizer *)tapGesture {
    self.menuIsShow = !self.menuIsShow;
    [self showMenuView:NO];
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchAction
{
    if (self.searchTextField.text.length > 0) {
        ASUSProductListViewController *productListVC = [[ASUSProductListViewController alloc] initWithKeyword:self.searchTextField.text]
        ;
        productListVC.productListViewControllerType = ASUSProductListViewControllerTypeSearch;
        [self.navigationController pushViewController:productListVC animated:YES];
        [self.searchTextField resignFirstResponder];
        self.searchTextField.text = @"";
    }else {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入关键字"];
        [self hideHudAfter:2.0];
    }
}

- (void)productMenuButtonAction {
    self.menuIsShow = !self.menuIsShow;
    [self showMenuView:self.menuIsShow];
}

- (void)shopCartButtonAction {
    ASUSShoppingCartViewController *shoppingCarVC = [[ASUSShoppingCartViewController alloc] init];
    [self.navigationController pushViewController:shoppingCarVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark - private Method

/*---------------------------------------
 初始化将要显示的cell的数据
 --------------------------------------- */
-(void) reloadDataForDisplayArray{
    NSMutableArray *tmp = [[NSMutableArray alloc]init];
    for (ASUSProductMenuModel *node in _allMenuModelList) {
        [tmp addObject:node];
        if(node.isExpanded){
            for(ASUSProductMenuModel *node2 in node.sonNodes){
                [tmp addObject:node2];
                if(node2.isExpanded){
                    for(ASUSProductMenuModel *node3 in node2.sonNodes){
                        [tmp addObject:node3];
                    }
                }
            }
        }
    }
    self.displayModelList = [NSArray arrayWithArray:tmp];
    [self.productMenuTableView reloadData];
}

/*---------------------------------------
 修改cell的状态(关闭或打开)
 --------------------------------------- */
-(void)reloadDataForDisplayArrayChangeAt:(NSInteger)row{
    NSMutableArray *tmp = [[NSMutableArray alloc]init];
    NSInteger cnt = 0;
    for (ASUSProductMenuModel *node in _allMenuModelList) {
        [tmp addObject:node];
        if(cnt == row){
            node.isExpanded = !node.isExpanded;
        }
        ++cnt;
        if(node.isExpanded){
            for(ASUSProductMenuModel *node2 in node.sonNodes){
                [tmp addObject:node2];
                if(cnt == row){
                    node2.isExpanded = !node2.isExpanded;
                }
                ++cnt;
                if(node2.isExpanded){
                    for(ASUSProductMenuModel *node3 in node2.sonNodes){
                        [tmp addObject:node3];
                        ++cnt;
                    }
                }
            }
        }
    }
    self.displayModelList = [NSArray arrayWithArray:tmp];
    [self.productMenuTableView reloadData];
}

- (void)changeCellExpandedStatus:(BOOL)isExpanded {
    NSMutableArray *tmp = [[NSMutableArray alloc]init];
    NSInteger cnt = 0;
    for (ASUSProductMenuModel *node in _allMenuModelList) {
        [tmp addObject:node];
        node.isExpanded = isExpanded;
        ++cnt;
        if(node.isExpanded){
            for(ASUSProductMenuModel *node2 in node.sonNodes){
                [tmp addObject:node2];
                node.isExpanded = isExpanded;
                ++cnt;
                if(node2.isExpanded){
                    for(ASUSProductMenuModel *node3 in node2.sonNodes){
                        [tmp addObject:node3];
                        ++cnt;
                    }
                }
            }
        }
    }
    self.displayModelList = [NSArray arrayWithArray:tmp];
    [self.productMenuTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.searchResults.count;
    } else if (tableView == self.productListTableView ){
        return self.productList.count;
    } else {
        return self.displayModelList.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 67;
    } else  if (tableView == self.productListTableView){
        return 200;
    } else {
        return 40;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ASUSBaseCell *cell = nil;
    if (tableView == self.productListTableView) {
        static NSString *identifier = @"ASUSPointsMallListCell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[ASUSPointsMallListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.bottomLineView.hidden = YES;
        ((ASUSPointsMallListCell *)cell).shopModel = [self.productList objectAtIndex:indexPath.row];
    } else {
        static NSString *identifier = @"menuCell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[ASUSProductMenuCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.bottomLineView.hidden = YES;
        ASUSProductMenuModel *menuModel = [self.displayModelList objectAtIndex:indexPath.row];
         ((ASUSProductMenuCell *)cell).menuModel = menuModel;
        [ ((ASUSProductMenuCell *)cell) setNeedsDisplay]; //重新描绘cell
//        ((ASUSProductMenuCell *)cell).menuModel = [self.productMenuList objectAtIndex:indexPath.row];
    }
    [cell refreshUI];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.productMenuTableView == tableView) {
        ASUSProductMenuModel *node = [self.displayModelList objectAtIndex:indexPath.row];
        if (node.sonNodes == nil) {
            [self productMenuButtonAction];
        }
        [self reloadDataForDisplayArrayChangeAt:indexPath.row];//修改cell的状态(关闭或打开)
        if(node.type == 2){
            //处理叶子节点选中，此处需要自定义
        }
        switch (indexPath.row) {
            case 0: {
                ASUSProductListViewController *productListVC = [[ASUSProductListViewController alloc] init];
                productListVC.productListViewControllerType = ASUSProductListViewControllerTypeNewProduct;
                [self.navigationController pushViewController:productListVC animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }   
                break;
            case 1: {
                ASUSProductListViewController *productListVC = [[ASUSProductListViewController alloc] init];
                productListVC.productListViewControllerType = ASUSProductListViewControllerTypeASUSProduct;
                [self.navigationController pushViewController:productListVC animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
                case 2:
            {
                
            }
                break;
            default: {
                ASUSProductListViewController *productListVC = [[ASUSProductListViewController alloc] init];
                productListVC.productListViewControllerType = ASUSProductListViewControllerTypeOther;
                productListVC.menuModel = [self.displayModelList objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:productListVC animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
        }
    }else {
        ASUSShopModel *model = (ASUSShopModel *)[self.productList objectAtIndex:indexPath.row];
        ASUSProductDetailViewController *dvc = [[ASUSProductDetailViewController alloc] initWithGiftId:[model.shopId integerValue]];
        [self.navigationController pushViewController:dvc animated:YES];
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchResults = [[NSMutableArray alloc] init];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.productMenuTableView.tableHeaderView.frame = CGRectMake(0, 0, ScreenWidth, 40);
    self.productSearchBar.frame =  CGRectMake(0, 0, ScreenWidth, 40);
    [self.productSearchBar setNeedsDisplay];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    self.productMenuTableView.tableHeaderView.frame = CGRectMake(0, 0, 250, 40);
    self.productSearchBar.frame =  CGRectMake(0, 0, 250, 40);
    [self.productSearchBar setNeedsDisplay];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.searchTextField.text.length > 0) {
        ASUSProductListViewController *productListVC = [[ASUSProductListViewController alloc] initWithKeyword:self.searchTextField.text]
        ;
         productListVC.productListViewControllerType = ASUSProductListViewControllerTypeSearch;
        [self.navigationController pushViewController:productListVC animated:YES];
        [self.searchTextField resignFirstResponder];
        self.searchTextField.text = @"";
    }else {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入关键字"];
        [self hideHudAfter:2.0];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    return YES;
}


@end

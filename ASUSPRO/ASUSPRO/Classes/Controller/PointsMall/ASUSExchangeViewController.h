//
//  ASUSExchangeEViewController.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSGiftModel.h"

@interface ASUSExchangeViewController : ASUSBaseViewController
- (id)initWithModels:(NSArray *)models;

@end

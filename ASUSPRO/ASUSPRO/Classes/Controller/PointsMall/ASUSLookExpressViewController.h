//
//  ASUSLookExpressViewController.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/5.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSExchangeModel.h"

@interface ASUSLookExpressViewController : ASUSBaseViewController

- (id)initWithModel:(ASUSExchangeModel *)model;

@end

//
//  ASUSProductListViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "ASUSProductMenuModel.h"

typedef enum {
    ASUSProductListViewControllerTypeNewProduct,
    ASUSProductListViewControllerTypeASUSProduct,
    ASUSProductListViewControllerTypeOther,
    ASUSProductListViewControllerTypeSearch
}ASUSProductListViewControllerType;
@interface ASUSProductListViewController : ASUSBaseViewController
@property (nonatomic, assign) ASUSProductListViewControllerType productListViewControllerType;
@property (nonatomic, strong) ASUSProductMenuModel *menuModel;

- (id)initWithKeyword:(NSString *)keyword;

@end

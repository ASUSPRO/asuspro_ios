//
//  ASUSProductDetailViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductDetailViewController.h"
#import "ASUSSegmentedControl.h"
#import "ASUSGiftModel.h"

#import "ASUSShoppingCartViewController.h"
#import "ASUSExchangeViewController.h"

#import "BAAuthenticate.h"

#import "NSString+Utils.h"

#import "ASUSBlurView.h"

#import "ASUSGiftManager.h"

@interface ASUSProductDetailViewController ()<UIWebViewDelegate>
{
    NSInteger giftID;
    BOOL isLoadingFinished;
}
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *needPointsLabel;
@property (nonatomic, strong) UIButton *detailButton;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *totalPointsLabel;
@property (nonatomic, strong) ASUSBlurView *blurView;
@property (nonatomic, strong) UILabel *detailInfoLabel;

@property (nonatomic, strong) ASUSGiftModel *giftModel;
@property (nonatomic, strong) UIWebView *giftDetailWebView;

@end

@implementation ASUSProductDetailViewController

#pragma mark - lifecycle
-(id)initWithGiftId:(NSInteger)giftId
{
    if (self = [super init]) {
        giftID = giftId;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    [self setUpWebView];
    [self setupTopViews];
    [self setupBottonViews];
    [self setupData];
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"商品详情";
    self.navigationItem.titleView = titleLabel;
}

- (void)setUpWebView
{
    //html是否加载完成
    isLoadingFinished = NO;
    self.giftDetailWebView = [[UIWebView alloc] initWithFrame:CGRectMake(-3, -3, ScreenWidth+6, ScreenHeight+6 - 50 - kNavigationBarHeight)];
    [self.giftDetailWebView setScalesPageToFit:YES];
    self.giftDetailWebView.hidden = YES;
    self.giftDetailWebView.delegate = self;
    [self.view addSubview:self.giftDetailWebView];
}

- (void)setupTopViews {
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    self.topView.backgroundColor = [UIColor colorWithRed:54.0/255 green:58.0/255 blue:63.0/255 alpha:0.5];
    
    UILabel *pointsText = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 70, 40)];
    pointsText.backgroundColor = [UIColor clearColor];
    pointsText.textColor = [UIColor whiteColor];
    pointsText.text = @"所需积分：";
    pointsText.font = [UIFont systemFontOfSize:14];
    
    [self.topView addSubview:pointsText];
    
    self.needPointsLabel = [[UILabel alloc]initWithFrame:CGRectMake(pointsText.frame.size.width + 5, 0, 150, 40)];
    self.needPointsLabel.textColor = [UIColor whiteColor];
    self.needPointsLabel.font = [UIFont systemFontOfSize:14];
    self.needPointsLabel.backgroundColor = [UIColor clearColor];
    [self.topView addSubview:self.needPointsLabel];
    
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.detailButton.frame = CGRectMake(ScreenWidth - 70, 0, 60, 40);
    [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.detailButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.detailButton setTitle:@"详细规格" forState:UIControlStateNormal];
    [self.detailButton addTarget:self action:@selector(detailButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.detailButton];
    
    [self.view addSubview:self.topView];
    [self.view bringSubviewToFront:self.topView];
}

- (void)setupBottonViews {
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight -kNavigationBarHeight - 50, ScreenWidth, 50)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.layer.borderColor = [NCColorUtil colorForSeparatorLine].CGColor;
    self.bottomView.layer.borderWidth = 0.5;
    
    UIButton *dollarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dollarButton.frame = CGRectMake(10, (50 - 25)/2, 25, 25);
    [dollarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dollarButton setImage:[NCImageUtil imageFromText:@"\ue61f" fontSize:20 color:[NCColorUtil colorFromHexString:@"#84898f"]] forState:UIControlStateNormal];
    dollarButton.backgroundColor = [UIColor whiteColor];
    [self.bottomView addSubview:dollarButton];
    
    self.totalPointsLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 0, 120, 50)];
    self.totalPointsLabel.backgroundColor = [UIColor clearColor];
    self.totalPointsLabel.textColor = [NCColorUtil colorFromHexString:@"#ff415b"];
    self.totalPointsLabel.font = [UIFont systemFontOfSize:14];
    self.totalPointsLabel.text = [BAAuthenticate sharedClient].score;
    [self.bottomView addSubview:self.totalPointsLabel];
    
    [self createSegmentedControl];
    
    [self.view addSubview:self.bottomView];
}

- (void)createSegmentedControl {
    UIView *segementedControlBg = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth - 201, (50 - 32)/2, 182, 32)];
    segementedControlBg.layer.borderColor = [NCColorUtil colorForTitleOrNickFont].CGColor;
    segementedControlBg.layer.borderWidth = 1.0;
    [self.bottomView addSubview:segementedControlBg];
    
    ASUSSegmentedControl *segementedControl = [[ASUSSegmentedControl alloc]
                                               initWithFrame:CGRectMake(ScreenWidth - 200, (50 - 30)/2,180, 30) items:@[               @{@"text":@"添加",},
                                                                                                                                       @{@"text":@"兑换"},
                                                                                                                                       
                                                                                                                                       ]
                                               iconPosition:IconPositionRight andSelectionBlock:^(NSUInteger segmentIndex) {
                                                   if (segmentIndex == 0) {
                                                       
                                                       self.giftModel.isSelected = YES;     ASUSGiftModel *giftModel = [ASUSGiftManager fetchShoppingCartGiftListGiftId:self.giftModel.gifId];
                                                       if (giftModel) {
                                                           self.giftModel.exchangeNum = giftModel.exchangeNum + 1;
                                                           [ASUSGiftManager modifyGiftToShoppingCart:self.giftModel];
                                                       } else {
                                                           self.giftModel.exchangeNum = 1;
                                                           [ASUSGiftManager addGiftToShoppingCart:self.giftModel];
                                                       }
                                                       ASUSShoppingCartViewController *shopCartVc = [[ASUSShoppingCartViewController alloc] init];
                                                       [self.navigationController pushViewController:shopCartVc animated:YES];
                                                       if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                                                           self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                                                       }
                                                   } else {
                                                       self.giftModel.exchangeNum = 1;              ASUSExchangeViewController *eVC = [[ASUSExchangeViewController alloc] initWithModels:[NSArray arrayWithObjects:self.giftModel, nil]];
                                                       [self.navigationController pushViewController:eVC animated:YES];
                                                       if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                                                           self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                                                       }
                                                   }
                                                   //                                                                 self.selectedMenuIndex = segmentIndex;
                                                   //                                                                 [self loadTableViewData];
                                                   
                                               }];
    segementedControl.layer.borderColor = [NCColorUtil colorForNavgationBarBackground].CGColor;
    segementedControl.layer.borderWidth = 1.0;
    segementedControl.selectedColor = [NCColorUtil colorForNavgationBarBackground];
    segementedControl.color = [UIColor whiteColor];
    segementedControl.textAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:[NCFontUtil fontForTitle]],
                                       NSForegroundColorAttributeName:[NCColorUtil colorForTitleOrNickFont]};
    segementedControl.selectedTextAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:[NCFontUtil fontForTitle]],
                                               NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self.bottomView addSubview:segementedControl];
}

- (void)setupDetailInfoView {
//    self.detailInfoView
}

#pragma mark - setup data

- (void)setupData
{
    [self showHud:HBHudShowTypeActivityOnly withText:nil];
    [ASUSGiftModel fetchGiftDetailByGiftID:giftID completion:^(NSError *error, ASUSGiftModel *giftModle) {
        [self hideHud];
        if (giftModle) {
            self.giftModel = giftModle;
            if ([[BAAuthenticate sharedClient].userType integerValue] == 1) {
                self.needPointsLabel.text = [NSString stringWithFormat:@"%ld",(long)giftModle.innerScore];
            }else {
                self.needPointsLabel.text = [NSString stringWithFormat:@"%ld",(long)giftModle.outerScore];
            }
            [self.giftDetailWebView loadHTMLString:self.giftModel.giftDesc baseURL:nil];
        }
    }];
}

#pragma mark - Event Response
- (void)backAction
{
    if (self.blurView&&!self.blurView.hidden) {
        self.blurView.hidden = YES;
    }else {
         [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (void)detailButtonClicked {
    if (!self.blurView) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kNavigationBarHeight)];
        [webView loadHTMLString:self.giftModel.giftNote baseURL:nil];        webView.backgroundColor = [UIColor clearColor];
        webView.opaque = NO;
//        NSString *note = [self.giftModel.giftNote stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
//        CGFloat detailInfoLabelHeight = ScreenHeight - kNavigationBarHeight;
//        CGSize size = [note sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ScreenWidth - 20, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
//        detailInfoLabelHeight = size.height;
//        
//        self.detailInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, ScreenWidth - 20, ScreenHeight - kNavigationBarHeight)];
//        self.detailInfoLabel.font = [UIFont systemFontOfSize:14];
//        self.detailInfoLabel.numberOfLines = 0;
//        self.detailInfoLabel.textColor = [NCColorUtil colorForHomeCellTitleColor];
//        self.detailInfoLabel.backgroundColor = [UIColor clearColor];
//        self.detailInfoLabel.text = note;
//        self.detailInfoLabel.frame = CGRectMake(10, 40, ScreenWidth - 20, detailInfoLabelHeight);
//        
//        UIScrollView *detailInfoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kNavigationBarHeight)];
//        detailInfoScrollView.backgroundColor = [UIColor clearColor];
//        detailInfoScrollView.contentSize = CGSizeMake(ScreenWidth, detailInfoLabelHeight + 40);
//        
//        float blurViewHeight = detailInfoLabelHeight  + 40 > (ScreenHeight - kNavigationBarHeight) ? detailInfoLabelHeight + 40:  (ScreenHeight - kNavigationBarHeight);
        self.blurView = [[ASUSBlurView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kNavigationBarHeight)];
        [self.blurView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:self.blurView];
//         [self.blurView addSubview:detailInfoScrollView];
        [self.blurView addSubview:webView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detailInfoScrollViewClicked:)];
        [self.blurView addGestureRecognizer:tapGesture];
        
//        [detailInfoScrollView addSubview:self.detailInfoLabel];
    }
    self.blurView.hidden = NO;
}

- (void)detailInfoScrollViewClicked:(UITapGestureRecognizer *)tapGesture {
    tapGesture.view.hidden = YES;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //若已经加载完成，则显示webView并return
    if(isLoadingFinished)
    {
        [self.giftDetailWebView setHidden:NO];
        return;
    }
    
    //js获取body宽度
    NSString *bodyWidth= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollWidth"];
    
    int widthOfBody = [bodyWidth intValue];
    
    //获取实际要显示的html
    NSString *html = [self htmlAdjustWithPageWidth:widthOfBody
                                              html:self.giftModel.giftDesc
                                           webView:webView];
    
    //设置为已经加载完成
    isLoadingFinished = YES;
    //加载实际要现实的html
    [self.giftDetailWebView loadHTMLString:html baseURL:nil];
}

//获取宽度已经适配于webView的html。这里的原始html也可以通过js从webView里获取
- (NSString *)htmlAdjustWithPageWidth:(CGFloat )pageWidth
                                 html:(NSString *)html
                              webView:(UIWebView *)webView
{
    NSMutableString *str = [NSMutableString stringWithString:html];
    //计算要缩放的比例
    CGFloat initialScale = webView.frame.size.width/pageWidth;
    //将</head>替换为meta+head
    NSString *stringForReplace = [NSString stringWithFormat:@"<meta name=\"viewport\" content=\" initial-scale=%f, minimum-scale=0.1, maximum-scale=2.0, user-scalable=yes\"></head>",initialScale];
    
    NSRange range =  NSMakeRange(0, str.length);
    //替换
    [str replaceOccurrencesOfString:@"</head>" withString:stringForReplace options:NSLiteralSearch range:range];
    return str;
}
@end

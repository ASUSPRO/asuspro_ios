//
//  ASUSProductListViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-17.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSProductListViewController.h"
#import "ASUSProductListCell.h"
#import "ASUSProductDetailViewController.h"
#import "NCImageUtil.h"
#import "ASUSGiftModel.h"

@interface ASUSProductListViewController ()<UITableViewDataSource, UITableViewDelegate, ASUSProductListCellViewDelegete> {
    NSInteger page;
}
@property (nonatomic, strong) UITableView *productListTableView;
@property (nonatomic, strong) NSMutableArray *productList;
@property (nonatomic, strong) NSString *searchWord;
@end

@implementation ASUSProductListViewController

#pragma mark - lifecycle method

- (id)initWithKeyword:(NSString *)keyword
{
    if (self = [super init]) {
        self.searchWord = keyword;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.productList = [[NSMutableArray alloc] initWithCapacity:0];
    self.view.backgroundColor = [NCColorUtil backGroundColorForView];
    [self configNavigationBar];
    [self setupTableView];
    [self setupData];
    
}

#pragma mark - views

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"积分商城";
    self.navigationItem.titleView = titleLabel;
}

- (void)setupTableView {
    self.productListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
    self.productListTableView.delegate = self;
    self.productListTableView.scrollEnabled = YES;
    self.productListTableView.dataSource = self;
    self.productListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.productListTableView];
}

#pragma mark - setup data 

- (void)setupData {
    [self showHud:HBHudShowTypeActivityOnly withText:@""];
    if (self.productListViewControllerType == ASUSProductListViewControllerTypeNewProduct) {
        [ASUSGiftModel fetchNewGiftListPerMonthWithCompletion:^(NSError *error, NSArray *list) {
            [self hideHud];
            [self.productList removeAllObjects];
            self.productList = [self arrayWithSplitArray:list];
            [self.productListTableView reloadData];
        }];
    } else if (self.productListViewControllerType == ASUSProductListViewControllerTypeASUSProduct) {
        [ASUSGiftModel fetchAsusGiftListWithCompletion:^(NSError *error, NSArray *list) {
            [self hideHud];
            [self.productList removeAllObjects];
            self.productList = [self arrayWithSplitArray:list];
            [self.productListTableView reloadData];
        }];
    }else if (self.productListViewControllerType == ASUSProductListViewControllerTypeSearch){
        [self showHud:HBHudShowTypeActivityOnly withText:@""];
        [ASUSGiftModel fetchSearchGiftByKeyword:self.searchWord completion:^(NSError *error, NSArray *list) {
            [self hideHud];
            [self.productList removeAllObjects];
            self.productList = [self arrayWithSplitArray:list];
            [self.productListTableView reloadData];
        }];
    }else {
        if (self.menuModel) {
            [ASUSGiftModel fetchGiftListByCategoryID:self.menuModel.categoryId page:page completion:^(NSError *error, NSArray *list) {
                [self hideHud];
                [self.productList removeAllObjects];
                self.productList = [self arrayWithSplitArray:list];
                [self.productListTableView reloadData];
            }];
        }
    }
}

#pragma mark - Event Response
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.productList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kASUSProductListCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ASUSProductListCell *productListCell = nil;
    static NSString *identifier = @"ASUSProductListCell";
    productListCell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (productListCell == nil) {
        productListCell = [[ASUSProductListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    [productListCell setupViewsWithArray:[self.productList objectAtIndex:indexPath.row] delegate:self];
    productListCell.bottomLineView.hidden = YES;
//    if (indexPath.row == self.productList.count - 1) {
//        productListCell.bottomLineView.hidden = YES;
//    }
//    [productListCell refreshUI];
    return productListCell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    ASUSPolicyNoticeDetailViewController *detailVC = [[ASUSPolicyNoticeDetailViewController alloc] init];
    //    [self.navigationController pushViewController:detailVC animated:YES];
    
}

#pragma mark - private method

//将请求的List数组拆分成:由最多2个元素的小数组组成的二维大数组以方便给cell赋值
- (NSMutableArray *)arrayWithSplitArray:(NSArray *)allProductListArray {
    NSMutableArray *productListArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (int i = 0 ; i < allProductListArray.count; i++) {
        if (i % 2 == 0) {
            NSMutableArray *tempArray = [NSMutableArray array];
            [productListArray addObject:tempArray];
        }
        [[productListArray lastObject] addObject:[allProductListArray objectAtIndex:i]];
    }
    return productListArray;
}

#pragma mark - ASUSProductListCellViewDelegete 代理方法

- (void)tapProductImageViewActionOfCellView:(id)sender {
        UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)sender;
        ASUSProductListCellView *productListCellView = (ASUSProductListCellView *)tapGesture.view;
    ASUSProductDetailViewController *productDetailsVC = [[ASUSProductDetailViewController alloc] initWithGiftId:productListCellView.giftModel.gifId];
    [self.navigationController pushViewController:productDetailsVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}




@end

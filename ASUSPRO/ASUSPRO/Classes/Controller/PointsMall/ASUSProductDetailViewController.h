//
//  ASUSProductDetailViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-13.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"

@interface ASUSProductDetailViewController : ASUSBaseViewController

-(id)initWithGiftId:(NSInteger)giftId;

@end

//
//  ASUSLookExpressViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/5.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSLookExpressViewController.h"
#import "JHAPISDK.h"
#import "JHOpenidSupplier.h"
#import "ASUSExpressCell.h"

@interface ASUSLookExpressViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)ASUSExchangeModel *exchangeModel;
@property (nonatomic, strong)UITableView *expressTableView;
@property (nonatomic, strong)UILabel *expressStatusLabel;
@property (nonatomic, strong)UILabel *expressNoLabel;
@property (nonatomic, strong)UILabel *expressCompanyLabel;
@property (nonatomic, strong)NSMutableArray *expressList;
@end

@implementation ASUSLookExpressViewController

#pragma mark - lifeCycle

- (id)initWithModel:(ASUSExchangeModel *)model
{
    if (self = [super init]) {
        self.exchangeModel = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configNavigationBar];
    
    [[JHOpenidSupplier shareSupplier] registerJuheAPIByOpenId:@"JHe6e69e130e671d6e9cfa9d3b9cbb6824"];
    
    self.expressList = [NSMutableArray arrayWithCapacity:0];
    
    [self setUpView];
    [self loadExpressData];
    // Do any additional setup after loading the view.
}

#pragma mark -- Private Method

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"查看物流";
    self.navigationItem.titleView = titleLabel;
}

- (UIView *)creatTableViewHeaderView
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 92)];
    [self.view addSubview:topView];
    
    UILabel *expressStatus =[[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth - 20, 30)];
    expressStatus.font = [UIFont systemFontOfSize:15];
    expressStatus.text = @"物流状态:";
    expressStatus.textColor = [NCColorUtil colorFromHexString:@"#a3a9af"];
    [topView addSubview:expressStatus];
    
    self.expressStatusLabel =[[UILabel alloc] initWithFrame:CGRectMake(80, 10, ScreenWidth - 20, 30)];
    self.expressStatusLabel.font = [UIFont systemFontOfSize:16];
    self.expressStatusLabel.textColor = [NCColorUtil colorFromHexString:@"#fd7864"];
    self.expressStatusLabel.text = @"配送中";
    [topView addSubview:self.expressStatusLabel];
    
    UILabel *expressNoText =[[UILabel alloc] initWithFrame:CGRectMake(10, 40, 80, 20)];
    expressNoText.font = [UIFont systemFontOfSize:15];
    expressNoText.text = @"运单号:";
    expressNoText.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:expressNoText];
    
    self.expressNoLabel =[[UILabel alloc] initWithFrame:CGRectMake(65, 40, ScreenWidth - 160, 20)];
    self.expressNoLabel.font = [UIFont systemFontOfSize:15];
    self.expressNoLabel.text = self.exchangeModel.expressNo;
    self.expressNoLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.expressNoLabel];
    
    self.expressCompanyLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth - 160, 40, 150, 20)];
//    self.expressCompanyLabel.text = self.exchangeModel.express;
    self.expressCompanyLabel.font  = [UIFont systemFontOfSize:14];
    self.expressCompanyLabel.textAlignment = NSTextAlignmentRight;
    self.expressCompanyLabel.textColor = [NCColorUtil colorFromHexString:@"#84898f"];
    [topView addSubview:self.expressCompanyLabel];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 72-0.5,ScreenWidth, 0.5)];
    bottomLineView.backgroundColor = [NCColorUtil colorForCellSeparator];
    [topView addSubview:bottomLineView];
    
    return topView;
}


- (void)setUpView {
    self.expressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight-kNavigationBarHeight)];
    self.expressTableView.delegate = self;
    self.expressTableView.dataSource = self;
    self.expressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.expressTableView.tableHeaderView = [self creatTableViewHeaderView];
    [self.view addSubview:self.expressTableView];
}



#pragma mark -- loadRequestData
- (void)loadExpressData
{
    NSLog(@"ff:%@",self.exchangeModel.express);
    NSLog(@"nihao:%@",self.exchangeModel.expressNo);
    NSString *path = @"http://v.juhe.cn/exp/index";
    NSString *api_id = @"43";
    NSString *method = @"POST";
    NSDictionary *param = @{@"key":@"e90971b00452d8079c08c047208647ea",@"com":self.exchangeModel.express,@"no":self.exchangeModel.expressNo,@"dtype":@"json"};
    JHAPISDK *juheapi = [JHAPISDK shareJHAPISDK];
    
    [juheapi executeWorkWithAPI:path
                          APIID:api_id
                     Parameters:param
                         Method:method
                        Success:^(id responseObject){
                            if ([[param objectForKey:@"dtype"] isEqualToString:@"xml"]) {
                                NSLog(@"***xml*** \n %@", responseObject);
                            }else{
                                int error_code = [[responseObject objectForKey:@"error_code"] intValue];
                                if (!error_code) {
                                    if (responseObject[@"result"] && [responseObject[@"result"][@"list"] isKindOfClass:[NSArray class]]) {
                                        NSArray *expressList = [NSArray arrayWithArray:responseObject[@"result"][@"list"]];
                                        self.expressCompanyLabel.text = responseObject[@"result"][@"company"];
                                        if ([responseObject[@"result"][@"status"] integerValue] == 0) {
                                            self.expressStatusLabel.text = @"退回";
                                        }else if ([responseObject[@"result"][@"status"] integerValue] == 1){
                                             self.expressStatusLabel.text = @"已签收";
                                        }else{
                                           self.expressStatusLabel.text = @"配送中";
                                        }
                                        for (int i = 0; i < expressList.count; i++) {
                                            ASUSExpressModel *express = [[ASUSExpressModel alloc] init];
                                            express.expressCurrentAddress = expressList[i][@"zone"];
                                            express.expressTime = expressList[i][@"datetime"];
                                            express.expressRemark = expressList[i][@"remark"];
                                            [self.expressList addObject:express];
                                        }
                                        [self.expressTableView reloadData];
                                    }
                                    NSLog(@" %@", responseObject);
                                }else{
                                    NSLog(@" %@", responseObject);
                                }
                            }
                        } Failure:^(NSError *error) {
                            NSLog(@"error:   %@",error.description);
                        }];
}

#pragma mark - Action Method
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.expressList.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ASUSExpressCell getASUSExpessCellHeight:[self.expressList objectAtIndex:indexPath.row]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ASUSExpressCell";
    ASUSExpressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ASUSExpressCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.expressModel = [self.expressList objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {
        [cell refreshFirstTimeLineFrame];
    } else if (indexPath.row == self.expressList.count - 1) {
        [cell refreshLastTimeLineFrame];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

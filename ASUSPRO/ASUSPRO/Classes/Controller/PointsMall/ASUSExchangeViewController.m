//
//  ASUSExchangeEViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/5/16.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSExchangeViewController.h"
#import "ASUSExchangeCell.h"
#import "ASUSAddressListViewController.h"
#import "ZSDPaymentView.h"
#import "ASUSLoginViewController.h"
#import "ASUSExchangeRecordViewController.h"

#import "BAAuthenticate.h"
#import "ASUSUserModel.h"
#import "BAAuthenticate.h"

#import "ASUSOrderModel.h"

#import "HuoBanKit.h"

@interface ASUSExchangeViewController ()<UITableViewDataSource,UITableViewDelegate,PaymentCompleteDelegate>

@property (nonatomic, strong) UITableView *exchangeTableView;

@property (nonatomic, strong) NSArray *exchangeTitleArr;

@property (nonatomic, strong) NSArray *exchangeDetailArr;

@property (nonatomic, strong) ASUSReceiveAddressModel *addressModel;

@property (nonatomic, strong) NSArray *giftModels;

@property (nonatomic, assign) long long totalPointsNum;

@end

@implementation ASUSExchangeViewController

#pragma mark - lifeCycle

- (id)initWithModels:(NSArray *)models
{
    if (self = [super init]) {
        self.giftModels = models;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configNavigationBar];
    
    [self setUpView];
    
    [self calculateTotalPointsNum];
    
    [self loadData];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- Private Method

- (void)configNavigationBar {
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithImage:[NCImageUtil imageFromText:@"\ue61b" fontSize:20 color:[NCColorUtil colorForTitleOrNickFont]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"兑换信息";
    self.navigationItem.titleView = titleLabel;
}

- (void)setUpView
{
    self.exchangeTableView = [[UITableView alloc] initWithFrame:self.view.frame];
    self.exchangeTableView.delegate = self;
    self.exchangeTableView.dataSource = self;
    self.exchangeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.exchangeTableView.tableFooterView = [self createFootView];
    [self.view addSubview:self.exchangeTableView];
}

- (void)loadData
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASUSReceiveAddress"];
    self.addressModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    if (self.addressModel) {
        self.exchangeTitleArr = @[@"收货信息",@"",@"兑换信息",@"    账户已有积分",@"    此单使用积分"];
        self.exchangeDetailArr = @[@"",@"",@"",[BAAuthenticate sharedClient].score,[NSString stringWithFormat:@"%ld",(long)self.totalPointsNum]];
    }else {
        self.exchangeTitleArr = @[@"收货信息",@"兑换信息",@"    账户已有积分",@"    此单使用积分"];
        self.exchangeDetailArr = @[@"",@"",[BAAuthenticate sharedClient].score,[NSString stringWithFormat:@"%ld",(long)self.totalPointsNum]];
       
    }
}

- (void)calculateTotalPointsNum {
    for (ASUSGiftModel *giftModel in self.giftModels) {
        if([[BAAuthenticate sharedClient].userType integerValue] == 1){
            self.totalPointsNum = self.totalPointsNum + (giftModel.innerScore * giftModel.exchangeNum);
        } else {
            self.totalPointsNum = self.totalPointsNum + (giftModel.outerScore * giftModel.exchangeNum);
        }
    }
}

#pragma mark - Event Response

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)editAddressAction
{
    ASUSAddressListViewController *addressListVC = [[ASUSAddressListViewController alloc] init];
    addressListVC.chooseAddressBlock = ^(ASUSReceiveAddressModel *model){
        self.addressModel = model;
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"ASUSReceiveAddress"];
        
        self.exchangeTitleArr = @[@"收货信息",@"",@"兑换信息",@"    账户已有积分",@"    此单使用积分"];
        self.exchangeDetailArr = @[@"",@"",@"",[BAAuthenticate sharedClient].score,[NSString stringWithFormat:@"%ld",(long)self.totalPointsNum]];
        [self.exchangeTableView  reloadData];
    };
    [self.navigationController pushViewController:addressListVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)exchangeAction
{
    if (self.addressModel) {
        ZSDPaymentView *payment = [[ZSDPaymentView alloc]init];
        payment.paymentForm.hideCloseButton = NO;
        payment.title = @"  请输入兑换密码";
        payment.goodsName = [NSString stringWithFormat:@"共消耗%lld积分",self.totalPointsNum];
        payment.paymentCompleteDelegate = self;
        [payment show];
    }else {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请填写收货地址"];
        [self hideHudAfter:2];
    }
    
}

- (void)pushViewController
{
    ASUSExchangeRecordViewController *exchangeVC = [[ASUSExchangeRecordViewController alloc] initWithType:DECORDTYPE];
    [self.navigationController pushViewController:exchangeVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - PayMentDelegate
- (void)paymentComplete:(NSString *)payPassword
{
    NSLog(@"paypassword:%@",payPassword);
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error &&[message isEqualToString:@"用户token有效"]) {
            NSString *md5_payment = [NCDeviceUtil md5:[NSString stringWithFormat:@"SHANGHAIOKAYBEIJING%@",payPassword]];
            [ASUSUserModel checkPayPassByUserID:[[BAAuthenticate sharedClient].userId integerValue] pay_password:md5_payment completion:^(NSError *error, NSString *message) {
                if ([message isEqualToString:@"验证正确"]) {
                    NSMutableArray *productArray = [[NSMutableArray alloc] initWithCapacity:0];
                    for (ASUSGiftModel *giftModel in self.giftModels) {
                        NSDictionary *orderDetail = @{@"ProductID":[NSString stringWithFormat:@"%ld",(long)giftModel.gifId],@"Num":[NSString stringWithFormat:@"%ld",(long)giftModel.exchangeNum]};
                        [productArray addObject:orderDetail];
                    }
                    NSDictionary *dic = @{@"OrderInfo":@{@"UserId":[BAAuthenticate sharedClient].userId,@"ReceiveId":[NSString stringWithFormat:@"%ld",(long)self.addressModel.addressId],@"UseScore":[NSString stringWithFormat:@"%ld",(long)self.self.totalPointsNum]},@"OrderDetail":productArray};
                    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
                    NSString *dataString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    [ASUSOrderModel UploadOrderByData:dataString completion:^(NSError *error,  NSString *success) {
                        if (error == nil&&[success isEqualToString:@"操作成功"]) {
                            [BAAuthenticate sharedClient].score = [NSString stringWithFormat:@"%ld",(long)([[BAAuthenticate sharedClient].score integerValue] - self.totalPointsNum)];
                            [self showHud:HBHudShowTypeCaptionOnly withText:@"兑换成功!"];
                            [self hideHudAfter:2.0];
                            [self performSelector:@selector(pushViewController) withObject:nil afterDelay:2.0f];
                        }else {
                            [self showHud:HBHudShowTypeCaptionOnly withText:success];
                            [self hideHudAfter:2.0];
                        }
                    }];
                }else {
                    [self showHud:HBHudShowTypeCaptionOnly withText:@"输入兑换密码错误"];
                    [self hideHudAfter:2];
                    
                }
                
            }];
        }else {
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.addressModel) {
        return 5;
    }
    return 4;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.addressModel) {
        if (indexPath.row == 1) {
            return 117;
        }
    }
    return 55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addressModel) {
        if (indexPath.row == 1) {
            ASUSExchangeCell *cell = [[ASUSExchangeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ASUSExchangeCell"];
            [cell setAddressData:self.addressModel];
            return cell;
        }else {
            static NSString *cellIdentifier = @"ExchangeCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
            cell.textLabel.font = [UIFont systemFontOfSize:16.0];
            
            if (indexPath.row ==0) {
                
                UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                editBtn.frame = CGRectMake(self.view.frame.size.width-90, 13, 100, 30);
                [editBtn setTitleColor:[UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0] forState:UIControlStateNormal];
                editBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
                editBtn.titleLabel.textAlignment = NSTextAlignmentRight;
                [editBtn setTitle:@"编辑地址" forState:UIControlStateNormal];
                [editBtn addTarget:self action:@selector(editAddressAction) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:editBtn];
                
            }else {
                cell.detailTextLabel.textColor = [UIColor colorWithRed:253.0/255 green:120.0/255 blue:100.0/255 alpha:1.0];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:15.0];
            }
            
            if (indexPath.row == 2 || indexPath.row == 3) {
                UIView *lineView= [[UIView alloc] initWithFrame:CGRectMake(20, 54, self.view.frame.size.width-10, 1)];
                lineView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
                [cell.contentView addSubview:lineView];
            }
            if (indexPath.row == 4) {
                UIView *lineView= [[UIView alloc] initWithFrame:CGRectMake(0, 54, self.view.frame.size.width, 1)];
                lineView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
                [cell.contentView addSubview:lineView];
            }
            cell.textLabel.text = self.exchangeTitleArr[indexPath.row];
            cell.detailTextLabel.text = self.exchangeDetailArr[indexPath.row];
           
            return cell;
        }

    }else
    {
        static NSString *cellIdentifier = @"ExchangeCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.textLabel.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
        cell.textLabel.font = [UIFont systemFontOfSize:16.0];
        
        if (indexPath.row ==0) {
            
            UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            editBtn.frame = CGRectMake(self.view.frame.size.width-90, 13, 100, 30);
            [editBtn setTitleColor:[UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0] forState:UIControlStateNormal];
            editBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
            editBtn.titleLabel.textAlignment = NSTextAlignmentRight;
            [editBtn setTitle:@"编辑地址" forState:UIControlStateNormal];
            [editBtn addTarget:self action:@selector(editAddressAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:editBtn];
            
        }else {
            cell.detailTextLabel.textColor = [UIColor colorWithRed:253.0/255 green:120.0/255 blue:100.0/255 alpha:1.0];
            cell.detailTextLabel.font = [UIFont systemFontOfSize:15.0];
        }
        
        if (indexPath.row == 1 || indexPath.row == 2) {
            UIView *lineView= [[UIView alloc] initWithFrame:CGRectMake(20, 54, self.view.frame.size.width-10, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
            [cell.contentView addSubview:lineView];
        }
        if (indexPath.row == 3) {
            UIView *lineView= [[UIView alloc] initWithFrame:CGRectMake(0, 54, self.view.frame.size.width, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1.0];
            [cell.contentView addSubview:lineView];
        }
        
        cell.textLabel.text = self.exchangeTitleArr[indexPath.row];
        cell.detailTextLabel.text = self.exchangeDetailArr[indexPath.row];
        return cell;

    }
}

-(UIView *)createFootView
{
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 104)];
    footView.userInteractionEnabled = YES;
    footView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1.0];
    UIButton *exchangeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exchangeBtn.frame = CGRectMake(10, 32, self.view.frame.size.width -20, 40);
    exchangeBtn.backgroundColor= [UIColor blackColor];
    [exchangeBtn setTitle:@"兑换" forState:UIControlStateNormal];
    [exchangeBtn addTarget:self action:@selector(exchangeAction) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:exchangeBtn];
    
    return footView;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

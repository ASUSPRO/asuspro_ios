//
//  HomeViewController.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/4/29.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "HomeViewController.h"
#import "ASUSLoginViewController.h"
#import "ASUSArticleViewController.h"
#import "ASUSPriceArticleViewController.h"
#import "ASUSPointsMallViewController.h"
#import "ASUSShoppingCartViewController.h"
#import "ASUSPersonCenterViewController.h"
#import "BAClient.h"

#import <AFNetworking/AFNetworking.h>
#import "NCColorUtil.h"
#import "NCImageUtil.h"
#import "PinYinForObjc.h"
#import "ASUSProductDetailViewController.h"

#import "UIImageView+AFNetworking.h"
#import "BAAuthenticate.h"
#import "ASUSEditPersonInfoViewController.h"
#import "ASUSSearchViewController.h"

#import "ZSDPaymentView.h"
#import "ASUSColumnPicModel.h"
#import "ASUSUserModel.h"
#import "HuoBanKit.h"
#import "ASUSGiftModel.h"
#import "ASUSSearchModel.h"
#import "ASUSSearchManager.h"

#import "AdScrollView.h"

@interface HomeViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,PaymentCompleteDelegate,AdPageViewDelegate,UITextFieldDelegate >
{
    UISearchDisplayController *searchDisplayController;
}
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSArray *cellTitleArray;
@property (nonatomic, strong) NSArray *cellIconArray;
//@property (nonatomic, strong) UISearchBar *homeSearchBar;
@property (nonatomic, strong) UITextField *searchTextField;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UIButton *avatarView;
@property (nonatomic, assign) BOOL searchIsActive;
@property (nonatomic, strong) UITableView *recentlySearchTableView;
@property (nonatomic, strong) UIView *recentlySearchMask;
@property (nonatomic, assign) BOOL isShowRecentlySearch;

@property (nonatomic, strong) NSArray *columnPicArr;
@property (nonatomic, strong) NSArray *recentlySearchTexts;
@property (nonatomic, strong) NSString *firstPwd;
@property (nonatomic, strong) NSString *secondPwd;
@property (nonatomic, strong) ZSDPaymentView *payment ;
@end

@implementation HomeViewController

#pragma mark - lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    // Do any additional setup after loading the view, typically from a nib.
    [self configNavigationBar];
    [self setupTableView];
//    [self setupRecentlySearchTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.avatarView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[BAAuthenticate sharedClient].avatarLink]]] forState:UIControlStateNormal];
    self.isShowRecentlySearch = NO;
//    self.recentlySearchMask.hidden = !self.isShowRecentlySearch;
    self.mainTableView.tableFooterView = nil;
    [self.mainTableView reloadData];
}

#pragma mark - RequestData

- (void)loadData
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLogin"] && [[BAAuthenticate sharedClient].set_pay_password integerValue] == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLogin"];
        [self setPayPassword:@"请设置6位数兑换密码"];
    }
    
    self.cellTitleArray = @[@"积分商城",@"政策通知",@"配件价格",@"技术分享"];
    self.cellIconArray = @[@"\ue60c",@"\ue60e",@"\ue606",@"\ue60f"];
    [ASUSColumnPicModel fetchColumnPicWithCompletion:^(NSError *error, NSArray *list) {
        if (list) {
            self.columnPicArr = [NSArray arrayWithArray:list];
            [self setupAdView];
        }
    }];
    
}

#pragma mark - views

- (void)configNavigationBar {
    UILabel  *titleLabel = [[UILabel alloc] initWithFrame:(CGRect){(self.view.bounds.size.width - 100)/2,0,100,44}];
    titleLabel.font = [UIFont systemFontOfSize:[NCFontUtil fontForTitle]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"ASUSPRO";
    self.navigationItem.titleView = titleLabel;
    
    self.avatarView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarView.frame = CGRectMake(0, 0, 32, 32);
    self.avatarView.layer.cornerRadius = 16;
    self.avatarView.layer.masksToBounds = YES;
    [self.avatarView addTarget:self action:@selector(avatarViewClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.avatarView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[BAAuthenticate sharedClient].avatarLink]]] forState:UIControlStateNormal];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.avatarView];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

- (void)setupTableView {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kNavigationBarHeight)];
    self.mainTableView.delegate = self;
    self.mainTableView.scrollEnabled = YES;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableHeaderView = [self createTableViewHeaderView];
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.mainTableView];
}

- (void)setupAdView {
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    [self.columnPicArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
        ASUSColumnPicModel *model = (ASUSColumnPicModel *)obj;
        [imageArr addObject:model.picUrl];
    }];
    AdScrollView *scrollView = [[AdScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 195)];
    scrollView.iDisplayTime = 2;
    scrollView.bWebImage  = YES;
    scrollView.delegate = self;
    [scrollView startAdsWithBlock:imageArr block:^(NSInteger clickIndex){
    }];
    [self.mainTableView.tableHeaderView addSubview:scrollView];
}

- (UIView *)createTableViewHeaderView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40 + 195)];
    headerView.userInteractionEnabled = YES;
//    self.homeSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 40, 40)];
//    self.homeSearchBar.delegate = self;
//    self.homeSearchBar.backgroundColor = [UIColor clearColor];
//    self.homeSearchBar.backgroundImage = [UIImage imageNamed:@"searchBarbg"];
//    [self.homeSearchBar setPlaceholder:@"please input keywords"];
//    [self.homeSearchBar setValue:[NCColorUtil colorFromHexString:@"#b5bdc5"] forKeyPath:@"_placeholderLabel.textColor"];
    UIImageView *searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10 + 195, 20, 20)];
    searchImageView.image = [NCImageUtil imageFromText:@"\ue60a" fontSize:20 color:[NCColorUtil colorFromHexString:@"#84898f"]];
    [headerView addSubview:searchImageView];
    
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(50, 195, ScreenWidth - 80, 40)];
    self.searchTextField.borderStyle = UITextBorderStyleNone;
    self.searchTextField.delegate = self;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.placeholder = @"please input keywords";
    self.searchTextField.font = [UIFont systemFontOfSize:14.0];
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.textColor = [UIColor colorWithRed:96.0/255 green:99.0/255 blue:102.0/255 alpha:1.0];
    [headerView addSubview:self.searchTextField];
    
    UIButton *recentlySearchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    recentlySearchButton.frame = CGRectMake(ScreenWidth - 40, 195, 40, 40);
    [recentlySearchButton setImage:[NCImageUtil imageFromText:@"\ue602" fontSize:15 color:[NCColorUtil colorForCellSeparator]] forState:UIControlStateNormal];
    [recentlySearchButton addTarget:self action:@selector(recentlySearchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:recentlySearchButton];
    
//    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.homeSearchBar contentsController:self];
//    searchDisplayController.active = NO;
//    searchDisplayController.searchResultsDataSource = self;
//    searchDisplayController.searchResultsDelegate = self;
//    [headerView addSubview:self.homeSearchBar];
    
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(20, 195 + 39.5, ScreenWidth, 0.5)];
    horizontalLine.backgroundColor = [NCColorUtil colorForCellSeparator];
    [headerView addSubview:horizontalLine];
    
    return headerView;
    
}

- (void)setPayPassword:(NSString *)tipString
{
    ZSDPaymentView *payment = [[ZSDPaymentView alloc]init];
    payment.paymentForm.hideCloseButton = YES;
    payment.title = @"  设置";
    payment.paymentCompleteDelegate = self;
    payment.goodsName = tipString;
    [payment show];
}

- (void)setupRecentlySearchTableView  {
    self.recentlySearchMask = [[UIView alloc] initWithFrame:CGRectMake(0, 195 + 40, ScreenWidth, ScreenHeight - (195 + 40))];
    self.recentlySearchMask.backgroundColor = [UIColor whiteColor];
    self.recentlySearchMask.hidden = YES;
    [self.view addSubview:self.recentlySearchMask];
    
    self.recentlySearchTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - (195 + 40 + kNavigationBarHeight)) style:UITableViewStylePlain];
    self.recentlySearchTableView.delegate = self;
    self.recentlySearchTableView.dataSource = self;
    self.recentlySearchTableView.backgroundColor = [UIColor clearColor];
    UIView *bgview = [[UIView alloc] initWithFrame:self.view.bounds];
    bgview.backgroundColor = [UIColor whiteColor];
    self.recentlySearchTableView.showsVerticalScrollIndicator = NO;
    self.recentlySearchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.recentlySearchTableView.tableFooterView = bgview;
    [self.recentlySearchMask addSubview:self.recentlySearchTableView];

    if (self.recentlySearchTexts) {
        self.recentlySearchTableView.tableFooterView = [self createRecentlySearchTableViewFooterView];
    }
}

- (UIView *)createRecentlySearchTableViewFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    UIButton *clearHistoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    clearHistoryButton.frame = CGRectMake((ScreenWidth - 120)/2, 5, 120, 30);
    [clearHistoryButton addTarget:self action:@selector(clearHistoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    clearHistoryButton.backgroundColor = [UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0];
    clearHistoryButton.layer.cornerRadius = 2;
    [clearHistoryButton setTitle:@"清除历史记录" forState:UIControlStateNormal];
    clearHistoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [clearHistoryButton setTitleColor:[NCColorUtil colorForHomeCellTitleColor] forState:UIControlStateNormal];
    [footerView addSubview:clearHistoryButton];
    return footerView;
}

#pragma mark - Actions 

- (void)recentlySearchButtonClicked:(UIButton *)recentlyButton {
    [self.searchTextField resignFirstResponder];
    self.isShowRecentlySearch = !self.isShowRecentlySearch;
//    self.recentlySearchMask.hidden = !self.isShowRecentlySearch;
//    self.recentlySearchTableView.hidden = !self.isShowRecentlySearch;
    self.recentlySearchTexts = [ASUSSearchManager fetchSearchTextList];
//    [self.recentlySearchTableView reloadData];
    [self.mainTableView reloadData];
    if (self.isShowRecentlySearch) {
        if (self.recentlySearchTexts.count > 0) {
            self.mainTableView.tableFooterView = [self createRecentlySearchTableViewFooterView];
        } else {
            self.mainTableView.tableFooterView = nil;
        }
    } else {
        self.mainTableView.tableFooterView = nil;
    }
   
}

- (void)clearHistoryButtonClicked:(UIButton *)clearButton {
    [ASUSSearchManager clearSearchTexts];
    self.recentlySearchTexts = [ASUSSearchManager fetchSearchTextList];
//    [self.recentlySearchTableView reloadData];
//    self.isShowRecentlySearch = !self.isShowRecentlySearch;
//    self.recentlySearchMask.hidden = !self.isShowRecentlySearch;
//    self.recentlySearchTableView.tableFooterView = nil;
    self.isShowRecentlySearch = NO;
    [self.mainTableView reloadData];
    self.mainTableView.tableFooterView = nil;
}

#pragma mark - Event Response

- (void)avatarViewClicked {
    ASUSPersonCenterViewController *noticeVC = [[ASUSPersonCenterViewController alloc] init];
    [self.navigationController pushViewController:noticeVC animated:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

//- (void)recentlySearchButtonClicked {
//    if (self.searchTextField.text.length > 0) {
//        ASUSSearchViewController *searchVC = [[ASUSSearchViewController alloc] initWithKeyword:self.searchTextField.text];
//        [self.navigationController pushViewController:searchVC animated:YES];
//        [self.searchTextField resignFirstResponder];
//        self.searchTextField.text = @"";
//    }else {
//        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入关键字"];
//        [self hideHudAfter:2.0];
//    }
//    
//}

#pragma mark - PayMentDelegate
- (void)paymentComplete:(NSString *)payPassword
{
//    NSLog(@"paypassword:%@",payPassword);
    if (!self.firstPwd && [[NSUserDefaults standardUserDefaults] boolForKey:@"secondLogin"] == NO) {
        self.firstPwd = payPassword;
    }
    
    if (![payPassword isEqualToString:self.firstPwd]) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"secondLogin"] == NO) {
            [self showHud:HBHudShowTypeCaptionOnly withText:@"密码不一致!"];
            [self hideHudAfter:1.0f];
            self.secondPwd = nil;
            [self performSelector:@selector(setPayPassword:) withObject:@"请设置6位数兑换密码" afterDelay:1.0f];
            return;
        }
    } else {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"secondLogin"] == NO) {
            if (self.secondPwd == nil) {
                self.secondPwd = payPassword;
                [self setPayPassword:@"请设置6位数兑换密码"];
                return;
            }
        }
    }
    [HuoBanKit authenticateTokenWithcompletion:^(NSError *error, NSString *message) {
        if (!error||[message isEqualToString:@"用户token有效"]) {
            [self showHud:HBHudShowTypeActivityOnly withText:nil];
            [ASUSUserModel updateUserInfoByUserID:[[BAAuthenticate sharedClient].userId integerValue] name:[BAAuthenticate sharedClient].name pay_password:payPassword birthdday:[BAAuthenticate sharedClient].brithday mobile:[BAAuthenticate sharedClient].mobile phone:[BAAuthenticate sharedClient].phone completion:^(NSError *error, NSString *message) {
                [self hideHud];
                if (!error) {
                    if ([message isEqualToString:@"用户信息更新成功"]) {
                        [BAAuthenticate sharedClient].payPassword = payPassword;
                        [BAAuthenticate sharedClient].set_pay_password = [NSString stringWithFormat:@"1"];
                    }
                    [self showHud:HBHudShowTypeCaptionOnly withText:@"密码设置成功!"];
                    [self hideHudAfter:1.0f];
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"secondLogin"];
                }
            }];
        }else {
            ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
            [self presentViewController:loginVC animated:YES completion:nil];
        }
    }];
    
    
}

#pragma mark - UITableViewDataSource

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.searchResults.count;
    } else if (self.isShowRecentlySearch) {
        if (self.recentlySearchTexts.count == 0) {
            return 1;
        } else {
            return self.recentlySearchTexts.count;
        }
    } else {
        return 4;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 67;
    } else if (self.isShowRecentlySearch) {
        return 40;
    }  else {
        return 67;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(20, 66, ScreenWidth, 0.5)];
    separator.backgroundColor = [NCColorUtil colorForCellSeparator];
    if (!self.isShowRecentlySearch) {
        static NSString *normalIdentifier = @"normalCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:normalIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:normalIdentifier];
            UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
            arrow.image = [NCImageUtil imageFromText:@"\ue60d" fontSize:20 color:[NCColorUtil colorFromHexString:@"#cfcfcf"]];
            cell.accessoryView = arrow;
        }
        [cell.contentView addSubview:separator];
        separator.frame = CGRectMake(20, 66, ScreenWidth, 0.5);
        cell.textLabel.text = [self.cellTitleArray objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [NCColorUtil colorForHomeCellTitleColor];
        cell.imageView.image = [NCImageUtil imageFromText:[self.cellIconArray objectAtIndex:indexPath.row] fontSize:21 color:[NCColorUtil colorFromHexString:@"#84898f"]];
    } else {
        NSString *cellIdentifier = @"recentlydCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.textLabel.textColor = [NCColorUtil colorForHomeCellTitleColor];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
        }
        separator.frame = CGRectMake(20, 39.5, ScreenWidth, 0.5);
        [cell.contentView addSubview:separator];
        cell.backgroundColor = [UIColor whiteColor];
        if (self.recentlySearchTexts.count == 0) {
            cell.textLabel.text = @"暂无历史记录";
            cell.textLabel.frame = CGRectMake(0, 0, ScreenWidth, 40);
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
            cell.textLabel.text = ((ASUSSearchModel *)[self.recentlySearchTexts objectAtIndex:indexPath.row]).searchText;
        }
    }
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
    } else {
        
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
    } else if (self.isShowRecentlySearch) {
        if (self.recentlySearchTexts.count == 0) {
            
        } else {
        ASUSSearchViewController *searchVC = [[ASUSSearchViewController alloc] initWithKeyword:[self.recentlySearchTexts objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:searchVC animated:YES];
        }
    } else {
        switch (indexPath.row) {
            case 0: {
                ASUSPointsMallViewController *pointsMall = [[ASUSPointsMallViewController alloc] init];
                [self.navigationController pushViewController:pointsMall animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
            case 1: {
                ASUSArticleViewController *pointsMall = [[ASUSArticleViewController alloc] initWithType:POLICYNOTICE];
                [self.navigationController pushViewController:pointsMall animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
            case 2:{
                ASUSPriceArticleViewController *noticeVC = [[ASUSPriceArticleViewController alloc] init];
                [self.navigationController pushViewController:noticeVC animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
            case 3: {
                ASUSArticleViewController *pointsMall = [[ASUSArticleViewController alloc] initWithType:TECHNIQUESHARE];
                [self.navigationController pushViewController:pointsMall animated:YES];
                if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchResults = [[NSMutableArray alloc] init];
//    if (self.homeSearchBar.text.length  >0 && ![NCMacro isIncludeChineseInString:self.homeSearchBar.text]) {
//        for (int i=0; i<self.travelData.count; i++) {
//            NCShopModel *travelModel = (NCShopModel *)self.travelData[i];
//            if ([NCMacro isIncludeChineseInString:travelModel.shopName]) {
//                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:travelModel.shopName];
//                NSRange titleResult=[tempPinYinStr rangeOfString:self.homeSearchBar.text options:NSCaseInsensitiveSearch];
//                if (titleResult.length>0) {
//                    [self.searchResults addObject:self.travelData[i]];
//                }
//                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:travelModel.shopName];
//                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:self.homeSearchBar.text options:NSCaseInsensitiveSearch];
//                if (titleHeadResult.length>0) {
//                    [self.searchResults addObject:self.travelData[i]];
//                }
//            }
//            else {
//                NSRange titleResult=[travelModel.shopName rangeOfString:self.homeSearchBar.text options:NSCaseInsensitiveSearch];
//                if (titleResult.length>0) {
//                    [self.searchResults addObject:self.travelData[i]];
//                }
//            }
//        }
//    } else if (self.homeSearchBar.text.length>0&&[NCMacro isIncludeChineseInString:self.homeSearchBar.text]) {
//        for (NCShopModel *tempModel in self.travelData) {
//            NSRange titleResult=[tempModel.shopName rangeOfString:self.homeSearchBar.text options:NSCaseInsensitiveSearch];
//            if (titleResult.length>0) {
//                [self.searchResults addObject:tempModel];
//            }
//        }
//    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    ASUSSearchViewController *searchVC = [[ASUSSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
    
}

#pragma mark -AdPageViewDelegate

- (void)setWebImage:(UIImageView *)imgView imgUrl:(NSString *)imgUrl
{
    [imgView setImageWithURL:[NSURL URLWithString:imgUrl]];
}
- (void)clickAdImageWithIndex:(NSInteger)clickIndex
{
    ASUSColumnPicModel *model = (ASUSColumnPicModel *)self.columnPicArr[clickIndex];
    if (![model.linkUrl isEqualToString:@""]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.linkUrl]];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchTextField resignFirstResponder];
    if (self.searchTextField.text.length > 0) {
        ASUSSearchViewController *searchVC = [[ASUSSearchViewController alloc] initWithKeyword:self.searchTextField.text];
        [self.navigationController pushViewController:searchVC animated:YES];
        [self.searchTextField resignFirstResponder];
        ASUSSearchModel *searchModel = [[ASUSSearchModel alloc] init];
        searchModel.searchText = textField.text;
        [ASUSSearchManager addSearchText:searchModel];
        self.searchTextField.text = @"";
    }else {
        [self showHud:HBHudShowTypeCaptionOnly withText:@"请输入关键字"];
        [self hideHudAfter:2.0];
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self.searchTextField resignFirstResponder];
    return YES;
}


@end

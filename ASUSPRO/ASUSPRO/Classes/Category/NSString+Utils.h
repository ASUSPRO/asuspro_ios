//
//  NSString+Utils.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/1/20.
//  Copyright (c) 2015年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

//去掉字符串中html标签的方法
+(NSString *)filterHTML:(NSString *)html;

//去掉字符串中首尾的换行
+ (NSString *)filterTrimmingCharacters:(NSString *)string;

+(id)stringConvertJson:(NSString *)string;

-(CGSize)calculateSize:(CGSize)size font:(UIFont *)font ;

//针对返回图片的地址进行解析
+ (NSString *)getImageURLString:(NSString *)imageString;
@end

//
//  NSString+Utils.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/1/20.
//  Copyright (c) 2015年 abel. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

+(NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"<" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@">" intoString:&text];
        //替换字符
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
        html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
        html = [html stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        html = [html stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    return html;
}

+ (NSString *)filterTrimmingCharacters:(NSString *)string {
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return string;
}



+(id)stringConvertJson:(NSString *)string
{
    if (string && [string dataUsingEncoding:NSUTF8StringEncoding]) {
        return [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:0 error:0];
    }
    return @"";
}

-(CGSize)calculateSize:(CGSize)size font:(UIFont *)font
{
    CGSize expectedLabelSize = CGSizeZero;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
        expectedLabelSize = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    }
    else {
        expectedLabelSize = [self sizeWithFont:font
                             constrainedToSize:size
                                 lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    return CGSizeMake(ceil(expectedLabelSize.width), ceil(expectedLabelSize.height));
}


+ (NSString *)getImageURLString:(NSString *)imageString
{
    NSString *imageUrlString;
    if ([[NSString stringConvertJson:imageString] isKindOfClass:[NSArray class]]) {
        NSArray *imageArr = [NSString stringConvertJson:imageString];
        imageUrlString = [NSString stringWithFormat:@"%@",imageArr[0][@"originalURL"]];
    }else if ([[NSString stringConvertJson:imageString] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *imageDic = [NSString stringConvertJson:imageString];
        imageUrlString = imageDic[@"originalURL"];
    }else {
        imageUrlString = @"";
    }
    
    return imageUrlString;
}

@end

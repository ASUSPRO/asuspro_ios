//
//  NCMacro.h
//  NetChampin
//
//  Created by Tammy on 14-11-9.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KTEXTFONTSIZE [UIFont boldSystemFontOfSize:14.0]
#define KGAPMIDDLE 24.0f
#define KPADDINGLEFT 10.0f

// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 获取RGB颜色
#define COLOR(R,G,B,A) [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:A]
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

#define KTEXTCOLOR_DEFAULT RGBCOLOR(76, 76, 76)



@interface NCMacro : NSObject

+ (BOOL)isIncludeChineseInString:(NSString*)str;

@end

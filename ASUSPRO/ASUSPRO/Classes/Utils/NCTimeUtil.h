//
//  NCTimeUtil.h
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/4.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCTimeUtil : NSObject

+(NSString *)convertTimeInterval:(NSString *)timeInterval;


@end

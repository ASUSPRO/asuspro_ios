//
//  NCFontUtil.h
//  NetChampin
//
//  Created by 周红梅 on 15/1/29.
//  Copyright (c) 2015年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCFontUtil : NSObject

+(CGFloat)fontForTitle;
+(CGFloat)fontForContent;

//loginTextFont
+(CGFloat)fontForLoginText;

//passwordTextFont
+(CGFloat)fontForPasswordText;
@end

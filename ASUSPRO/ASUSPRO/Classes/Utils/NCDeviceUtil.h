//
//  NCDeviceUtil.h
//  NetChampin
//
//  Created by May on 14-11-7.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

// 竖屏时各种Bar的高度
#define kTabBarHeight                   49.0f
#define kNavigationBarHeight            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 ? 64 : 44)
#define kStatusBarHeight                ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 ? 0 : 20)
#define kSearchBarHeight                44

#define iOS_Version     [[[UIDevice currentDevice] systemVersion] floatValue]
#define ScreenHeight    [[UIScreen mainScreen] bounds].size.height
#define ScreenWidth     [[UIScreen mainScreen] bounds].size.width

@interface NCDeviceUtil : NSObject

+(NSString *)md5:(NSString *)str;

+ (BOOL)is4InchScreen;

+ (float)deviceSystemVersion;

+ (float)screenWidth;

+ (float)screenHeight;

+ (BOOL)transparentStatusbar;

+ (BOOL)isSimulatorForCurrentDevice;

+ (CGFloat)getNavigationOffSet;

+ (BOOL)isValidateMobile:(NSString *)mobileNum;

+ (double)getDistanceFromOriLatitude:(CLLocationDegrees)oriLatitude andOriLongitude:(CLLocationDegrees)oriLongitude ToDestLatitude:(CLLocationDegrees)destLatitude andDestLongitude:(CLLocationDegrees)destLongitude;

@end

//
//  NCTimeUtil.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/6/4.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "NCTimeUtil.h"

@implementation NCTimeUtil

+(NSString *)convertTimeInterval:(NSString *)timeInterval
{
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:[timeInterval doubleValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    return [formatter stringFromDate:detaildate];
}

@end

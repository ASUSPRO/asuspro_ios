//
//  NCColorUtil.h
//  NetChampin
//
//  Created by May on 14-11-7.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define colorRGB(r,g,b)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

@interface NCColorUtil : NSObject
+ (UIColor *)colorForHomeCellTitleColor;
+ (UIColor *)colorForPointsMallProductNameColor;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIColor *)colorForTilte;
+ (UIColor *)colorForCellSeparator;
+ (UIColor *)colorForNavgationBarBackground;
+ (UIColor *)colorForAddressList;

//BackGroundColor
+ (UIColor *)backGroundColorForView;
+ (UIColor *)backGroundColorForMenuView;
+ (UIColor *)backGroundColorForImageFrame;

//SeparatorLineColor
+ (UIColor *)colorForSeparatorLine;

//FontColor
+(UIColor *)colorForContentTimeFont;
+(UIColor *)colorForTypeOrSubContentFont;
+(UIColor *)colorForTitleOrNickFont;
+(UIColor *)colorForMenuSelectFont;
+(UIColor *)colorForMenuUnSelectFont;


//FontColor
+(UIColor *)colorForLikeNumFont;
+(UIColor *)colorForLikeNumIcon;

//IconColor
+(UIColor *)colorForContentTimeIcon;
+(UIColor *)colorForListIcon;


+(UIColor *)colorForTab;
@end

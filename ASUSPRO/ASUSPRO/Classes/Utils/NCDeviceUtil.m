//
//  NCDeviceUtil.m
//  NetChampin
//
//  Created by May on 14-11-7.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import "NCDeviceUtil.h"
#import "sys/utsname.h"
#import <CommonCrypto/CommonDigest.h>

#define StatusBarHeight     [UIApplication sharedApplication].statusBarFrame.size.height

@implementation NCDeviceUtil

+(NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    return [[NSString stringWithFormat:
             @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseStringWithLocale:[NSLocale currentLocale]];
}

+ (BOOL)is4InchScreen {
    static BOOL bIs4Inch = NO;
    static BOOL bIsGetValue = NO;
    
    if (!bIsGetValue) {
        CGRect rcAppFrame = [UIScreen mainScreen].bounds;
        bIs4Inch = (rcAppFrame.size.height == 568.0f);
        
        bIsGetValue = YES;
    }else{}
    
    return bIs4Inch;
}
+ (BOOL)isSimulatorForCurrentDevice
{
    if (TARGET_IPHONE_SIMULATOR) {
        return YES;
    }else {
        return NO;
    }
}

+ (float)deviceSystemVersion {
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

+ (float)screenWidth {
    return [[UIScreen mainScreen] bounds].size.width;
}

+ (float)screenHeight {
    return [[UIScreen mainScreen] bounds].size.height;
}

+ (BOOL)transparentStatusbar {
    return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0;
}

+ (void)resetScrlView:(UIScrollView *)sclView contentInsetWithNaviBar:(BOOL)bHasNaviBar tabBar:(BOOL)bHasTabBar iOS7ContentInsetStatusBarHeight:(NSInteger)iContentMulti inidcatorInsetStatusBarHeight:(NSInteger)iIndicatorMulti {
    if (sclView) {
        UIEdgeInsets inset = sclView.contentInset;
        UIEdgeInsets insetIndicator = sclView.scrollIndicatorInsets;
        CGPoint ptContentOffset = sclView.contentOffset;
        CGFloat fTopInset = bHasNaviBar ? 44.0f : 0.0f;
        CGFloat fTopIndicatorInset = bHasNaviBar ? 44.0f : 0.0f;
        CGFloat fBottomInset = bHasTabBar ? 49.0f : 0.0f;
        
        fTopInset += StatusBarHeight;
        fTopIndicatorInset += StatusBarHeight;
        
        if ([NCDeviceUtil deviceSystemVersion] >= 7.0) {
            fTopInset += iContentMulti * StatusBarHeight;
            fTopIndicatorInset += iIndicatorMulti * StatusBarHeight;
        }else{}
        
        inset.top += fTopInset;
        inset.bottom += fBottomInset;
        [sclView setContentInset:inset];
        
        insetIndicator.top += fTopIndicatorInset;
        insetIndicator.bottom += fBottomInset;
        [sclView setScrollIndicatorInsets:insetIndicator];
        
        ptContentOffset.y -= fTopInset;
        [sclView setContentOffset:ptContentOffset];
    } else {
    }
}

// 清除PerformRequests和notification
+ (void)cancelPerformRequestAndNotification:(UIViewController *)viewCtrl {
    if (viewCtrl)
    {
        [[viewCtrl class] cancelPreviousPerformRequestsWithTarget:viewCtrl];
        [[NSNotificationCenter defaultCenter] removeObserver:viewCtrl];
    }else{}
}

+ (CGFloat)getNavigationOffSet {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        return 20;
    }
    return 0;
}

+ (BOOL)isValidateMobile:(NSString *)mobileNum
{
    
    NSString *nod = @"^1\\d{10}$";
    NSPredicate *nodMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nod];
    
    if ([nodMobile evaluateWithObject:mobileNum] == YES){
        return YES;
    } else {
        return NO;
    }
}
//+ (BOOL)isMobileNumber:(NSString *)mobileNum
//{
//    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
//    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
//    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
//    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
//    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
//    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
//    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
//    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
//    
//    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
//        || ([regextestcm evaluateWithObject:mobileNum] == YES)
//        || ([regextestct evaluateWithObject:mobileNum] == YES)
//        || ([regextestcu evaluateWithObject:mobileNum] == YES))
//    {
//        return YES;
//    }
//    else
//    {
//        return NO;
//    }
//}

+ (double)getDistanceFromOriLatitude:(CLLocationDegrees)oriLatitude andOriLongitude:(CLLocationDegrees)oriLongitude ToDestLatitude:(CLLocationDegrees)destLatitude andDestLongitude:(CLLocationDegrees)destLongitude
{
    CLLocation *oriLocation = [[CLLocation alloc] initWithLatitude:oriLatitude longitude:oriLongitude];
    CLLocation *desLocation = [[CLLocation alloc] initWithLatitude:destLatitude longitude:destLongitude];
    CLLocationDistance meters = [oriLocation distanceFromLocation:desLocation];
    return meters;
}

@end

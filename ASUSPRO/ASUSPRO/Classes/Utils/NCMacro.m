//
//  NCMacro.m
//  NetChampin
//
//  Created by Tammy on 14-11-9.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import "NCMacro.h"

@implementation NCMacro

+ (BOOL)isIncludeChineseInString:(NSString*)str {
    for (int i=0; i<str.length; i++) {
        unichar ch = [str characterAtIndex:i];
        if (0x4e00 < ch  && ch < 0x9fff) {
            return true;
        }
    }
    return false;
}

@end

//
//  NCImageUtil.h
//  NetChampin
//
//  Created by zhm on 14-11-17.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCImageUtil : NSObject

+ (UIImage *)imageFromText:(NSString *)text fontSize:(CGFloat)fontSize color:(UIColor *)color;

+(UIImage*)circleImage:(UIImage*) image withParam:(CGFloat) inset;

+ (UIImage *)image:(UIImage*)image scaledToSize:(CGSize)newSize;

+ (UIImage *)cutImage:(UIImage*)image toSize:(CGSize)toSize;

+ (UIImage *)createImageWithColor:(UIColor *)color;
@end

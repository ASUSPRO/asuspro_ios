//
//  NCFontUtil.m
//  NetChampin
//
//  Created by 周红梅 on 15/1/29.
//  Copyright (c) 2015年 abel. All rights reserved.
//

#import "NCFontUtil.h"

@implementation NCFontUtil

+(CGFloat)fontForTitle
{
    return 15.0;
}
+(CGFloat)fontForContent
{
    return 12.0;
}
+(CGFloat)fontForLoginText
{
    return 17.0;
}
+(CGFloat)fontForPasswordText
{
    return 14.0;
}
@end

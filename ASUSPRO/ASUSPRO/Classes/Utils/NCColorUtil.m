//
//  NCColorUtil.m
//  NetChampin
//
//  Created by May on 14-11-7.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import "NCColorUtil.h"

@implementation NCColorUtil
// 从WEB Color获取RGB颜色值
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
+ (UIColor *)colorForTilte {
    return [UIColor colorWithRed:38.0/255.0 green:36.0/255.0 blue:49.0/255.0 alpha:1.0];
}

+ (UIColor *)colorForHomeCellTitleColor {
   return [NCColorUtil colorFromHexString:@"#727982"];
}

+ (UIColor *)colorForPointsMallProductNameColor {
    return [NCColorUtil colorFromHexString:@"#606366"];
}

+ (UIColor *)colorForNavgationBarBackground {
    return [NCColorUtil colorFromHexString:@"#363a3f"];
}

+ (UIColor *)colorForAddressList {
    return [UIColor colorWithRed:96.0/255.0 green:99.0/255.0 blue:102.0/255.0 alpha:1.0];
}

+ (UIColor *)colorForCellSeparator {
    return [NCColorUtil colorFromHexString:@"#cfcfcf"];
}

+ (UIColor *)backGroundColorForView
{
    return colorRGB(236, 236, 236);
}

+ (UIColor *)backGroundColorForMenuView
{
     return colorRGB(119, 119, 119);
}
+ (UIColor *)backGroundColorForImageFrame
{
     return colorRGB(255, 255, 255);
}

+ (UIColor *)colorForSeparatorLine
{
    return [NCColorUtil colorFromHexString:@"#cfcfcf"];
}

+(UIColor *)colorForContentTimeFont
{
    return colorRGB(170, 170, 170);
}
+(UIColor *)colorForTypeOrSubContentFont
{
    return colorRGB(136, 136, 136);
}
+(UIColor *)colorForTitleOrNickFont
{
    return colorRGB(102, 102, 102);
}

+(UIColor *)colorForMenuSelectFont
{
    return colorRGB(62, 248, 228);
}
+(UIColor *)colorForMenuUnSelectFont
{
    return colorRGB(248, 248, 248);
}

+(UIColor *)colorForContentTimeIcon
{
    return colorRGB(170, 170, 170);
}
+(UIColor *)colorForListIcon
{
   return colorRGB(136, 136, 136);
}

+(UIColor *)colorForLikeNumFont
{
    return colorRGB(136, 136, 136);
}

+(UIColor *)colorForLikeNumIcon
{
    return colorRGB(136, 136, 136);
}

+(UIColor *)colorForTab
{
    return colorRGB(248, 248, 248);
}

@end

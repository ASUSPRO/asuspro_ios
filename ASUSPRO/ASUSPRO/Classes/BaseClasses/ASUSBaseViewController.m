//
//  ASUSBaseViewController.m
//  ASUSPRO
//
//  Created by May on 15-5-4.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "ASUSBaseViewController.h"
#import "MBProgressHUD.h"

@interface ASUSBaseViewController ()

@property (nonatomic, strong) MBProgressHUD * Hud;

@end

@implementation ASUSBaseViewController

//- (void)back {
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
//    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.extendedLayoutIncludesOpaqueBars = NO;
//    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) { // 判断是否是IOS7
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    }
   
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setCustomView:(UIView *)view
{
    self.Hud.customView = view;
}

- (void)showHud:(HBHudShowType)type withText:(NSString *)text
{
    //初始化进度框，置于当前的View当中
    if (!self.Hud) {
        self.Hud = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.Hud];
    }
    
    //设置对话框文字
    self.Hud.labelText = text;
    
    switch (type) {
        case HBHudShowTypeCaptionOnly:
            self.Hud.mode = MBProgressHUDModeText;
            break;
        case HBHudShowTypeCaptionAndImage:
            self.Hud.mode = MBProgressHUDModeCustomView;
            break;
        case HBHudShowTypeCaptionAndActivity:
        case HBHudShowTypeActivityOnly:
            self.Hud.mode = MBProgressHUDModeIndeterminate;
        default:
            break;
    }
    
    //如果设置此属性则当前的view置于后台
    self.Hud.dimBackground = YES;
    
    [self.Hud show:NO];
}

- (void)hideHud
{
    if (self.Hud) {
        [self.Hud hide:NO];
    }
}

- (void)hideHudAfter:(NSTimeInterval)delay
{
    if (self.Hud) {
        [self.Hud hide:NO afterDelay:delay];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

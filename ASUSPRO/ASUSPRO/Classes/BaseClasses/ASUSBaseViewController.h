//
//  ASUSBaseViewController.h
//  ASUSPRO
//
//  Created by May on 15-5-4.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    HBHudShowTypeActivityOnly = 0,//无文本的Loading
    HBHudShowTypeCaptionOnly,//纯文本
    HBHudShowTypeCaptionAndActivity,//自定义文本的Loading
    HBHudShowTypeCaptionAndImage//自定义文本和图片
} HBHudShowType;


@interface ASUSBaseViewController : UIViewController

- (void)setCustomView:(UIView *)view;
- (void)showHud:(HBHudShowType)type withText:(NSString *)text;

- (void)hideHud;
- (void)hideHudAfter:(NSTimeInterval)delay;

//- (void)back;


@end

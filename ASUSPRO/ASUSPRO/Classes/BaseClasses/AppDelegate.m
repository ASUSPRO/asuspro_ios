//
//  AppDelegate.m
//  ASUSPRO
//
//  Created by 周红梅 on 15/4/29.
//  Copyright (c) 2015年 周红梅. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "NCColorUtil.h"
#import "NCImageUtil.h"
#import "HuoBanKit.h"
#import "ASUSLoginViewController.h"

@interface AppDelegate ()
//{
//    NSTimer *conectionTimer;
//    BOOL done;
//}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    UINavigationController *
    
    self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
//    conectionTimer=[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
//    
//    [[NSRunLoop currentRunLoop] addTimer:conectionTimer forMode:NSDefaultRunLoopMode];
//    do
//    {
//        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
//    }
//    while (!done);
    [NSThread sleepForTimeInterval:1.0];
    if ([HuoBanKit isAuthenticated]) {
        HomeViewController *hvc = [[HomeViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hvc];
        [nav.navigationBar setBackgroundImage:[NCImageUtil createImageWithColor:[NCColorUtil colorFromHexString:@"#363A3F"] ]forBarMetrics:UIBarMetricsDefault];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.window.rootViewController = nav;
    }else {
        ASUSLoginViewController *loginVC = [[ASUSLoginViewController alloc] init];
        self.window.rootViewController = loginVC;
    }
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    return YES;
}
//-(void)timerFired:(NSTimer *)timer
//{
//    done=YES;
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
